/**
 * Style.as
 * Keith Peters
 * version 0.9.10
 * 
 * A collection of style variables used by the components.
 * If you want to customize the colors of your components, change these values BEFORE instantiating any components.
 * 
 * Copyright (c) 2011 Keith Peters
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.bit101.components
{
	import flash.display.Stage;

	public class Style
	{
		public static var TEXT_BACKGROUND:uint = 0xFFFFFF;//default 0xFFFFFF
		public static var BACKGROUND:uint = 0xD6DBDF;//0xCCCCCC
		public static var BUTTON_FACE:uint = 0xE5E5E5;//default 0xFFFFFF
		public static var BUTTON_DOWN:uint = 0xEEEEEE;
		public static var INPUT_TEXT:uint = 0x333333;//default 0x333333
		public static var LABEL_TEXT:uint = 0x666666;
		public static var DROPSHADOW:uint = 0x000000;
		public static var PANEL:uint = 0xF3F3F3; // change color of Motors and No voice command
		public static var PROGRESS_BAR:uint = 0xFFFFFF;
		public static var LIST_DEFAULT:uint = CSS.listColor;//0xF1F1F1;//default 0xFFFFFF // list color
		public static var LIST_ALTERNATE:uint = 0xF3F3F3;
		public static var LIST_SELECTED:uint = 0xCCCCCC; // UIfile_list selected item color
		public static var LIST_ROLLOVER:uint = 0XDDDDDD; // UIfile_list rollover item color
		
		public static var embedFonts:Boolean = false;//default true
		public static var fontName:String = "PF Ronda Seven";//default PF Ronda Seven
		public static var fontSize:Number = 12;//HAHAHA
		public static const DARK:String = "dark";
		public static const LIGHT:String = "light";
		public static var fontNameUIFile:String = "Arial";
		/**
		 * Applies a preset style as a list of color values. Should be called before creating any components.
		 */
		public function setStyle(style:String):void
		{
			switch(style)
			{
				case DARK:
					Style.BACKGROUND = 0x444444; // Motor bottom rectangle
					Style.BUTTON_FACE = 0x666666; // scroll on right
					Style.BUTTON_DOWN = 0x222222;
					Style.INPUT_TEXT = 0xBBBBBB;
					Style.LABEL_TEXT = CSS.golden;//white//0xCCCCCC; // color UIFile text
					Style.PANEL = 0x666666; // color panel Motor upper rectangle, No Voice Project
					Style.PROGRESS_BAR = 0x666666;
					Style.TEXT_BACKGROUND = 0x555555;
//					Style.LIST_DEFAULT = 0xFF;//0x7F7F7F;//0x444444; // list color
					Style.LIST_ALTERNATE = 0x393939;
					Style.LIST_SELECTED = 0x666666;
					Style.LIST_ROLLOVER = 0x777777;
					CSS.tabColor = CSS.tabColorDark; // dark background
					CSS.panelColor = CSS.tabColorDark; // dark background for sound section
					CSS.itemSelectedColor = CSS.itemSelectedColor; // selection music in sound section
					CSS.borderColor = CSS.tabColorDark;
					CSS.textColor =	0xFF0000;//CSS.white;
					CSS.buttonLabelOverColor = 0xFFF000;
	//				CSS.offColor = CSS.tabColorDark;// for control, motions, wheels ...
	//				CSS.onColor = 0xFF;// script, sound writing when on
	//				CSS.overColor = 0xFFF000;// over control, motions, wheels
					CSS.titleBarColors = [CSS.white, 0x4c4c4c];
					MBlock.app.stage.color = 0x4c4c4c; // topBar
					CSS.textNoVCProject = CSS.golden;//white;
					CSS.sendTextPaneColor = 0xFF;
					CSS.listColor = 0x7F7F7F;
					break;
				case LIGHT:
				default:
					Style.BACKGROUND = 0xCCCCCC;
					Style.BUTTON_FACE = 0xFFFFFF;
					Style.BUTTON_DOWN = 0xEEEEEE;
					Style.INPUT_TEXT = 0x333333;
					Style.LABEL_TEXT = 0x666666;
					Style.PANEL = 0xF3F3F3;
					Style.PROGRESS_BAR = 0xFFFFFF;
					Style.TEXT_BACKGROUND = 0xFFFFFF;
					Style.LIST_DEFAULT = 0xF1F1F1;
					Style.LIST_ALTERNATE = 0xF3F3F3;
					Style.LIST_SELECTED = 0xCCCCCC;
					Style.LIST_ROLLOVER = 0xDDDDDD;
					CSS.tabColor = 0xE6E8E8;
					CSS.panelColor = 0xF2F2F2;
					CSS.itemSelectedColor = 0xD0D0D0; // selection music in sound section
					CSS.borderColor = 0xD0D1D2;
					CSS.textColor =	0x5C5D5F;// text color
					CSS.buttonLabelOverColor = 0xFBA939;//
	//				CSS.offColor = 0x8F9193;// for control, motions, wheels ...
	//				CSS.onColor = CSS.textColor;// script, sound writing when on
	//				CSS.overColor = 0x179FD7;// over control, motions, wheels
					CSS.titleBarColors = [CSS.white, CSS.tabColor];
					MBlock.app.stage.color = CSS.white; //topBar
					CSS.textNoVCProject = 0x505050;
					CSS.sendTextPaneColor = 0xFFF000;
					CSS.listColor = 0xF1F1F1;
					break;
			}
		}
	}
}