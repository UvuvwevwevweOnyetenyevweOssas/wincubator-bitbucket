rem Scratch for Wonderboy V 0.0, Steven, GT Robot, December 2016, Singapore.
rem This batch script is used for environment init.
adb.exe shell mkdir /data/local/tmp/scratch
adb.exe shell mkdir /data/local/tmp/scratch/main_prg
adb.exe shell mkdir /data/local/tmp/scratch/movlog
adb.exe push busybox /data/local/tmp
adb.exe shell chmod 777 /data/local/tmp/busybox
