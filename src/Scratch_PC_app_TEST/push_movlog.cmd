rem Scratch for Wonderboy V 0.0, Steven, GT Robot, December 2016, Singapore.
rem This batch script is used for file copy from PC to Wonderboy, %2 should be movlog file.
rem %1 push %2 /mnt/sdcard/GTWonderBoy/scratch/user_mov/
SET binPath=%1
SET binPath=%binPath:${SPACE}= %
SET lcPath=%2
SET lcPath=%lcPath:${SPACE}= %
"%binPath%" %3 %4 push "%lcPath%" /mnt/sdcard/GTWonderBoy/console/