rem Scratch for Wonderboy V 0.0, Steven, GT Robot, December 2016, Singapore.
rem This batch script is used for file delete in Wonderboy, %1 should be prg file.

adb shell rm /data/local/tmp/scratch/prg/%1
pause