rem Scratch for Wonderboy V 0.0, Steven, GT Robot, December 2016, Singapore.
rem This batch script is used for file copy from PC to Wonderboy, %1 should be prg file.
rem %1 shell chmod 777 /data/local/tmp/scratch/main_prg/%3
SET binPath=%1
SET binPath=%binPath:${SPACE}= %
SET lcPath=%2
SET lcPath=%lcPath:${SPACE}= %
"%binPath%" %4 %5 push "%lcPath%" /mnt/sdcard/GTWonderBoy/console/
"%binPath%" %4 %5 shell am broadcast -a com.gtgroup.gtdollar.custom.task --es task_value "%3"
rem C:\Users\xin\Documents\gblock\bin-debug\Scratch_PC_app_TEST\adb.exe -s 0123456789ABCDEF push C:\Users\xin\Documents\gblock\bin-debug\mainscripts\1000008.cet /mnt/sdcard/GTWonderBoy/console/
rem C:\Users\xin\Documents\gblock\bin-debug\Scratch_PC_app_TEST\adb.exe -s 0123456789ABCDEF shell am broadcast -a com.gtgroup.gtdollar.custom.task --es task_value "a1651000013.cet"