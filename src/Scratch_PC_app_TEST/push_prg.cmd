rem Scratch for Wonderboy V 0.0, Steven, GT Robot, December 2016, Singapore.
rem This batch script is used for file copy from PC to Wonderboy, %1 should be prg file.
rem %1 shell chmod 777 /data/local/tmp/scratch/main_prg/%3
SET binPath=%1
SET binPath=%binPath:${SPACE}= %
"%binPath%" push %2 /data/local/tmp/scratch/main_prg
