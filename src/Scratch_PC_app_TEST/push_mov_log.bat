rem Scratch for Wonderboy V 0.0, Steven, GT Robot, December 2016, Singapore.
rem This batch script is used for file copy from PC to Wonderboy, %1 should be movlog file.

adb push %1 /data/local/tmp/scratch/movlog
adb shell chmod 777 /data/local/tmp/scratch/movlog/%1
pause