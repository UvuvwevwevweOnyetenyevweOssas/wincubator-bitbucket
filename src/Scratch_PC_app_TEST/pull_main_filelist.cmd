rem Scratch for Wonderboy V 0.0, Steven, GT Robot, December 2016, Singapore.
rem This batch script is used for file copy from Wonderboy to PC, %1 should be prg file.
rem %1 shell ls /data/local/tmp/scratch/main_prg/ > /data/local/tmp/scratch/main_prg/main_prg_filelist.txt
rem %1 pull /data/local/tmp/scratch/main_prg/main_prg_filelist.txt %2
SET binPath=%1
SET binPath=%binPath:${SPACE}= %
SET lcPath=%2
SET lcPath=%lcPath:${SPACE}= %
"%binPath%" %3 %4 pull /mnt/sdcard/GTWonderBoy/task.gwb "%lcPath%