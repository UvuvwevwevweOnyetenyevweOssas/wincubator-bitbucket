rem Scratch for Wonderboy V 0.0, Steven, GT Robot, December 2016, Singapore.
rem This batch script is used for WiFi connection, %2 should be the IP address of Wonderboy.
rem adb connect %2:5555
SET binPath=%1
SET binPath=%binPath:${SPACE}= %
"%binPath%" connect %2:5555
