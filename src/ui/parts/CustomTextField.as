// ActionScript file
package ui.parts 
{
	import flash.events.Event;
	import flash.text.TextField;
	
	public class CustomTextField extends TextField
	{
		public function CustomTextField()
		{
			super();
		}
		
		override public function set text( value:String ):void
		{
			if( super.text != value )
			{
				super.text = value;
				dispatchEvent(new Event(Event.CHANGE, true));
			}
		}
	}
}