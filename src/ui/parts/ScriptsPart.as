/*
* Scratch Project Editor and Player
* Copyright (C) 2014 Massachusetts Institute of Technology
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

// ScriptsPart.as
// John Maloney, November 2011
//
// This part holds the palette and scripts pane for the current sprite (or stage).

package ui.parts {
	import com.bit101.components.InputText;
	import com.bit101.components.Label;
	import com.bit101.components.List;
	import com.bit101.components.ListItem;
	import com.bit101.components.Panel;
	
	import flash.concurrent.Mutex;
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.errors.EOFError;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.dns.AAAARecord;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	import flash.utils.getTimer;
	
	import assets.Resources;
	
	import blocks.Block;
	
	import cc.makeblock.util.FileUtil;
	import cc.makeblock.util.HexUtil;
	
	import extensions.ArduinoManager;
	import extensions.PosChecker;
	import extensions.SerialManager;
	
	import scratch.ScratchObj;
	import scratch.ScratchSprite;
	import scratch.ScratchStage;
	
	import svgeditor.ColorPicker;
	
	import translation.Translator;
	
	import ui.BlockPalette;
	import ui.PaletteSelector;
	import ui.media.MediaLibrary;
	
	import uiwidgets.Button;
	import uiwidgets.DialogBox;
	import uiwidgets.IconButton;
	import uiwidgets.IndicatorLight;
	import uiwidgets.Menu;
	import uiwidgets.ScriptsPane;
	import uiwidgets.ScrollFrame;
	import uiwidgets.Scrollbar;
	import uiwidgets.SimpleTooltips;
	import uiwidgets.StretchyBitmap;
	import uiwidgets.TextPane;
	import uiwidgets.ZoomWidget;
	
	import util.JSON;
	
	public class ScriptsPart extends UIPart {
		
		private var shape:Shape;
		public var selector:PaletteSelector;
		private var spriteWatermark:Bitmap;
		private var paletteFrame:ScrollFrame;
		private var scriptsFrame:ScrollFrame;
		private var arduinoFrame:ScrollFrame;
		private var arduinoTextPane:TextPane;
		private var messageTextPane:TextPane;
		private var lineNumText:TextField;
		private var zoomWidget:ZoomWidget;
		private var lineNumWidth:uint = 20;
		private const readoutLabelFormat:TextFormat = new TextFormat(CSS.font, 12, CSS.textColor, true);
		private const readoutFormat:TextFormat = new TextFormat(CSS.font, 12, CSS.textColor);
		private var xyDisplay:Sprite;
		private var xLabel:TextField;
		private var yLabel:TextField;
		private var xReadout:TextField;
		private var yReadout:TextField;
		private var lastX:int = -10000000; // impossible value to force initial update
		private var lastY:int = -10000000; // impossible value to force initial update
		private var backBt:Button = new Button(Translator.map("Back"));
		private var uploadBt:Button = new Button(Translator.map("Upload to Arduino"));
		//	private var openBt:Button = new Button(Translator.map("Open with Arduino IDE"));
		//	private var sendBt:Button = new Button(Translator.map("Send"));
//		private var sendTextPane:TextPane;
		private var sendTextPane:CustomTextField;
		private var sendTextPane_translated:CustomTextField;
		private var isByteDisplayMode:Boolean = true;
		//	private var displayModeBtn:Button = new Button(Translator.map("binary mode"));
		private var isByteInputMode:Boolean = false;
		//	private var inputModeBtn:Button = new Button(Translator.map("char mode"));
		
		//Steven, WiFi connect button
		private var wifiBt:Button = new Button(Translator.map("Init WiFi"));// translator mapping needs to be done!
		private var wifiBt1:Button = new Button(Translator.map("Connect"));
		private var uploadwbBt:Button = new Button(Translator.map("Upload"));
		private var ipBt:Button = new Button(Translator.map("Get IP"));//
		private var abortBt:Button = new Button(Translator.map("Con Status"));
		private var pullBt:Button = new Button(Translator.map("Pull"));
		private var filelistBt:Button = new Button(Translator.map("FL"));
		private var filedelBt:Button = new Button(Translator.map("Del"));
		private var filerunBt:Button = new Button(Translator.map("Run"));
		private var startBt_icon: IconButton = new IconButton(null, "startbt");
		private var stopBt_icon: IconButton = new IconButton(null, "stopbt");
		private var deleteBt_icon: IconButton = new IconButton(null, "deletebt");
		private var uploadBt_icon: IconButton = new IconButton(null, "uploadbt");
		private var connectBt_icon: IconButton = new IconButton(null, "connectbt");
		
		private var fl_label : Label = new Label;
		private var fl_panel: Panel = new Panel;
		private var wb_panel_pic: Panel = new Panel;
		private var wb_panel_1: Panel = new Panel;
		private var wb_panel_2: Panel = new Panel;
		private var wb_panel_3: Panel = new Panel;
		private var wb_panel_4: Panel = new Panel;
		private var wb_panel_5: Panel = new Panel;
		private var wb_panel_6: Panel = new Panel;
		private var wb_panel_7: Panel = new Panel;
		private var wb_panel_8: Panel = new Panel;
		private var wb_panel_9: Panel = new Panel;
		private var wb_panel_10: Panel = new Panel;
		private var wb_panel_11: Panel = new Panel;
		private var wb_panel_12: Panel = new Panel;
		//14. Malcolm Save movement block
//		private var saveMovBt:Button = new Button(Translator.map("Save Movement"));
		private var saveMovBt:IconButton = new IconButton(null, "mov_savebt");
		//
		//15. Malcolm Delete movement block
//		private var delMovBt:Button = new Button(Translator.map("Delete Movement"));
		private var delMovBt:IconButton = new IconButton(null, "mov_deletebt");
				
//		private var testMovBt:Button = new Button(Translator.map("T"));
		private var testMovBt:IconButton = new IconButton(null, "mov_uploadbt");
//		private var testMovBt:Button = new Button(Translator.map("T"));
		private var testMovBt2:IconButton = new IconButton(null, "mov_startbt");
		
		private var motor_pwon:IconButton = new IconButton(null, "motor_pwon");
		private var motor_pwof:IconButton = new IconButton(null, "motor_pwof");
		
		//Steven, List
		private var menu_files:Menu;
		private var UIfile_list: List = new List;
		//Image ui/part/'wb_fv.png
		[Embed('wb_fv.jpg')]
		private var ImageAsset	: Class;
		private var wbpic:Bitmap = new ImageAsset() as Bitmap;
		//Input text fields *12
		public static var input_text_1: InputText = new InputText;
		private var input_lb_1:TextField = new TextField;
		public static var input_text_2: InputText = new InputText;
		private var input_lb_2:TextField = new TextField;
		public static var input_text_3: InputText = new InputText;
		private var input_lb_3:TextField = new TextField;
		public static var input_text_4: InputText = new InputText;
		private var input_lb_4:TextField = new TextField;
		public static var input_text_5: InputText = new InputText;
		private var input_lb_5:TextField = new TextField;
		public static var input_text_6: InputText = new InputText;
		private var input_lb_6:TextField = new TextField;
		public static var input_text_7: InputText = new InputText;
		private var input_lb_7:TextField = new TextField;
		public static var input_text_8: InputText = new InputText;
		private var input_lb_8:TextField = new TextField;
		public static var input_text_9: InputText = new InputText;
		private var input_lb_9:TextField = new TextField;
		public static var input_text_10: InputText = new InputText;
		private var input_lb_10:TextField = new TextField;
		public static var input_text_11: InputText = new InputText;
		private var input_lb_11:TextField = new TextField;
		public static var input_text_12: InputText = new InputText;
		private var input_lb_12:TextField = new TextField;
		private var myFormat:TextFormat = new TextFormat();
		
		public static var pos_info: String = new String;
		public var time_stamp: String = new String;
		
		public function ScriptsPart(app:MBlock) {
			//Font setting
			var font:String = Resources.chooseFont([
				'Lucida Grande', 'Verdana', 'Arial', 'DejaVu Sans']);
			myFormat = new TextFormat(font, 10, CSS.textNoVCProject, false);//new TextFormat(font, 10, 0x505050, false);
//			input_lb_1.defaultTextFormat =  myFormat; 
//			input_lb_2.defaultTextFormat =  myFormat; 
//			input_lb_3.defaultTextFormat =  myFormat;
//			input_lb_4.defaultTextFormat =  myFormat;
//			input_lb_5.defaultTextFormat =  myFormat;
//			input_lb_6.defaultTextFormat =  myFormat;
//			input_lb_7.defaultTextFormat =  myFormat;
//			input_lb_8.defaultTextFormat =  myFormat;
//			input_lb_9.defaultTextFormat =  myFormat;
//			input_lb_10.defaultTextFormat =  myFormat;
//			input_lb_11.defaultTextFormat =  myFormat;
//			input_lb_12.defaultTextFormat =  myFormat;
			
			
			this.app = app;
			addChild(shape = new Shape());
			//9. Malcolm
			//addChild(spriteWatermark = new Bitmap());
			//9. Malcolm
			//addXYDisplay();
			addChild(selector = new PaletteSelector(app));
			
			var palette:BlockPalette = new BlockPalette();
			palette.color = CSS.tabColor;
			paletteFrame = new ScrollFrame();
			paletteFrame.allowHorizontalScrollbar = false;
			paletteFrame.setContents(palette);
			addChild(paletteFrame);
			
			var scriptsPane:ScriptsPane = new ScriptsPane(app);
			scriptsFrame = new ScrollFrame(true);
			scriptsFrame.setContents(scriptsPane);
			addChild(scriptsFrame);
			
			app.palette = palette;
			app.scriptsPane = scriptsPane;
			
			addChild(zoomWidget = new ZoomWidget(scriptsPane));
			
			arduinoFrame = new ScrollFrame(false);
			arduinoFrame.visible = false;
			
			arduinoTextPane = new TextPane();
			//		arduinoTextPane.type = TextFieldType.INPUT;
			var ft:TextFormat = new TextFormat("Arial",14,0x00325a);
			ft.blockIndent = 5;
			arduinoTextPane.textField.defaultTextFormat = ft;
			arduinoTextPane.textField.background = true;
			arduinoTextPane.textField.backgroundColor = 0xfcfcfc;
			arduinoTextPane.textField.type = TextFieldType.DYNAMIC;
			
			messageTextPane = new TextPane;
			messageTextPane.textField.defaultTextFormat = ft;
			messageTextPane.textField.background = true;
			messageTextPane.textField.backgroundColor = 0xc8c8c8;
			
//			sendTextPane = new TextPane();
			sendTextPane = new CustomTextField();
			sendTextPane.defaultTextFormat = ft;
			sendTextPane.background = true;
			sendTextPane.backgroundColor = 0xf8f8f8;
			sendTextPane.type = TextFieldType.INPUT;
			sendTextPane.multiline = false;
//			sendTextPane.scrollbar.visible = false;
			
//			sendTextPane_translated = new TextPane();
			sendTextPane_translated = new CustomTextField();
//			sendTextPane_translated.defaultTextFormat = ft;
			var ft2:TextFormat = new TextFormat("Arial",14,CSS.sendTextPaneMessage); // dark blue
			sendTextPane_translated.defaultTextFormat = ft2;
			sendTextPane_translated.background = true;
			sendTextPane_translated.backgroundColor = CSS.sendTextPaneColor;//0xf8f8f8; // white/grey
//			sendTextPane_translated.backgroundColor = 0xf8f8f8;
			sendTextPane_translated.type = TextFieldType.INPUT;
			sendTextPane_translated.multiline = false;
//			sendTextPane_translated.visible = false;
			
			lineNumText = new TextField;
			lineNumText.defaultTextFormat = new TextFormat("Arial",14,0x8a8a8a);
			lineNumText.selectable = false;
			arduinoTextPane.textField.addEventListener(Event.SCROLL,onScroll);		
			//		backBt.x = 10;
			//		backBt.y = 10;
			//		backBt.addEventListener(MouseEvent.CLICK,onHideArduino);
			//		arduinoFrame.addChild(backBt);
			//		uploadBt.x = 70;
			//		uploadBt.y = 10;
			//		uploadBt.addEventListener(MouseEvent.CLICK,onCompileArduino);
			//		arduinoFrame.addChild(uploadBt);
			
			//Steven, WiFi connect button
			wifiBt.addEventListener(MouseEvent.CLICK,onWiFiConnect_timer);
//			arduinoFrame.addChild(wifiBt);
			
			ipBt.x = 130;
			ipBt.y = 10;
			ipBt.addEventListener(MouseEvent.CLICK,ongetWiFiIP);
//			arduinoFrame.addChild(ipBt);
			
			wifiBt1.x = 180;
			wifiBt1.y = 10;
			wifiBt1.addEventListener(MouseEvent.CLICK,onWiFiConnect1);
//			arduinoFrame.addChild(wifiBt1);
			

			uploadwbBt.addEventListener(MouseEvent.CLICK,onuploadwbBt_timer);//_timer
//			arduinoFrame.addChild(uploadwbBt);

			
			abortBt.x = 310;
			abortBt.y = 10;
			abortBt.addEventListener(MouseEvent.CLICK,adb_con_check);
//			abortBt.addEventListener(MouseEvent.CLICK,onabortBt);
//			arduinoFrame.addChild(abortBt);
			
			
//			arduinoFrame.addChild(filedelBt);
//			arduinoFrame.addChild(filerunBt);
			
//			arduinoFrame.addChild(startBt_icon);
//			arduinoFrame.addChild(stopBt_icon);
//			arduinoFrame.addChild(deleteBt_icon);
//			arduinoFrame.addChild(uploadBt_icon);
//			arduinoFrame.addChild(connectBt_icon);
			
			//14. Malcolm
			saveMovBt.addEventListener(MouseEvent.CLICK,onSaveLog);
			motor_pwon.addEventListener(MouseEvent.CLICK,onmotor_pwon);
			motor_pwof.addEventListener(MouseEvent.CLICK,onmotor_pwof);
			//
			//15. Malcolm
			delMovBt.addEventListener(MouseEvent.CLICK,onDelLog);
			testMovBt.addEventListener(MouseEvent.CLICK,onT);
			testMovBt2.addEventListener(MouseEvent.CLICK,onM);
			//		openBt.y = 10;
			//		openBt.addEventListener(MouseEvent.CLICK,onOpenArduinoIDE);		
			//		sendBt.addEventListener(MouseEvent.CLICK,onSendSerial);
			//		displayModeBtn.addEventListener(MouseEvent.CLICK,onDisplayModeChange);
			//		inputModeBtn.addEventListener(MouseEvent.CLICK,onInputModeChange);
			//		arduinoFrame.addChild(openBt);
			
			arduinoFrame.addChild(UIfile_list);
			arduinoFrame.addChild(arduinoTextPane);
//			arduinoFrame.addChild(messageTextPane);
//			arduinoFrame.addChild(lineNumText);//not useful
//			arduinoFrame.addChild(sendTextPane);
			arduinoFrame.addChild(sendTextPane_translated);
			sendTextPane.addEventListener(Event.CHANGE, onsendTextPaneChange);
				
				
//			arduinoFrame.addChild(fl_label);
			arduinoFrame.addChild(fl_panel);
//			arduinoFrame.addChild(wbpic);
			arduinoFrame.addChild(wb_panel_pic);//wb_panel_pic
			wb_panel_pic.addChild(wbpic);
			
			//		arduinoFrame.addChild(sendBt);
			//		arduinoFrame.addChild(displayModeBtn);
			//		arduinoFrame.addChild(inputModeBtn);
//			arduinoFrame.addChild(input_text_1);
			arduinoFrame.addChild(wb_panel_1);arduinoFrame.addChild(wb_panel_2);arduinoFrame.addChild(wb_panel_3);arduinoFrame.addChild(wb_panel_4);
			arduinoFrame.addChild(wb_panel_5);arduinoFrame.addChild(wb_panel_6);arduinoFrame.addChild(wb_panel_7);arduinoFrame.addChild(wb_panel_8);
			arduinoFrame.addChild(wb_panel_9);arduinoFrame.addChild(wb_panel_10);arduinoFrame.addChild(wb_panel_11);arduinoFrame.addChild(wb_panel_12);
			
//			arduinoFrame.addChild(input_text_2);arduinoFrame.addChild(input_text_3);arduinoFrame.addChild(input_text_4);arduinoFrame.addChild(input_text_5);
//			arduinoFrame.addChild(input_text_6);arduinoFrame.addChild(input_text_7);arduinoFrame.addChild(input_text_8);arduinoFrame.addChild(input_text_9);arduinoFrame.addChild(input_text_10);
//			arduinoFrame.addChild(input_text_11);arduinoFrame.addChild(input_text_12);
//			arduinoFrame.addChild(input_lb_1);
//			arduinoFrame.addChild(input_lb_1);arduinoFrame.addChild(input_lb_2);arduinoFrame.addChild(input_lb_3);arduinoFrame.addChild(input_lb_4);arduinoFrame.addChild(input_lb_5);
//			arduinoFrame.addChild(input_lb_6);arduinoFrame.addChild(input_lb_7);arduinoFrame.addChild(input_lb_8);arduinoFrame.addChild(input_lb_9);arduinoFrame.addChild(input_lb_10);
//			arduinoFrame.addChild(input_lb_11);arduinoFrame.addChild(input_lb_12);
			
			addChild(arduinoFrame);
			
			//14. Malcolm
			arduinoFrame.addChild(saveMovBt);
			arduinoFrame.addChild(motor_pwon);
			arduinoFrame.addChild(motor_pwof);

			//
			//15. Malcolm
//			arduinoFrame.addChild(delMovBt);//
//			arduinoFrame.addChild(testMovBt);
			arduinoFrame.addChild(testMovBt2);
						
			paletteFrame.addEventListener(MouseEvent.ROLL_OVER, __onMouseOver);
			paletteFrame.addEventListener(MouseEvent.ROLL_OUT, __onMouseOut);
			paletteIndex = getChildIndex(paletteFrame);
			
			//		arduinoFrame.addEventListener(Event.USER_PRESENT, onUSBcheck);
			
			//
		}
		
	private function onsendTextPaneChange(e: Event):void{
		if(MBlock.app.lang_flag == 'zh_CN'){
		if(sendTextPane.text == "Wonderboy is Ready!") sendTextPane_translated.text ="机器人准备就绪！" ;
		else if(sendTextPane.text == "File deleted!" ) sendTextPane_translated.text = "删除文件！" ;
		else if(sendTextPane.text == "WiFi connection OK, ready to upload or run!") sendTextPane_translated.text = "WiF连接就绪！" ;
		else if(sendTextPane.text == "Run OK!") sendTextPane_translated.text = "运行！";
		else if(sendTextPane.text == "Upload OK!") sendTextPane_translated.text = "上传完成！";
		else if(sendTextPane.text == "Sound Upload OK!") sendTextPane_translated.text = "声音上传完成！";
		else if(sendTextPane.text == "Movlog Upload OK!") sendTextPane_translated.text = "动作上传完成！";
		else if(sendTextPane.text == "Error upload!") sendTextPane_translated.text = "上传错误！";
		else if(sendTextPane.text == "Error Run!") sendTextPane_translated.text = "运行错误！";
		else if(sendTextPane.text == "Error Delete!") sendTextPane_translated.text = "删除文件发生错误！";
		else if(sendTextPane.text == "Please plug in USB cable and Click Init button!") sendTextPane_translated.text = "请插入USB连接线并点击紫色WiFi按钮";
		else if(sendTextPane.text == "Please Unplug USB cable!") sendTextPane_translated.text = "请拔出USB连接线！";
		else if(sendTextPane.text == "Please select a file!") sendTextPane_translated.text = "请选择一个文件！";
		else if(sendTextPane.text == "Device Search not ready!") sendTextPane_translated.text ="机器人连接错误！" ;
		else if(sendTextPane.text == "Device List ready!") sendTextPane_translated.text = "机器人搜索完成！";
		else if(sendTextPane.text == "Sound Upload Error!") sendTextPane_translated.text = "声音上传错误！";
		else if(sendTextPane.text == "Movlog Upload Error!") sendTextPane_translated.text = "动作上传错误！";
		else if(sendTextPane.text == "IP NOK, Not ready to connect WiFi, please try again!") sendTextPane_translated.text = "请再次连接WiFi！";
		else if(sendTextPane.text =="Device Search not ready!" ) sendTextPane_translated.text = "机器人搜索未完成！";
		else if(sendTextPane.text =="Error Pull!" ) sendTextPane_translated.text = "读取文件列表错误！";
		else if(sendTextPane.text =="Not ready to connect WiFi, please try again!" ) sendTextPane_translated.text = "WiFi连接错误，请重试！";
		else if(sendTextPane.text == "Not connected to WiFi, please try again!") sendTextPane_translated.text = "WiFi连接失败，请重试！";
		else if(sendTextPane.text == "Error Stop!") sendTextPane_translated.text = "终止过程发生错误！";
		else if(sendTextPane.text == "Execution Stopped!") sendTextPane_translated.text = "执行终止！";
		else if(sendTextPane.text == "initializing...") sendTextPane_translated.text = "初始化连接...";
		else if(sendTextPane.text == "Check Robot Connection...") sendTextPane_translated.text = "检测连接状态！";
		else if(sendTextPane.text == "Robot is Learning new move...") sendTextPane_translated.text = "机器人在学习新动作！";
		else if(sendTextPane.text == "Robot is Learning new music...") sendTextPane_translated.text = "机器人在学习新音乐！";
		else if(sendTextPane.text == "Ready to connect WiFi! ") sendTextPane_translated.text = "准备连接WiFi！";
		else if(sendTextPane.text == "Connecting WiFi!") sendTextPane_translated.text = "正在连接WiFi！";
		else if(sendTextPane.text == "Device List!") sendTextPane_translated.text = "设备列表！";
		else if(sendTextPane.text == "Reading File List!") sendTextPane_translated.text = "读取文件列表！";
		else if(sendTextPane.text == "Running...") sendTextPane_translated.text = "运行";
		else if(sendTextPane.text == "Delete File!") sendTextPane_translated.text = "删除文件！";
		else if(sendTextPane.text == "Robot is Learning...") sendTextPane_translated.text = "删除文件！";
		else sendTextPane_translated.text =sendTextPane.text;}
		else{
			sendTextPane_translated.text =sendTextPane.text;
		}
		
	}
		
		private var paletteIndex:int;
		private var maskWidth:int;
		
		private function __onMouseOver(event:MouseEvent):void
		{
			setChildIndex(paletteFrame, numChildren-1);
			paletteFrame.addEventListener(Event.ENTER_FRAME, __onEnterFrame);
			maskWidth = 0;
		}
		
		private function __onEnterFrame(event:Event):void
		{
			if(maskWidth < 1200){
				maskWidth += 30;
				paletteFrame.showRightPart(maskWidth);
			}
			if(paletteFrame.mouseX > BlockPalette.WIDTH){
				__onMouseOut(null);
			}
		}
		
		private function __onMouseOut(event:MouseEvent):void
		{
			paletteFrame.removeEventListener(Event.ENTER_FRAME, __onEnterFrame);
			paletteFrame.hideRightPart();
			setChildIndex(paletteFrame, paletteIndex);
		}
		
		//	private function onInputModeChange(evt:MouseEvent):void
		//	{
		//		var str:String = sendTextPane.text;
		//		isByteInputMode = !isByteInputMode;
		//		if(isByteInputMode){
		//			sendTextPane.restrict = "0-9 a-fA-F";
		//			inputModeBtn.setLabel(Translator.map("binary mode"));
		//		}else{
		//			sendTextPane.restrict = null;
		//			inputModeBtn.setLabel(Translator.map("char mode"));
		//		}
		//		if(str.length <= 0){
		//			return;
		//		}
		//		var bytes:ByteArray;
		//		if(isByteInputMode){
		//			bytes = new ByteArray();
		//			bytes.writeUTFBytes(str);
		//			sendTextPane.text = HexUtil.bytesToString(bytes);
		//		}else{
		//			bytes = HexUtil.stringToBytes(str);
		//			sendTextPane.text = bytes.readUTFBytes(bytes.length);
		//		}
		//		bytes.clear();
		//	}
		//	
		//	private function onDisplayModeChange(evt:MouseEvent):void
		//	{
		//		isByteDisplayMode = !isByteDisplayMode;
		//		if(isByteDisplayMode){
		//			displayModeBtn.setLabel(Translator.map("binary mode"));
		//		}else{
		//			displayModeBtn.setLabel(Translator.map("char mode"));
		//		}
		//	}
		public function appendMessage(msg:String):void{
			messageTextPane.textField.appendText(msg+"\n");
			messageTextPane.textField.scrollV = messageTextPane.textField.maxScrollV-1;
		}
		
		public function onSerialSend(bytes:ByteArray):void
		{
			if(!MBlock.app.stageIsArduino){
				return;
			}
			if(isByteDisplayMode){
				appendMsgWithTimestamp(HexUtil.bytesToString(bytes), true);
			}else{
				bytes.position = 0;
				var str:String = bytes.readUTFBytes(bytes.length);
				appendMsgWithTimestamp(str, true);
			}
		}
		
		public function appendMsgWithTimestamp(msg:String, isOut:Boolean):void
		{
			var date:Date = new Date();
			var sendType:String = isOut ? " > " : " < ";
			msg = (date.month+1) + "-" + date.date + " " + date.hours + ":" + date.minutes + ":" + date.seconds + "." +date.milliseconds + sendType + msg;
			appendMessage(msg);
		}
		public function onSerialDataReceived(bytes:ByteArray):void{
			appendMsgWithTimestamp(HexUtil.bytesToString(bytes), false);
			/*
			return;
			var date:Date = new Date;
			var s:String = SerialManager.sharedManager().asciiString;
			if(s.charCodeAt(0)==20){
			return;
			}
			appendMessage(""+(date.month+1)+"-"+date.date+" "+date.hours+":"+date.minutes+":"+(date.seconds+date.milliseconds/1000)+" < "+SerialManager.sharedManager().asciiString.split("\r\n").join("")+"\n");
			*/
		}
		//	private function onSendSerial(evt:MouseEvent):void{
		//		if(!SerialManager.sharedManager().isConnected){
		//			return;
		//		}
		//		var str:String = sendTextPane.text;
		//		if(str.length <= 0){
		//			return;
		//		}
		//		var bytes:ByteArray;
		//		if(isByteInputMode){
		//			bytes = HexUtil.stringToBytes(str);
		//			SerialManager.sharedManager().sendBytes(bytes);
		//		}else{
		//			bytes = new ByteArray();
		//			bytes.writeUTFBytes(str);
		//			SerialManager.sharedManager().sendString(str+"\n");
		//		}
		//		onSerialSend(bytes);
		//		bytes.clear();
		////		var date:Date = new Date;
		////		messageTextPane.append(""+(date.month+1)+"-"+date.date+" "+date.hours+":"+date.minutes+":"+(date.seconds+date.milliseconds/1000)+" > "+sendTextPane.text+"\n");
		//		
		////		messageTextPane.textField.scrollV = messageTextPane.textField.maxScrollV-1;
		//	}
		public function get isArduinoMode():Boolean{
			return arduinoFrame.visible;
		}
		//	private function onCompileArduino(evt:MouseEvent):void{
		//		if(SerialManager.sharedManager().isConnected){
		//			if(ArduinoManager.sharedManager().isUploading==false){
		//				messageTextPane.clear();
		//				if(showArduinoCode()){
		//					messageTextPane.append(ArduinoManager.sharedManager().arduinoInstallPath);
		//					messageTextPane.append(ArduinoManager.sharedManager().buildAll(arduinoTextPane.textField.text));
		//				}
		//			}
		//		}else{
		//			var dialog:DialogBox = new DialogBox();
		//			dialog.addTitle("Message");
		//			dialog.addText("Please connect the serial port.");
		//			function onCancel():void{
		//				dialog.cancel();
		//			}
		//			dialog.addButton("OK",onCancel);
		//			dialog.showOnStage(app.stage);
		//		}
		//	}
		
		//	private function onCompileArduino(evt:MouseEvent):void{
		//		nativeProcessStartupInfo.executable =  file; 
		//		processArgs.push("/C echo 'hello'");
		//		nativeProcessStartupInfo.arguments = processArgs; 
		//		if(1){		//SerialManager.sharedManager().isConnected
		//			if(ArduinoManager.sharedManager().isUploading==false){
		//				messageTextPane.clear();
		//				if(showArduinoCode()){			//Upload 
		//
		//					process.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, onOutputData); 
		//					process.start(nativeProcessStartupInfo);
		//					process.closeInput();
		////					process.exit();
		////					messageTextPane.append(ArduinoManager.sharedManager().arduinoInstallPath);
		////					messageTextPane.append(ArduinoManager.sharedManager().buildAll(arduinoTextPane.textField.text));
		//				}
		//			}
		//		}else{
		//			var dialog:DialogBox = new DialogBox();
		//			dialog.addTitle("Message");
		//			dialog.addText("Please connect the USB port.");
		//			function onCancel():void{
		//				dialog.cancel();
		//			}
		//			dialog.addButton("OK",onCancel);
		//			dialog.showOnStage(app.stage);
		//		}
		//	}
		
		//Upload Scripts
		
		public function onOutputData(event:ProgressEvent):void 
		{ 
			//		trace("Got: ", NativeProcess(event.target).standardOutput.readUTFBytes(process.standardOutput.bytesAvailable)); 
		}
		
		private function onHideArduino(evt:MouseEvent):void{
			app.toggleArduinoMode();
		}
		
		/*
		//{	TCPIP5555:
		//		if(EchoCMD.indexOf(CMP_string) >= 0){
		//check USB connection
		
		//		process.exit();
		//unplug USB cable //---------------dialog box-------------------//
		//						var dialog:DialogBox = new DialogBox();
		//						dialog.addTitle("Message");
		//						dialog.addText("Please unplug USB cable.");
		//						function onCancel():void{
		////							process.exit(true);
		//							if(process.running == true){
		//								trace("TTTTTTTTTTTTTTTTTTT");
		//								process.exit();
		//							}
		//							if(process.running == false){
		//							trace("FFFFFFFFFFFFFFFFFFFFFF");
		//							CMP_string = "connected to";
		////							WinCMD("as_test.cmd");
		//							WinCMD("wifi_connect.cmd");
		//							dialog.cancel();
		//							}
		//						}
		//						dialog.addButton("OK",onCancel);
		//						dialog.showOnStage(app.stage);
		
		

		//wifi connect
		
		
		//		if(1){		//SerialManager.sharedManager().isConnected  
		//			//WinCMD
		//			//if WinCMD's output includes tcpip 5555, returns true, then connect WiFi
		//			
		//			
		//		}else{
		////			var dialog:DialogBox = new DialogBox();
		////			dialog.addTitle("Message");
		////			dialog.addText("Please connect the USB port.");
		////			function onCancel():void{
		////				dialog.cancel();
		////			}
		////			dialog.addButton("OK",onCancel);
		////			dialog.showOnStage(app.stage);
		//		}
		*/
		
		//Steven WiFi
		private var temp_IP:String = new String;
		private var adb_serial:String = " ";
		private var adb_serial_no:String = " ";
		public static var con_status:Boolean = new Boolean;
		public static var wifi_con_status:Boolean = new Boolean;
		
//This function checks if the communication status is "a final state", "final state" is a state where is the end of a communication cycle, here is a list of the final states:
//		true --> is final state
		private 	var text_fs_state: Array = new Array("Wonderboy is Ready!", "File deleted!","WiFi connection OK, ready to upload or run!",
			"Run OK!",
			"Upload OK!",	"Sound Upload OK!",	
			"Movlog Upload OK!","Error upload!","Error Run!",
			"Error Delete!",
			"",
			"Please plug in USB cable and Click Init button!",
			"Please Unplug USB cable!",
			"Please select a file!",
//			"Device List!",
			"Device Search not ready!",
			"Device List ready!",
			"Sound Upload Error!",
			"Movlog Upload Error!",
			"IP NOK, Not ready to connect WiFi, please try again!",
			"Device Search not ready!",
			"Error Pull!",
			"Not ready to connect WiFi, please try again!",
			"Not connected to WiFi, please try again!",
			"Error Stop!",
			"Movement upload OK!",
			"Execution Stopped!", "Power off OK!", "Power on OK!", "Error Power off!", "Error Power on!",
			"Position OK!", "Error Position!", "Error Get Position!");
		private function fs_check(): Boolean{
		if(text_fs_state.indexOf(sendTextPane.text) !=-1)
			return true;
		else
			return false;
		}
		
		//This device check is for normal use, followed up with nothing, just changes device variables.
		private function adb_con_check(...args):void{
//			if(args[0] is MouseEvent){}
//			if(args == null){}
			EchoOK = "Device List!";
			EchoNOK = "Device Search not ready!";
			adb_function(200, preWaiting_function, afterWaiting_function, "List of devices attached", "connect_test.cmd");//changed from 100 to 200, otherwise slow PC would show error.
		}
		
		//This is just for device check when Gblock connection is initialized, because it is followed with pull_mainfile_list
		private function adb_con_check_init(...args):void{
			if(args[0] is MouseEvent){}
			//			if(args == null){}
			EchoOK = "Device Search Done!";
			EchoNOK = "Device Search not ready!";
			adb_function(200, preWaiting_function, afterWaiting_function, "List of devices attached", "connect_test.cmd");
		}
		
		
		//disable all icon buttons
		public function disable_icons(): void{
			startBt_icon.mouseEnabled = false;
			stopBt_icon.mouseEnabled = false;
			deleteBt_icon.mouseEnabled = false;
			uploadBt_icon.mouseEnabled = false;
			uploadBt_icon.mouseEnabled = false;
			
//			saveMovBt.mouseEnabled = false;
			testMovBt2.mouseEnabled = false;
			
			motor_pwon.mouseEnabled = false;
			motor_pwof.mouseEnabled = false;
			
			startBt_icon.turnOn();
			stopBt_icon.turnOn();
			deleteBt_icon.turnOn();
			uploadBt_icon.turnOn();
			uploadBt_icon.turnOn();
			
//			saveMovBt.turnOn();
			testMovBt2.turnOn();
			
			motor_pwon.turnOn();
			motor_pwof.turnOn();
		}
		
		//enable all icon buttons
		private function enable_icons(): void{
			startBt_icon.mouseEnabled = true;
			stopBt_icon.mouseEnabled = true;
			deleteBt_icon.mouseEnabled = true;
			uploadBt_icon.mouseEnabled = true;
			uploadBt_icon.mouseEnabled = true;
			
//			saveMovBt.mouseEnabled = true;
			testMovBt2.mouseEnabled = true;
			
			motor_pwon.mouseEnabled = true;
			motor_pwof.mouseEnabled = true;
			
			startBt_icon.turnOff();
			stopBt_icon.turnOff();
			deleteBt_icon.turnOff();
			uploadBt_icon.turnOff();
			uploadBt_icon.turnOff();
			
//			saveMovBt.turnOff();
			testMovBt2.turnOff();
			
			motor_pwon.turnOff();
			motor_pwof.turnOff();
		}
		
		//Steven WiFi init
		public function onWiFiConnect_timer(evt:MouseEvent):void{
//			MBlock.app.topBarPart.wifi_con_Button.isMomentary= false;
//			MBlock.app.topBarPart.wifi_con_Button.turnOff();
//			MBlock.app.topBarPart.wifi_con_Button.isMomentary = true;
			if( fs_check() ==false)
				return;
			if(sendTextPane.text == "Device List!")  //Device List! is not a totally final state, if another button pressed during DL phase, error will occur
				return;
			adb_con_check();
			var timerupload:Timer = new Timer(delay);
			timerupload = new Timer(700, 1);//
			timerupload.start();
			timerupload.addEventListener(TimerEvent.TIMER, check_device_status);//
			
			function check_device_status(){
				if(con_status == false){
					MBlock.app.topBarPart.addwifibutton_nok("NOK");
					MBlock.app.topBarPart.setDisconnectedTitle();
					//put this line, so that status flow can continue from "Device List ready!"
					sendTextPane.text ="Device Search not ready!";
					//dialog
					dialog_box("Please plug in USB and Click Init WiFi!", "Robot Connection Error!");
					disable_icons();
					return;				
				}
				onWiFiConnect();//onWiFiConnect();
			}
		}
		
		private function onWiFiConnect():void{	//evt:MouseEvent
			//		init	OK			"Ready to connect WiFi, please unplug USB cable! "
			//				NOK			"Not ready to connect WiFi, please try again!"
			EchoOK = "Ready to connect WiFi! ";
			EchoNOK = "Not ready to connect WiFi, please try again!";
			loop_counter = 0;
			adb_function(8500, preWaiting_function, afterWaiting_function, "port: 5555", "init_wifi.cmd");
		}
				
		//Steven WiFi IP address
		private function ongetWiFiIP(evt:MouseEvent):void{
			adb_function(2500, preWaiting_getIP, afterWaiting_getIP, "inet", "get_ip.cmd");
		}
		
		//Steven WiFi connect
		private function onWiFiConnect1(evt:MouseEvent):void{
			//		connect	OK			"WiFi connection OK, ready to upload or run!"
			//				NOK			"Not connected to WiFi, please try again!"
			EchoOK = "WiFi connection OK, ready to upload or run!";
			EchoNOK = "Not connected to WiFi, please try again!";
			adb_function(1000, preWaiting_function, afterWaiting_function, "connected to", "wifi_connect.cmd");
		}
		
		//pull file list  UIfile_list
//		private function pull_filelog(evt:MouseEvent): void{		
		private function pull_filelog(): void{	
//			adb_con_check();
			EchoOK = "Wonderboy is Ready!";//era Voice Command List Ready!
			EchoNOK = "Error Pull!";
			adb_function(1000, preWaiting_function, afterWaiting_function, "100", "pull_main_filelist.cmd");//100 not used, because empty file pulled has no echo.
		}
		
		//read file log, push items into vector
		//Steven, List new UI library
		//		private var dp:ArrayCollection = new ArrayCollection; //Flexlite
		private var dp:Array = new Array; 
		private var dp_project:Array = new Array; 
		
//		private function check_filelog(evt:MouseEvent): void{		//event for development test button
		private static var largest_fileNo: String = new String;
		//update currentJSON.txt after fetch it.
		private function update_currentJSON():void{
			var fs : FileStream = new FileStream();
			var currentJSONpath : File = File.desktopDirectory.resolvePath(File.applicationDirectory.nativePath + "\\Scratch_PC_app_TEST\\" + "currentJSON.txt");
			var currentJSON: String = largest_fileNo; //largest file no. from file list.
			if(Number(currentJSON)==9999999)
				currentJSON = "1000000";
			fs.open(currentJSONpath, "write");
			fs.writeUTFBytes((Number(currentJSON)+1).toString());
			fs.close();
		}
		
		private function check_filelog(): void{	
			var fs : FileStream = new FileStream();
			var filelogpath : File = File.desktopDirectory.resolvePath(File.applicationDirectory.nativePath + "\\Scratch_PC_app_TEST\\Androidfilelist\\" + "task.gwb");
			fs.open(filelogpath, "read");
			var filelog_buffer: String = fs.readMultiByte(fs.bytesAvailable,'utf-8');
//			
			var pattern_mapping:RegExp = /.*\d\d\d\d\d\d\d/gi ;// should be able to check .cet\n, now this is not perfect, pending!!!
			var result_mapping:Array = new Array;
		
			try{
//				dp.removeAll();//Flexlite
//				UIfile_list.dataProvider = dp; //Flexlite
				UIfile_list.removeAll();
				dp = new Array;
				dp_project = new Array;
				
				var i: int = 0;//counter for all items
				var l: int = 0;//counter for listed items
				
				while (result_mapping != null) 				
				{ 
					result_mapping = pattern_mapping.exec(filelog_buffer); 
					var slice_string: String = new String;
					slice_string = result_mapping[0];
					slice_string = slice_string.slice(0,-8);//-7 was without en zh
					
					trace(dp.length, "result_mapping  :", result_mapping);
					if(slice_string == "testmovement999/en/temp_project"){
						if(i ==0)
							largest_fileNo = result_mapping[0].slice(-7);
						
						i++;
						continue;
					}
					

						
					
//			123		dp_project.push(slice_string);//push voice cmd
			
			//add spaces to make the word in the middle
			var  space_number: int = 0;
			var slice_string_length: int = 0;
			var space_between: String = ""; 
			var  voice_cmd_string: String = ""; 
			
			if(slice_string.indexOf("/en/") != -1){
				slice_string_length = slice_string.indexOf("/en/");
				voice_cmd_string = slice_string.slice(0,slice_string_length);
				dp.push(voice_cmd_string);//push voice cmd
			}		
			else  if(slice_string.indexOf("/zh/") != -1){
				slice_string_length = slice_string.indexOf("/zh/");
				voice_cmd_string = slice_string.slice(0,slice_string_length);
				dp.push(voice_cmd_string);//push voice cmd
			}

			
			//space problem can't be perfectly soloved by using this way.
			while(space_number<(25-slice_string_length/2)){
				slice_string = " "+slice_string;
				space_number++;
				space_between = space_between + " ";
			}
				
			if(slice_string.indexOf("/en/") != -1)
				slice_string = slice_string.replace("/en/", space_between);
			else  if(slice_string.indexOf("/zh/") != -1)
				slice_string = slice_string.replace("/zh/", space_between);
			
			
					UIfile_list.addItem({ label: "	"+ (l+1).toString()  +slice_string }) ;
					
//	 				if (i >0)
//						UIfile_list.selectedIndex = 0;//always set the default selected item to the top 1.
					
					if(i ==0)
						largest_fileNo = result_mapping[0].slice(-7);
						
					i++;
					l++;
				} 
			}catch(e: TypeError)
			{}

//			UIfile_list.dataProvider = dp;//Flexlite
			fs.close();
			
			update_currentJSON();
		}
		
		//Delete file
		public function delete_file_timer(evt:MouseEvent):void{
			if( fs_check() ==false)
				return;
			if(sendTextPane.text == "Device List!")  //Device List! is not a totally final state, if another button pressed during DL phase, error will occur
				return;
			adb_con_check();
			var timerupload:Timer = new Timer(delay);
			timerupload = new Timer(700, 1);//
			timerupload.start();
			timerupload.addEventListener(TimerEvent.TIMER, check_device_status);//
						
			function check_device_status(){
				if(con_status == false){
					MBlock.app.topBarPart.addwifibutton_nok("NOK");
					MBlock.app.topBarPart.setDisconnectedTitle();
					//put this line, so that status flow can continue from "Device List ready!"
					sendTextPane.text ="Device Search not ready!";
					//dialog
					dialog_box("Please plug in USB and Click Init WiFi button!", "Robot Connection Error!");
					disable_icons();
				}

				else if(con_status == true){
					MBlock.app.topBarPart.addwifibutton_nok("OK");
					MBlock.app.topBarPart.setConnectedTitle("WiFi Connected!");
					enable_icons();
					if(UIrunfelstr == null){
						//put this line, so that status flow can continue from "Device List ready!"
						sendTextPane.text ="Device Search not ready!";
						dialog_box("Please select a file!", "No file selected!");
						return;
					}	
					delete_file();//delete_file();
				}

			}
		}
		private function delete_file(): void{		//evt:TimerEvent
			timerupload.stop();
			timerupload.removeEventListener(TimerEvent.TIMER, delete_file);
			EchoOK = "File deleted!";
			EchoNOK = "Error Delete!";
			adb_function(2000, preWaiting_function, afterWaiting_function, "Broadcast completed", "delete_file.cmd");	
		}
		
		//Run file
//		private var fscounter: int = 0;
		public function run_file_timer(evt:MouseEvent):void{//evt:MouseEvent
			if( fs_check() ==false){
//				fscounter++;
				return;
			}
			
			if(sendTextPane.text == "Device List!")  //Device List! is not a totally final state, if another button pressed during DL phase, error will occur
				return;
			//text log to see what happened when there should not be error
//			var fs : FileStream = new FileStream();
//			var text_comm : File = File.desktopDirectory.resolvePath(File.applicationDirectory.nativePath + "\\text_comm.log");
//			fs.open(text_comm, FileMode.APPEND);
//				fs.writeUTFBytes(sendTextPane.text);
////				fs.writeUTFBytes(fscounter.toString());
//				fs.close();
			
			
			adb_con_check();
			var timerupload:Timer = new Timer(delay);
			timerupload = new Timer(700, 1);//
			timerupload.start();
			timerupload.addEventListener(TimerEvent.TIMER, check_device_status);//
			
			function check_device_status(){
				if(con_status == false){
					MBlock.app.topBarPart.addwifibutton_nok("NOK");
					MBlock.app.topBarPart.setDisconnectedTitle();
					//put this line, so that status flow can continue from "Device List ready!"
					sendTextPane.text ="Device Search not ready!";
					//dialog
					dialog_box("Please plug in USB cable and Click Init WiFi button!", "Robot Connection Error!");
					disable_icons();
				}
				else if( adb_serial_no == "0123456789ABCDEF"){
					//put this line, so that status flow can continue from "Device List ready!"
					sendTextPane.text ="Device Search not ready!";
					dialog_box("Please Unplug USB cable!", "Unplug USB!");
					return;
				}
				else if(con_status == true){
					MBlock.app.topBarPart.addwifibutton_nok("OK");
					MBlock.app.topBarPart.setConnectedTitle("WiFi Connected!");
					enable_icons();
					if(UIrunfelstr == null){
						//put this line, so that status flow can continue from "Device List ready!"
						sendTextPane.text ="Device Search not ready!";
						dialog_box("Please select a file!", "No file selected!");
						return;
					}				
					run_file();//delete_file();  run_file();
				}
					
			}
		}
		private function run_file(): void{	//evt:TimerEvent
			sendTextPane.text =  "Running...";//Add this message, so that textfield can go out from final status "immediately"
			timerupload.stop();
			timerupload.removeEventListener(TimerEvent.TIMER, run_file);
			EchoOK = "Run OK!";
			EchoNOK = "Error Run!";
			adb_function(1000, preWaiting_function, afterWaiting_function, "am broadcast", "run_file.cmd");//Broadcast completed
		}
		
		//load pos
		public function load_pos_timer():void{//evt:MouseEvent
			if( fs_check() ==false){
				return;
			}
			
			if(sendTextPane.text == "Device List!")  //Device List! is not a totally final state, if another button pressed during DL phase, error will occur
				return;
			
			adb_con_check();
			var timerupload:Timer = new Timer(delay);
			timerupload = new Timer(700, 1);//
			timerupload.start();
			timerupload.addEventListener(TimerEvent.TIMER, check_device_status);//
			
			function check_device_status(){
				timerupload.stop();
				timerupload.removeEventListener(TimerEvent.TIMER, check_device_status);
				
				if(con_status == false){
					MBlock.app.topBarPart.addwifibutton_nok("NOK");
					MBlock.app.topBarPart.setDisconnectedTitle();
					//put this line, so that status flow can continue from "Device List ready!"
					sendTextPane.text ="Device Search not ready!";
					//dialog
					dialog_box("Please plug in USB cable and Click Init WiFi button!", "Robot Connection Error!");
					disable_icons();
				}
				else if( adb_serial_no == "0123456789ABCDEF"){
					//put this line, so that status flow can continue from "Device List ready!"
					sendTextPane.text ="Device Search not ready!";
					dialog_box("Please Unplug USB cable!", "Unplug USB!");
					return;
				}
				else if(con_status == true){
					MBlock.app.topBarPart.addwifibutton_nok("OK");
					MBlock.app.topBarPart.setConnectedTitle("WiFi Connected!");
					enable_icons();
//					if(UIrunfelstr == null){
//						//put this line, so that status flow can continue from "Device List ready!"
//						sendTextPane.text ="Device Search not ready!";
//						dialog_box("Please select a file!", "No file selected!");
//						return;
//					}				
					get_pos();//delete_file();  run_file();
				}
				
			}
		}
		
		//send broadcast to get pos
		private function get_pos(): void{	//evt:TimerEvent
			sendTextPane.text =  "Loading...";//Add this message, so that textfield can go out from final status "immediately"
			EchoOK = "Loading Position!";
			EchoNOK = "Error Get Position!";
			adb_function(1000, preWaiting_function, afterWaiting_function, "am broadcast", "get_pos.cmd");//Broadcast completed
		}
		
		//load from file
		private function load_pos(): void{	//evt:TimerEvent
			var timerupload:Timer = new Timer(delay);
			timerupload = new Timer(4000, 1);//
			timerupload.start();
			timerupload.addEventListener(TimerEvent.TIMER, load_pos_delay);//
			
			function load_pos_delay(){
			sendTextPane.text =  "Loading......";//Add this message, so that textfield can go out from final status "immediately"
			timerupload.stop();
			timerupload.removeEventListener(TimerEvent.TIMER, load_pos_delay);
			EchoOK = "Position OK!";
			EchoNOK = "Error Position!";
			adb_function(1500, preWaiting_function, afterWaiting_function, "#1P", "load_pos.cmd");//Broadcast completed
			}
		}
		
		
		//test movement
		private function run_tm_file_timer():void{//evt:MouseEvent
			if( fs_check() ==false)
				return;
			if(sendTextPane.text == "Device List!")  //Device List! is not a totally final state, if another button pressed during DL phase, error will occur
				return;
			adb_con_check();
			var timerupload:Timer = new Timer(delay);
			timerupload = new Timer(700, 1);//
			timerupload.start();
			timerupload.addEventListener(TimerEvent.TIMER, check_device_status);//
			
			function check_device_status(){
				if(con_status == false){
					MBlock.app.topBarPart.addwifibutton_nok("NOK");
					MBlock.app.topBarPart.setDisconnectedTitle();
					//put this line, so that status flow can continue from "Device List ready!"
					sendTextPane.text ="Device Search not ready!";
					//dialog
					dialog_box("Please plug in USB cable and Click Init WiFi button!", "Robot Connection Error!");
					disable_icons();
				}
				else if( adb_serial_no == "0123456789ABCDEF"){
					//put this line, so that status flow can continue from "Device List ready!"
					sendTextPane.text ="Device Search not ready!";
					dialog_box("Please Unplug USB cable!", "Unplug USB!");
					return;
				}
				else if(con_status == true){
					MBlock.app.topBarPart.addwifibutton_nok("OK");
					MBlock.app.topBarPart.setConnectedTitle("WiFi Connected!");
//					if(UIrunfelstr == null){
//						dialog_box("Please select a file!", "No file selected!");
//						return;
//					}				
					enable_icons();
					run_tm_file();
				}
				
			}
		}
		//test movement
		private function run_tm_file(): void{	
			timerupload.stop();
			timerupload.removeEventListener(TimerEvent.TIMER, run_tm_file);
			EchoOK = "Run OK!";
			EchoNOK = "Error Run!";
			adb_function(1000, preWaiting_function, afterWaiting_function, "am broadcast", "run_tm_file.cmd");//Broadcast completed
		}
		
		//test movement upload
		private var tm_timerupload:Timer = new Timer(200);
		private function tm_onuploadwbBt_timer():void{//evt:MouseEvent
			if( fs_check() ==false)
				return;
			adb_con_check();
			tm_timerupload = new Timer(700, 1);//
			tm_timerupload.start();
			tm_timerupload.addEventListener(TimerEvent.TIMER, check_device_status);//
			
			function check_device_status(){
				if(con_status == false){
					//dialog
					MBlock.app.topBarPart.addwifibutton_nok("NOK");
					MBlock.app.topBarPart.setDisconnectedTitle();
					//put this line, so that status flow can continue from "Device List ready!"
					sendTextPane.text ="Device Search not ready!";
					dialog_box("Please plug in USB and Click Init WiFi button!", "Robot Connection Error!");
					disable_icons();
				}
				else if(con_status == true){
					MBlock.app.topBarPart.addwifibutton_nok("OK");
					MBlock.app.topBarPart.setConnectedTitle("WiFi Connected!");
					enable_icons();
					tm_onuploadwbBt();//delete_file();  run_file(); onuploadwbBt();
				}
				
			}
		}
		private function tm_onuploadwbBt():void{//evt:TimerEvent
			timerupload.stop();
			timerupload.removeEventListener(TimerEvent.TIMER, tm_onuploadwbBt);
//			if(null == app.projectFile){
//				//				Error Dialog
//				dialog_box("No Project! Please create one!", "Error");
//				return;
//			}
//			var pattern:RegExp = /[\s\`\~\!\@\#\$\%\^\&\*\(\)\_\-\=\+\[\]\{\}\|\;\:\'\"\,\.\<\>\/\?\\]/;
//			if(pattern.test(extensions.ArduinoManager.filename_voice)== true){
//				//				Error Dialog
//				dialog_box("No  punctuation characters and whitespace allowed in Voice Command!", "Wrong Voice Command!");
//				return;
//			}
//			if(dp.indexOf(extensions.ArduinoManager.filename_voice) != -1){
//				//				Overwrite Dialog
//				dialog_box("Voice command \"" + extensions.ArduinoManager.filename_voice + "\" already exists in Robot, overwrite it?", "Overwrite Existed File?");
//				return;
//			}
			
			upload_tm_file();
		}
		
		private function upload_tm_file():void{
				//save script. save pj first if which has not been saved.
//			if(app._saveNeeded)
//				app.saveFile();
			
			extensions.ArduinoManager.filename_voice = "testmovement999";
			MBlock.app.savescript_tm();							
			prg_file = MBlock.scriptpath;//script file to be uploaded
			prg_filename = MBlock.scriptname;

			//Check *.vlog
			movlogfile_num = 0;
			var pattern_movlog:RegExp = /\"[0-9a-zA-Z()]+\.log/gi ;
			var result_movlog:Array = new Array;
//			try{
//				while (result_movlog != null) 			
//				{ 
//					result_movlog = pattern_movlog.exec(textpaneCode); 
//					movlogfile_list[movlogfile_num] = result_movlog[0].slice(1);
//					trace(movlogfile_num.toString(), "result_movlog  :", movlogfile_list[movlogfile_num]);
//					movlogfile_num++;
//				} 
//			}catch(e: TypeError)
//			{}
			result_movlog = pattern_movlog.exec("\{\"actionsBefore\"\:\[\{\"leg\"\:\[\"testmovement999\.log\"\]\}\]\}"); 
			movlogfile_list[movlogfile_num] = result_movlog[0].slice(1);
			trace(movlogfile_num.toString(), "result_movlog  :", movlogfile_list[movlogfile_num]);
			movlogfile_num++;
			
			loop_counter = 0;
			EchoOK ="Upload OK!";//"Movement upload OK!";  // 
			EchoNOK = "Error upload!";
			adb_function(2000, preWaiting_function, afterWaiting_function, "100", "push_tm_cet.cmd");		
//			run_tm_file_timer();
		}
		
		
		//Stop file
		public function stop_file_timer(evt:MouseEvent):void{
			if( fs_check() ==false)
				return;
			if(sendTextPane.text == "Device List!")  //Device List! is not a totally final state, if another button pressed during DL phase, error will occur
				return;
			adb_con_check();
			var timerupload:Timer = new Timer(delay);
			timerupload = new Timer(700, 1);//
			timerupload.start();
			timerupload.addEventListener(TimerEvent.TIMER, check_device_status);//
			
			function check_device_status(){
				if(con_status == false){
					MBlock.app.topBarPart.addwifibutton_nok("NOK");
					MBlock.app.topBarPart.setDisconnectedTitle();
					//put this line, so that status flow can continue from "Device List ready!"
					sendTextPane.text ="Device Search not ready!";
					//dialog
					dialog_box("Please plug in USB and Click Init WiFi button!", "Robot Connection Error!");
					disable_icons();
				}
				else if(con_status == true){
					MBlock.app.topBarPart.addwifibutton_nok("OK");
					MBlock.app.topBarPart.setConnectedTitle("WiFi Connected!");
					enable_icons();
					stop_file();//stop_file();
				}
			}
		}
		private function stop_file(): void{		//evt:TimerEvent
			timerupload.stop();
			timerupload.removeEventListener(TimerEvent.TIMER, stop_file);
			EchoOK = "Execution Stopped!";
			EchoNOK = "Error Stop!";
			adb_function(2000, preWaiting_function, afterWaiting_function, "Broadcast completed", "stop_file.cmd");	
		}
		
		
			
		private var soundfile_num: int = 0;
		private var soundfile_list = new Vector.<String>();
		private var soundfilelog_list = new Vector.<String>();
		private var movlogfile_num: int = 0;
		private var movlogfile_list = new Vector.<String>();
		private var wheellogfile_num: int = 0;
		private var wheellogfile_list = new Vector.<String>();
		
		private var timerupload:Timer = new Timer(200);
		public function onuploadwbBt_timer(evt:MouseEvent):void{
			if( fs_check() ==false)
				return;
			if(sendTextPane.text == "Device List!")  //Device List! is not a totally final state, if another button pressed during DL phase, error will occur
				return;
			
//			text log to see what happened when there should not be error
//						var fs : FileStream = new FileStream();
//						var text_comm : File = File.desktopDirectory.resolvePath(File.applicationDirectory.nativePath + "\\text_comm.log");
//						fs.open(text_comm, FileMode.APPEND);
//							fs.writeUTFBytes(sendTextPane.text);
//			//				fs.writeUTFBytes(fscounter.toString());
//							fs.close();
			
			adb_con_check();
			timerupload = new Timer(700, 1);//
			timerupload.start();
			timerupload.addEventListener(TimerEvent.TIMER, check_device_status);//
			
			function check_device_status(){
				if(con_status == false){
					//dialog
					MBlock.app.topBarPart.addwifibutton_nok("NOK");
					MBlock.app.topBarPart.setDisconnectedTitle();
					//put this line, so that status flow can continue from "Device List ready!"
					sendTextPane.text ="Device Search not ready!";
					dialog_box("Please plug in USB and Click Init WiFi button!", "Robot Connection Error!");
					disable_icons();
				}
				else if(con_status == true){
					MBlock.app.topBarPart.addwifibutton_nok("OK");
					MBlock.app.topBarPart.setConnectedTitle("WiFi Connected!");
					enable_icons();
					onuploadwbBt();//delete_file();  run_file(); onuploadwbBt();
				}

			}
		}
		private function onuploadwbBt():void{//evt:TimerEvent
			timerupload.stop();
			timerupload.removeEventListener(TimerEvent.TIMER, onuploadwbBt);
			
			//the following if check is commented out due to "no project" upload feature, it means that even no project was created the script can still be uploaded.
//			if(null == app.projectFile){
//
//				//put this line, so that status flow can continue from "Device List ready!"
//				sendTextPane.text ="Device Search not ready!";
//				//Error Dialog
//				dialog_box("No Project! Please create one!", "Error");
//				return;
//			}
			
			var pattern:RegExp = /[\`\~\!\@\#\$\%\^\&\*\(\)\_\-\=\+\[\]\{\}\|\;\:\'\"\,\.\<\>\/\?\\]/;//\s space
						if(pattern.test(extensions.ArduinoManager.filename_voice)== true){
							//put this line, so that status flow can continue from "Device List ready!"
							sendTextPane.text ="Device Search not ready!";
				//				Error Dialog
				dialog_box("No  punctuation characters and whitespace allowed in Voice Command!", "Wrong Voice Command!");
				return;
			}
			if(dp.indexOf(extensions.ArduinoManager.filename_voice) != -1){
				//put this line, so that status flow can continue from "Device List ready!"
				sendTextPane.text ="Device Search not ready!";
				//				Overwrite Dialog
//				dialog_box("Voice command \"" + extensions.ArduinoManager.filename_voice + "\" already exists in Robot, overwrite it?", "Overwrite Existed File?");
				dialog_box("Voice command already exists in Robot, overwrite it?", "Overwrite Existed File?");
				return;
			}
			if(extensions.ArduinoManager.file_name_FLAG_result==false){
				//put this line, so that status flow can continue from "Device List ready!"
				sendTextPane.text ="Device Search not ready!";
				//				No Cap warning
				dialog_box("Please put a Voice Command block!", "No WIncubator Probram!");
				return;
			}
			if(extensions.ArduinoManager.file_name_FLAG_result==true)//set the flag back to false, because it becomes to true only when there is a cap, but when there is no cap it doesn't change back to false automatically. so here I put this line to reset it to false.
				extensions.ArduinoManager.file_name_FLAG_result=false;
			upload_files();
		}
		
		private function upload_files():void{

//			EchoOK = "Robot Connected! ";
//			EchoNOK = "No Robot Connected!";
//			loop_counter = 0;
//			adb_function(1500, afterWaiting_function, "0123456789", "conect_test.cmd");
			
			//save script. save pj first if which has not been saved.
			if((app._saveNeeded)&&(null != app.projectFile))
				app.saveFile();
			MBlock.app.savescript();							
			prg_file = MBlock.scriptpath;//script file to be uploaded
			prg_filename = MBlock.scriptname;
//			prg_file = 'D:\\Users\\xin\\AppData\\Local\\Android\\sdk\\platform-tools\\GT_Scratch_Home\\Scratch_PC_app_TEST\\testfile.sh';//script file to be uploaded
//			prg_filename = 'testfile.sh';

//Check *.wav, was for godlike script
//			soundfile_num = 0;
//			var pattern_wav:RegExp = /[0-9a-zA-Z()]+\.wav/gi ; //no space allowed here!!
////			var pattern_array: Array = textpaneCode.match(pattern_wav);
//			var result:Array = new Array;
//			try{
//				while (result != null) 
//			
//			{ 
//				result = pattern_wav.exec(textpaneCode); //need to check repetitive file name
//				soundfile_list[soundfile_num] = result[0];
//				trace(soundfile_num.toString(), "result  :", result[0]);
//				soundfile_num++;
//			} 
//		}catch(e: TypeError)
//		{}
			
			//Check *#wheel.log
//			wheellogfile_num = 0;
			movlogfile_num = 0;//use the same counter and Array for wheel log and mov log, to simplify upload part.
			var pattern_wheel:RegExp = /[0-9a-zA-Z()]+\#wheel\.log/gi ; //no space allowed here!!
			//			var pattern_array: Array = textpaneCode.match(pattern_wav);
			var result:Array = new Array;
			try{
				while (result != null) 			
				{ 
					result = pattern_wheel.exec(textpaneCode); //need to check repetitive file name
					
					trace(movlogfile_num.toString(), "result  #wheel\.log:", result[0]);				
					var sharp_pos: int = result[0].indexOf("#");
					var wheellog_name: String = result[0].slice(0, sharp_pos);
					movlogfile_list[movlogfile_num] =  wheellog_name + "#wheel.log";//Shared with movlog
					
					var fs : FileStream = new FileStream();
					var wheel_logpath : File = File.desktopDirectory.resolvePath(File.applicationDirectory.nativePath + "\\movlog\\" + wheellog_name + "#wheel.log");
					fs.open(wheel_logpath, "write");
					if(wheellog_name.search("forward")!=-1){
						var pos_f: int = wheellog_name.search("forward");
						var slice_f: String = wheellog_name.slice(pos_f+7);
						fs.writeUTFBytes("#\n#\n#\n#\n#\n#\n#\n#\n" + "111 -111 " + slice_f);
					}else if(wheellog_name.search("backward")!=-1){
						var pos_b: int = wheellog_name.search("backward");
						var slice_b: String = wheellog_name.slice(pos_b+8);
						fs.writeUTFBytes("#\n#\n#\n#\n#\n#\n#\n#\n" + "-111 111 " + slice_b);
					}
					
					fs.close();
//					wheellogfile_num++;
					movlogfile_num++;
					
				} 
			}catch(e: TypeError)
			{}
				
			//Check *#led.log
			//			wheellogfile_num = 0;
//			movlogfile_num = 0;//use the same counter and Array for wheel log and mov log, to simplify upload part.
			var pattern_led:RegExp = /[0-9a-zA-Z()]+\#led\.log/gi ; //no space allowed here!!
			//			var pattern_array: Array = textpaneCode.match(pattern_wav);
			var result:Array = new Array;
			try{
				while (result != null) 			
				{ 
					result = pattern_led.exec(textpaneCode); //need to check repetitive file name					
					trace(movlogfile_num.toString(), "result  #led\.log:", result[0]);				
					var sharp_pos: int = result[0].indexOf("#");
					var ledlog_name: String = result[0].slice(0, sharp_pos);
					movlogfile_list[movlogfile_num] =  ledlog_name + "#led.log";//Shared with movlog
					
					var fs : FileStream = new FileStream();
					var led_logpath : File = File.desktopDirectory.resolvePath(File.applicationDirectory.nativePath + "\\movlog\\" + ledlog_name + "#led.log");
					fs.open(led_logpath, "write");
					if(ledlog_name.search("on")!=-1){
						var pos_f: int = ledlog_name.search("on");
						var slice_f: String = ledlog_name.slice(pos_f+2);
						fs.writeUTFBytes("#\n#\n#\n#\n#\n#\n#\n#\n" + "1/127/127/127 1/63 1/127/127/127 " + slice_f);
					}else if(ledlog_name.search("off")!=-1){
						var pos_b: int = ledlog_name.search("off");
						var slice_b: String = ledlog_name.slice(pos_b+3);
						fs.writeUTFBytes("#\n#\n#\n#\n#\n#\n#\n#\n" + "-1/127/127/127 -1/63 -1/127/127/127 "+ slice_b);
					}
					
					fs.close();
					//					wheellogfile_num++;
					movlogfile_num++;
					
				} 
			}catch(e: TypeError)
			{}
			
			//Check *#music.log
			soundfile_num = 0;
			var pattern_wav:RegExp = /\"[0-9a-zA-Z()\s\-\,\.\!\_\^\&\@\'\;\~\`\#\$]+\#music\.log/gi ; //no space allowed here!!
			//			var pattern_array: Array = textpaneCode.match(pattern_wav);
			var result:Array = new Array;
			try{
				while (result != null) 			
				{ 
					result = pattern_wav.exec(textpaneCode); //need to check repetitive file name			
					trace(soundfile_num.toString(), "result  #music\.log:", result[0]);				
					var sharp_pos: int = result[0].indexOf("#");
					var music_name: String = result[0].slice(0, sharp_pos);  
					
					music_name = music_name.slice(1);//cut off "
					
					soundfilelog_list[soundfile_num] =  music_name + "#music.log";
					soundfilelog_list[soundfile_num+1] = music_name + ".wav";
//					soundfile_list[soundfile_num] = music_name + ".wav";
					
					var fs : FileStream = new FileStream();
					var music_logpath : File = File.desktopDirectory.resolvePath(File.applicationDirectory.nativePath + "\\music\\" + music_name + "#music.log");
					fs.open(music_logpath, "write");
					fs.writeUTFBytes("#\n#\n#\n#\n#\n#\n#\n#\n" + music_name+".wav");
					fs.close();
					soundfile_num++;			
					soundfile_num++;			
				} 
			}catch(e: TypeError)
			{}
			
			
//Check *.movlog
//			movlogfile_num = 0;
			var pattern_movlog:RegExp = /\"[0-9a-zA-Z()\一-\龥]+\.log/gi ;
			var result_movlog:Array = new Array;
			try{
				while (result_movlog != null) 
					
				{ 
					result_movlog = pattern_movlog.exec(textpaneCode); 
					movlogfile_list[movlogfile_num] = result_movlog[0].slice(1);
					trace(movlogfile_num.toString(), "result_movlog  :", movlogfile_list[movlogfile_num]);
					movlogfile_num++;
				} 
			}catch(e: TypeError)
			{}
			
			loop_counter = 0;
			EchoOK = "Upload OK!";
			EchoNOK = "Error upload!";
//			adb_function(2000, preWaiting_function, afterWaiting_function, "100", "push_prg.cmd");//godlike code
			adb_function(2000, preWaiting_function, afterWaiting_function, "100", "push_cet.cmd");		
		}
		

		
		private function onrunBt(evt:MouseEvent):void{
//			EchoOK = "Unplug USB!";
//			EchoNOK = "Ready to run!";
//			loop_counter = 0;
//			adb_function(1500, afterWaiting_function, "0123456789", "conect_test.cmd");
			
			//			prg_file = 'testfile.sh';//script file to be uploaded
			prg_file = MBlock.scriptname + ".sh";
			wincmd_public = "exe_prg.cmd";
			WinCMD(wincmd_public);
			
//			EchoOK = "Running!";
//			EchoNOK = "Not Running!";
//			adb_function(1000, afterWaiting_function, "Intent", "exe_prg.cmd");
		}
		
		//abortBt
		private function onabortBt(evt:MouseEvent):void{
			wincmd_public = "abort_prg.cmd";
			WinCMD(wincmd_public);
		}
		
		//adb functions
		public function adb_function(timer_delay: Number, pre_waiting: Function, aft_waiting: Function, cmp_string: String, wincmd: String ):void{
			if (process.running ==1){
				trace("Native Process is running!!!");
				process.exit();
			}
			
//			wincmd_public
			wincmd_public = wincmd;
			CMP_string = cmp_string;//
			right_opt = 0;
			pre_waiting();
			//timer
			timer = new Timer(timer_delay);//
			timer.start();
			timer.addEventListener(TimerEvent.TIMER, aft_waiting);//
						
//			parameter list
//			init		5500, afterWaiting_init, "successfully", "init_wifi.cmd"
//			getwifiip	2500, afterWaiting_getIP, "inet", "get_ip.cmd"
//			wificonnect	1000, afterWaiting_connect, "connected to", "wifi_connect.cmd"
//					
		}
		
		
		private var process:NativeProcess = new NativeProcess(); 	
		private var EchoCMD : String = new String; //
		private var CMP_string : String = new String;
		private var nativeProcessStartupInfo:NativeProcessStartupInfo = new NativeProcessStartupInfo(); 
		private var processArgs:Vector.<String> = new Vector.<String>(); 
		
		//file path for upload
		private var prg_file : String = new String;
		private var prg_filename : String = new String;
		private var prg_filename2 : String = new String;
		
		//timer
		private var delay:uint = 6000;
		private var timer:Timer = new Timer(delay);
		
		//IP Wonderboy
		private var ip_Wonderboy : String = new String;
		private var resultIP:Array = new Array;
		
		//installation path
		private var installationpath:String = File.applicationDirectory.nativePath;
		
		private var UIdelfelstr: String;
		private var UIrunfelstr: String;
		//Execute one Windows CMD
		public function WinCMD(WinCMD_string:String):void { 
			processArgs = new Vector.<String>(); 
			process = new NativeProcess(); 
			var file:File = File.applicationDirectory.resolvePath(installationpath + "\\Scratch_PC_app_TEST\\"+WinCMD_string); 
			//			var file:File = File.applicationDirectory.resolvePath("D:\\Users\\xin\\AppData\\Local\\Android\\sdk\\platform-tools\\GT_Scratch_Home\\Scratch_PC_app_TEST\\"+WinCMD_string); 
			nativeProcessStartupInfo.executable =  file; 
			//		processArgs.push('/C D:\\Users\\xin\\AppData\\Local\\Android\\sdk\\platform-tools\\GT_Scratch_Home\\Scratch_PC_app_TEST\\as_test.bat\\adb.exe tcpip 5555');
			//			processArgs[0] = 'D:\\Users\\xin\\AppData\\Local\\Android\\sdk\\platform-tools\\GT_Scratch_Home\\Scratch_PC_app_TEST\\adb.exe';
			processArgs[0] = installationpath + '\\Scratch_PC_app_TEST\\adb.exe';
			//test for space problem
//			processArgs[0] = "\"" + processArgs[0] + "\"";
			
			if(processArgs[0].indexOf(" ") != -1)
			processArgs[0] = processArgs[0].split(" ").join("${SPACE}");
//			else
//				processArgs[0] = "${SPACE}" + processArgs[0];//if no space found in path, just add it to the end of the string.
			//push main program
//			if(WinCMD_string.indexOf("push_prg.cmd") >= 0){//godlike code
			if(WinCMD_string.indexOf("push_cet.cmd") >= 0){
				sendTextPane.text = "";
				sendTextPane.text =("Robot is Learning...");
				processArgs[1] = prg_file;
			if(processArgs[1].indexOf(" ") != -1)
				processArgs[1] = processArgs[1].split(" ").join("${SPACE}");
				processArgs[2] = MBlock.WBjsonname_project;
				if(processArgs[2].indexOf(" ") != -1)
					processArgs[2] = processArgs[2].split(" ").join("${SPACE}");//voice command
				processArgs[3] = adb_serial;//adb_serial;
				processArgs[4] = adb_serial_no;//adb_serial_no
				//				processArgs[2] = prg_filename + ".sh";	
			}
			//push test movement code  push_tm_cet.cmd
			if(WinCMD_string.indexOf("push_tm_cet.cmd") >= 0){
				sendTextPane.text = "";
				sendTextPane.text =("Robot is Learning...");
				processArgs[1] = prg_file;
				if(processArgs[1].indexOf(" ") != -1)
					processArgs[1] = processArgs[1].split(" ").join("${SPACE}");
				processArgs[2] = MBlock.WBjsonname;//WBjsonname_movement   //WBjsonname
				processArgs[3] = adb_serial;//adb_serial;
				processArgs[4] = adb_serial_no;//adb_serial_no
				//				processArgs[2] = prg_filename + ".sh";	
			}
			//execute (run) main program
			else if(WinCMD_string.indexOf("exe_prg.cmd") >= 0){
				sendTextPane.text = "";
				sendTextPane.text = "Running!";
				processArgs[1] = prg_file;
			}
			//abort main program
			else if(WinCMD_string.indexOf("abort_prg.cmd") >= 0){
				sendTextPane.text = "";
				sendTextPane.text = "Probram Terminated!";
			}
			//connect to WiFi
			else if(WinCMD_string.indexOf("wifi_connect.cmd") >= 0){
				sendTextPane.text = "";
				sendTextPane.text = "Connecting WiFi!";
				processArgs[1] = resultIP[0];
			}
			//pull main program folder file list
			else if(WinCMD_string.indexOf("pull_main_filelist.cmd") >= 0){
				sendTextPane.text = "";
				sendTextPane.text = "Reading File List!";
				processArgs[1] = File.applicationDirectory.nativePath + "\\Scratch_PC_app_TEST\\Androidfilelist\\";
				if(processArgs[1].indexOf(" ") != -1)
					processArgs[1] = processArgs[1].split(" ").join("${SPACE}");
				processArgs[2] = adb_serial;//
				processArgs[3] = adb_serial_no;//
			}
			//delete file from list
			else if(WinCMD_string.indexOf("delete_file.cmd") >= 0){
				sendTextPane.text = "";
				sendTextPane.text =("Delete File!");
				//exeception for null filename
				processArgs[1] = UIdelfelstr;
				processArgs[2] = adb_serial;//
				processArgs[3] = adb_serial_no;//
			}
			//run file from list
			else if(WinCMD_string.indexOf("run_file.cmd") >= 0){
				sendTextPane.text = "";
				sendTextPane.text =("Running!");
				processArgs[1] = UIrunfelstr;
				processArgs[2] = adb_serial;//
				processArgs[3] = adb_serial_no;//
			}
			//run test movement file from list
			else if(WinCMD_string.indexOf("run_tm_file.cmd") >= 0){
				sendTextPane.text = "";
				sendTextPane.text =("Running!");
				processArgs[1] = "testmovement999";
				processArgs[2] = adb_serial;//
				processArgs[3] = adb_serial_no;//
			}
			//stop file
			else if(WinCMD_string.indexOf("stop_file.cmd") >= 0){
				sendTextPane.text = "";
				sendTextPane.text =("Stop!");
				processArgs[1] = adb_serial;
				processArgs[2] = adb_serial_no;//
			}
			//push sound
			else if(WinCMD_string.indexOf("push_sound.cmd") >= 0){
				sendTextPane.text = "";
				sendTextPane.text =("Robot is Learning new music...");
				processArgs[1] = prg_file + "\\" + prg_filename;
				if(processArgs[1].indexOf(" ") != -1)
					processArgs[1] = processArgs[1].split(" ").join("${SPACE}");
//				processArgs[2] = prg_file + "\\" +prg_filename2;
//				if(processArgs[2].indexOf(" ") != -1)
//					processArgs[2] = processArgs[2].split(" ").join("${SPACE}");
				processArgs[2] = adb_serial;//
				processArgs[3] = adb_serial_no;//
			}
			//push movlog
			else if(WinCMD_string.indexOf("push_movlog.cmd") >= 0){
				sendTextPane.text = "";
				sendTextPane.text =("Robot is Learning new move...");
				processArgs[1] = prg_file + "\\" + prg_filename;
				if(processArgs[1].indexOf(" ") != -1)
					processArgs[1] = processArgs[1].split(" ").join("${SPACE}");
				processArgs[2] = adb_serial;//
				processArgs[3] = adb_serial_no;//
			}
			//init WiFi
			else if(WinCMD_string.indexOf("init_wifi.cmd") >= 0){
				sendTextPane.text = "";
				sendTextPane.text =("initializing...");
			}
			//connect_test.cmd
			else if(WinCMD_string.indexOf("connect_test.cmd") >= 0){
				sendTextPane.text = "";
				sendTextPane.text =("Check Robot Connection...");
			}
			//motor_poweron.cmd  motor_pwon
			else if(WinCMD_string.indexOf("motor_pwon.cmd") >= 0){
				sendTextPane.text = "";
				sendTextPane.text =("Motor Power On...");
				processArgs[1] = adb_serial;
				processArgs[2] = adb_serial_no;//
			}
			//motor_powerof.cmd  motor_pwof
			else if(WinCMD_string.indexOf("motor_pwof.cmd") >= 0){
				sendTextPane.text = "";
				sendTextPane.text =("Motor Power Off...");
				processArgs[1] = adb_serial;
				processArgs[2] = adb_serial_no;//
			}
			//get pos
			else if(WinCMD_string.indexOf("get_pos.cmd") >= 0){
				sendTextPane.text = "";
				sendTextPane.text =("Loading Position......");
				var mlk:Date = new Date;
				mlk.time = mlk.getTime()
				 time_stamp= mlk.time.toString();
				
				processArgs[1] = adb_serial;
				processArgs[2] = adb_serial_no;//
				processArgs[3] = time_stamp;
			}
			//load pos
			else if(WinCMD_string.indexOf("load_pos.cmd") >= 0){
				sendTextPane.text = "";
				sendTextPane.text =("Loading Position...");
				processArgs[1] = adb_serial;
				processArgs[2] = adb_serial_no;//
				processArgs[3] = time_stamp + ".gwb";
			}
			
			trace("Got processArgs: ", processArgs); 
			//for release bug
			
			nativeProcessStartupInfo.arguments = processArgs; 
			process.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, CheckCMD); 
//			process.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, CheckCMD_); instead of using timer, we can use counter and timer to improve conncetion speed.
			process.start(nativeProcessStartupInfo);
		}
		
		//Check CMD echo
		public function CheckCMD(event:ProgressEvent):String { 
			if(process.running!=1)
			{trace("Not Running!!! "); 
				return "ERROR!!";
			}

			try
			{
				if(CMP_string.indexOf("#1P")<0)
				var Echo : String = NativeProcess(event.target).standardOutput.readUTFBytes((process.standardOutput.bytesAvailable)-1);//To avoid EOF problem//-1
				else
					var Echo : String = NativeProcess(event.target).standardOutput.readUTFBytes((process.standardOutput.bytesAvailable)-0);//To avoid EOF problem//-1
			}
			catch( e : EOFError )
			{
				trace( e );     // EOFError: Error #2030: End of file was encountered.
			}
			
			trace("Got EchoCMD compare: ", Echo); 
			EchoCMD = Echo;

		try{	
//			if(CMP_string != " ")
//			{
				if(EchoCMD.indexOf(CMP_string)> 0){
					right_opt = 1;
					if(CMP_string=="List of devices attached"){

						if((EchoCMD.indexOf(resultIP[0])!=-1)&&(resultIP[0]!=null)){
							adb_serial = "-s";
							adb_serial_no =  resultIP[0] + ":5555"
							con_status = true;
							wifi_con_status = true;
//							sendTextPane.text ="Device List ready!";
						}
						if(EchoCMD.indexOf("0123456789ABCDEF")!=-1){
							adb_serial = "-s";
							adb_serial_no = "0123456789ABCDEF";
							con_status = true;//if usb is connected, use usb to communicate.
							wifi_con_status = false;
//							sendTextPane.text ="Device List ready!";
						}
						if((EchoCMD.indexOf("0123456789ABCDEF")==-1)&&(EchoCMD.indexOf(resultIP[0])==-1)){
							adb_serial = "";
							adb_serial_no = "";
							con_status = false;
							sendTextPane.text ="Device Search not ready!";
						}
						trace("con_status", con_status);
					}

					
					
				}
				
				if(EchoCMD.indexOf("#1P")!=-1){
					//position assignment to block
					pos_info = EchoCMD;	
					right_opt = 1;
				}
				
//			}
		}
		catch(e: TypeError)
		{}
		
			
			return EchoCMD;
		}
		
		//variables before adb_function()
		private var EchoOK: String = new String;
		private var EchoNOK: String = new String;
		private var loop_counter: int = 0;//also needs to be initialized
		private var wincmd_public: String = new String;//
		private var right_opt: int = 0;
		
		private function afterWaiting_function(event:TimerEvent): void{preWaiting_function_noCMD();}
		private function preWaiting_function_noCMD():int {
//			WinCMD(wincmd_public);//
			trace("afterWaiting_function EchoCMD:",EchoCMD);
			trace("afterWaiting_init CMP_string:",CMP_string);
			//			if(EchoCMD.indexOf(CMP_string)> 0){
			if(right_opt == 1){
				//				if(EchoOK != "Device List!")
				sendTextPane.text = "";
				trace("OK CMD:",EchoCMD);
				//				if(EchoOK != "Device List!")
				sendTextPane.text =(EchoOK);
				
			}
			else{			
				trace("NOK CMD:",EchoCMD);
				
				if(loop_counter > 5)
				{
					timer.removeEventListener(TimerEvent.TIMER, afterWaiting_function);
					timer.stop();
					sendTextPane.text = "";
					sendTextPane.text =(EchoNOK);
				}
				loop_counter ++;
				return 0;
			}
			timer.removeEventListener(TimerEvent.TIMER, afterWaiting_function);
			timer.stop();
			right_opt = 0;
			loop_counter = 0;
			switch (EchoOK){
				case "Ready to connect WiFi! ":
					//afterWaiting_getIP
					adb_function(2500, preWaiting_getIP, afterWaiting_getIP, "inet", "get_ip.cmd");
					break;
				
				case "Unplug USB!":
					//dialog to unplug USB
					dialog_box("Please Unplug USB!!","Attention!!");
					break;
				
				case "Wonderboy is Ready!":
					//load remote file log into local file list.	
					//			EchoOK = "null";
					enable_icons();
					check_filelog();
					break;
				
				case "File deleted!":
					pull_filelog();
					break;
				
				case "WiFi connection OK, ready to upload or run!":
					adb_con_check_init();
					MBlock.app.topBarPart.addwifibutton_nok("OK");
					MBlock.app.topBarPart.setConnectedTitle("WiFi Connected!");
					//			pull_filelog();
					break;
				
				case "Device Search Done!":
					pull_filelog();
					break;
				
				case "Loading Position!":
					load_pos();
					break;
				
				case "Run OK!":
				case "Device List!":
				case "Device Search not ready!":
				case "Execution Stopped!":
					break;			
				
				case "Upload OK!":
				case "Sound Upload OK!":
				case "Movlog Upload OK!":
					if(soundfile_num>0)
					{
						//upload sound files
						var upload_soundname: String	=	new String;	
						var upload_soundlogname: String	=	new String;	
						//for ()
						//					upload_soundname = soundfile_list[soundfile_num-1];
						upload_soundlogname = soundfilelog_list[soundfile_num-1];
						//upload single
						prg_file = MBlock.scratchws + "\\music";//script file to be uploaded
						//					prg_filename = upload_soundname;
						//					prg_filename2 = upload_soundlogname;
						//					prg_filename2 = upload_soundname;
						prg_filename = upload_soundlogname;
						EchoOK = "Sound Upload OK!";
						EchoNOK = "Sound Upload Error!";
						soundfile_num--;
						adb_function(3000, preWaiting_function, afterWaiting_function, "100", "push_sound.cmd");
						break;
					}
					else if(movlogfile_num>0)
					{
						//upload sound files
						var upload_movlogname: String	=	new String;	
						upload_movlogname = movlogfile_list[movlogfile_num-1];
						prg_file = MBlock.scratchws + "\\movlog";//script file to be uploaded
						prg_filename = upload_movlogname;
						EchoOK = "Movlog Upload OK!";
						EchoNOK = "Movlog Upload Error!";
						movlogfile_num--;
						adb_function(1000, preWaiting_function, afterWaiting_function, "100", "push_movlog.cmd");
						break;
					}
					//when file counters are 0, pull file list. whenTM==1, it is a Test Movement upload & debug
					if(TM==1){
						run_tm_file_timer();
						TM = 0;
					}else
						pull_filelog();
					break;
				
				default:
					break;
			}
			return 1;
			
		}
		private function preWaiting_function():int {
			WinCMD(wincmd_public);//
			trace("afterWaiting_function EchoCMD:",EchoCMD);
			trace("afterWaiting_init CMP_string:",CMP_string);
//			if(EchoCMD.indexOf(CMP_string)> 0){
			if(right_opt == 1){
//				if(EchoOK != "Device List!")
				sendTextPane.text = "";
				trace("OK CMD:",EchoCMD);
//				if(EchoOK != "Device List!")
				sendTextPane.text =(EchoOK);
				
			}
			else{			
				trace("NOK CMD:",EchoCMD);
				
				if(loop_counter > 5)
				{
				timer.removeEventListener(TimerEvent.TIMER, afterWaiting_function);
				timer.stop();
				sendTextPane.text = "";
				sendTextPane.text =(EchoNOK);
				}
				loop_counter ++;
				return 0;
			}
			timer.removeEventListener(TimerEvent.TIMER, afterWaiting_function);
			timer.stop();
			right_opt = 0;
			loop_counter = 0;
			switch (EchoOK){
			case "Ready to connect WiFi! ":
			//afterWaiting_getIP
			adb_function(2500, preWaiting_getIP, afterWaiting_getIP, "inet", "get_ip.cmd");
			break;
				
			case "Unplug USB!":
			//dialog to unplug USB
			dialog_box("Please Unplug USB!!","Attention!!");
			break;
			
			case "Wonderboy is Ready!":
			//load remote file log into local file list.	
//			EchoOK = "null";
			enable_icons();
			check_filelog();
			break;
			
			case "File deleted!":
			pull_filelog();
			break;
			
			case "WiFi connection OK, ready to upload or run!":
			adb_con_check_init();
			MBlock.app.topBarPart.addwifibutton_nok("OK");
			MBlock.app.topBarPart.setConnectedTitle("WiFi Connected!");
//			pull_filelog();
			break;
		
			case "Device Search Done!":
			pull_filelog();
			break;
			
			case "Loading Position!":
			load_pos();
			break;
				
			case "Run OK!":
			case "Device List!":
			case "Device Search not ready!":
			case "Execution Stopped!":
			break;			
					
			case "Upload OK!":
			case "Sound Upload OK!":
			case "Movlog Upload OK!":
					if(soundfile_num>0)
					{
					//upload sound files
					var upload_soundname: String	=	new String;	
					var upload_soundlogname: String	=	new String;	
					//for ()
//					upload_soundname = soundfile_list[soundfile_num-1];
					upload_soundlogname = soundfilelog_list[soundfile_num-1];
					//upload single
					prg_file = MBlock.scratchws + "\\music";//script file to be uploaded
//					prg_filename = upload_soundname;
//					prg_filename2 = upload_soundlogname;
//					prg_filename2 = upload_soundname;
					prg_filename = upload_soundlogname;
					EchoOK = "Sound Upload OK!";
					EchoNOK = "Sound Upload Error!";
					soundfile_num--;
					adb_function(3000, preWaiting_function, afterWaiting_function, "100", "push_sound.cmd");
					break;
					}
					else if(movlogfile_num>0)
					{
						//upload sound files
						var upload_movlogname: String	=	new String;	
						upload_movlogname = movlogfile_list[movlogfile_num-1];
						prg_file = MBlock.scratchws + "\\movlog";//script file to be uploaded
						prg_filename = upload_movlogname;
						EchoOK = "Movlog Upload OK!";
						EchoNOK = "Movlog Upload Error!";
						movlogfile_num--;
						adb_function(1000, preWaiting_function, afterWaiting_function, "100", "push_movlog.cmd");
						break;
					}
					//when file counters are 0, pull file list. whenTM==1, it is a Test Movement upload & debug
					if(TM==1){
						run_tm_file_timer();
						TM = 0;
					}else
					pull_filelog();
					break;
				
				default:
					break;
			}
			return 1;
			
		}
//		init	OK			"Ready to connect WiFi, please unplug USB cable! "
//				NOK			"Not ready to connect WiFi, please try again!"
//		connect	OK			"WiFi connection OK, ready to upload or run!"
//				NOK			"Not connected to WiFi, please try again!"
//		upload	OK			"WiFi connection OK, ready to upload or run!"
//				NOK			"Not connected to WiFi, please try again!"
//		run		OK			"WiFi connection OK, ready to upload or run!"
//				NOK			"Not connected to WiFi, please try again!"
		
		private function afterWaiting_getIP(event:TimerEvent):void {preWaiting_getIP();}
		private function preWaiting_getIP():int {
			WinCMD(wincmd_public);//
			trace("afterWaiting_getIP EchoCMD:",EchoCMD);
			trace("afterWaiting_init CMP_string:",CMP_string);
			if(EchoCMD.indexOf(CMP_string)> 0){
//			if(right_opt == 1){
				sendTextPane.text = "";
				sendTextPane.text = "IP OK, Ready to connect WiFi!";
				ip_Wonderboy = EchoCMD;
				var pattern:RegExp = /\d*\.\d*\.\d*\.\d*\// ;
				var pattern_ip:RegExp = /\d*\.\d*\.\d*\.\d*/ ;
				var pattern_array: Array = ip_Wonderboy.match(pattern);
				var result:Array = new Array;
				result = pattern.exec(ip_Wonderboy); 
				resultIP= pattern_ip.exec(result[0]); 
				trace("ip_Wonderboy:", ip_Wonderboy);
				trace("result :", result[0]); 
				trace("result IP:", resultIP);
			}
			else{
				
				
				if(loop_counter > 2)
				{
					timer.removeEventListener(TimerEvent.TIMER, afterWaiting_getIP);
					timer.stop();
					sendTextPane.text = "";
					sendTextPane.text =("IP NOK, Not ready to connect WiFi, please try again!");
				}
				loop_counter ++;
				return 0;
			}
			timer.removeEventListener(TimerEvent.TIMER, afterWaiting_getIP);
			timer.stop();
			right_opt = 0;
			loop_counter = 0;
			EchoOK = "WiFi connection OK, ready to upload or run!";
			EchoNOK = "Not connected to WiFi, please try again!";
			adb_function(2000, preWaiting_function, afterWaiting_function, "connected to", "wifi_connect.cmd");
			return 1;
		}
		
//		Dialog
		private function dialog_box(dialog_box_text:String, dialog_box_title:String): void{
			var dialog:DialogBox = new DialogBox();
			dialog.addTitle(dialog_box_title);
			dialog.addText(dialog_box_text);
			
			function onCancel():void{
				dialog.cancel();
				//not just cancel if title is ***
				if(dialog_box_title == "Overwrite Existed File?"){
					upload_files();
				}				
			}
			
			function onjustCancel():void{
				dialog.cancel();			
			}
			
			dialog.addButton("OK",onCancel);
			
			//add another button if title is ***
			if(dialog_box_title == "Overwrite Existed File?"){
				dialog.addButton("Cancel",onjustCancel);
			}
			
			dialog.showOnStage(app.stage);
		}
		
				
		private function onOpenArduinoIDE(evt:MouseEvent):void{
			if(showArduinoCode()){
				ArduinoManager.sharedManager().openArduinoIDE(arduinoTextPane.textField.text);
			}
		}
		private function onScroll(evt:Event):void{
			lineNumText.scrollV = arduinoTextPane.textField.scrollV;
		}
		
		static private const classNameList:Array = [
			"SoftwareSerial",
			"MeBoard",
			"MeDCMotor",
			"MeServo",
			"MeIR",
			"Me7SegmentDisplay",
			"MeRGBLed",
			"MePort",
			"MeGyro",
			"MeJoystick",
			"MeLight",
			"MeSound",
			"MeStepper",
			"MeEncoderMotor",
			"MeInfraredReceiver",
			"MeTemperature",
			"MeUltrasonicSensor",
			"MeSerial",
			"Servo",
			"mBot",
			"Arduino",
		];
		
		/*public function showArduinoCode(arg:String=""):Boolean{
		var retcode:String = util.JSON.stringify(app.stagePane);
		var formatCode:String = ArduinoManager.sharedManager().jsonToCpp(retcode);
		uploadBt.visible = !ArduinoManager.sharedManager().hasUnknownCode;
		if(formatCode==null){
		return false;
		}
		if(!app.stageIsArduino){
		app.toggleArduinoMode();
		}
		for(var i:uint=0;i<5;i++){
		formatCode = formatCode.split("\r\n\r\n").join("\r\n").split("\r\n\t\r\n").join("\r\n");
		}
		var codes:Array = formatCode.split("\n");
		arduinoTextPane.setText(formatCode);
		var fontGreen:TextFormat = new TextFormat("Arial",14,0x006633);
		var fontYellow:TextFormat = new TextFormat("Arial",14,0x999900);
		var fontOrange:TextFormat = new TextFormat("Arial",14,0x996600);
		var fontRed:TextFormat = new TextFormat("Arial",14,0x990000);
		var fontBlue:TextFormat = new TextFormat("Arial",14,0x000099);
		formatKeyword(arduinoTextPane.textField,"String ",fontRed,0,1);
		formatKeyword(arduinoTextPane.textField,"int ",fontRed,0,1);
		formatKeyword(arduinoTextPane.textField,"char ",fontRed,0,1);
		formatKeyword(arduinoTextPane.textField,"double ",fontRed,0,1); 
		formatKeyword(arduinoTextPane.textField,"boolean ",fontRed,0,1);
		formatKeyword(arduinoTextPane.textField,"false",fontRed,0,0);
		formatKeyword(arduinoTextPane.textField,"true",fontRed,0,0);
		formatKeyword(arduinoTextPane.textField,"void ",fontOrange,0,1);
		formatKeyword(arduinoTextPane.textField,"for(",fontOrange,0,1);
		formatKeyword(arduinoTextPane.textField,"if(",fontOrange,0,1);
		formatKeyword(arduinoTextPane.textField,"else{",fontOrange,0,1);
		formatKeyword(arduinoTextPane.textField,"while(",fontOrange,0,1);
		formatKeyword(arduinoTextPane.textField,"#include ",fontRed,0,1);
		
		formatKeyword(arduinoTextPane.textField," setup()",fontRed,1,2);
		formatKeyword(arduinoTextPane.textField," loop()",fontRed,1,2);
		formatKeyword(arduinoTextPane.textField,"Serial.",fontRed,0,1);
		formatKeyword(arduinoTextPane.textField,".begin(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,".available(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,".println(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,".print(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,".read(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,".length(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,"return ",fontOrange,0,1);
		formatKeyword(arduinoTextPane.textField,".run(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,".runSpeed(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,".setMaxSpeed(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,".move(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,".moveTo(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,".attach(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,".charAt(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,"memset",fontOrange,0,0);
		formatKeyword(arduinoTextPane.textField,".write(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,".display(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,".setColorAt(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,".show(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,".dWrite1(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,".dWrite2(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,".dRead1(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,".dRead2(",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,"(M1)",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,"(M2)",fontOrange,1,1);
		formatKeyword(arduinoTextPane.textField,"SLOT_1",fontOrange,0,0);
		formatKeyword(arduinoTextPane.textField,"SLOT_2",fontOrange,0,0);
		formatKeyword(arduinoTextPane.textField,"PORT_1",fontOrange,0,0);
		formatKeyword(arduinoTextPane.textField,"PORT_2",fontOrange,0,0);
		formatKeyword(arduinoTextPane.textField,"PORT_3",fontOrange,0,0);
		formatKeyword(arduinoTextPane.textField,"PORT_4",fontOrange,0,0);
		formatKeyword(arduinoTextPane.textField,"PORT_5",fontOrange,0,0);
		formatKeyword(arduinoTextPane.textField,"PORT_6",fontOrange,0,0);
		formatKeyword(arduinoTextPane.textField,"PORT_7",fontOrange,0,0);
		formatKeyword(arduinoTextPane.textField,"PORT_8",fontOrange,0,0);
		formatKeyword(arduinoTextPane.textField,"delay(",fontRed,0,1);
		formatKeyword(arduinoTextPane.textField,"OUTPUT)",fontOrange,0,1);
		formatKeyword(arduinoTextPane.textField,"INPUT)",fontOrange,0,1);
		formatKeyword(arduinoTextPane.textField,"pinMode(",fontRed,0,1);
		formatKeyword(arduinoTextPane.textField,"digitalWrite(",fontRed,0,1);
		formatKeyword(arduinoTextPane.textField,"digitalRead(",fontRed,0,1);
		formatKeyword(arduinoTextPane.textField,"analogWrite(",fontRed,0,1);
		formatKeyword(arduinoTextPane.textField,"analogRead(",fontRed,0,1);
		formatKeyword(arduinoTextPane.textField,"getAngle(",fontRed,0,1);
		formatKeyword(arduinoTextPane.textField,"refresh(",fontRed,0,1);
		formatKeyword(arduinoTextPane.textField,"update(",fontRed,0,1);
		
		formatKeyword(arduinoTextPane.textField,"tone(",fontRed,0,1);
		formatKeyword(arduinoTextPane.textField,"noTone(",fontRed,0,1);
		formatKeyword(arduinoTextPane.textField,"Wire.",fontGreen,0,1);
		
		for each(var clsName:String in classNameList){
		formatKeyword(arduinoTextPane.textField, clsName, fontGreen, 0, 0);
		}
		
		
		lineNumText.text = "";
		var preS:String = "";
		var t:Number = arduinoTextPane.textField.numLines;
		var tt:uint = 0;
		while(t>1){
		t/=10;
		preS+="0";
		tt++;
		}
		lineNumWidth = tt*10;
		fixlayout();
		for(i = 0;i<arduinoTextPane.textField.numLines;i++){
		lineNumText.appendText((preS+(i+1)).substr(-tt,tt)+".\n");
		}
		if(ArduinoManager.sharedManager().hasUnknownCode){
		if(!isDialogBoxShowing){
		isDialogBoxShowing = true;
		var dBox:DialogBox = new DialogBox();
		dBox.addTitle(Translator.map("unsupported block found, remove them to continue."));
		for each(var b:Block in ArduinoManager.sharedManager().unknownBlocks){
		b.mouseEnabled = false;
		b.mouseChildren = false;
		dBox.addBlock(b);
		}
		function cancelHandle():void{
		isDialogBoxShowing = false;
		dBox.cancel();
		}
		dBox.addButton("OK",cancelHandle);
		dBox.showOnStage(app.stage);
		dBox.fixLayout();
		}
		arduinoFrame.visible = false;
		if(app.stageIsArduino){
		app.toggleArduinoMode();
		}
		}else{
		arduinoFrame.visible = true;
		}
		return true;
		} */
		
		public static var textpaneCode: String = new String;
		
		private var saveLOG:String = "";
		
	//16. malcolm PosChecker
		private function checkPos():Boolean{
				var errorStatus:Boolean = false;
				
//					errorStatus = PosChecker.sharedManager().checkPos();
				errorStatus = PosChecker.checker;
					return errorStatus;
			}
	
		//16. PosChecker Warning Dialog
		private function showWarningDialog():void{
				var dBox:DialogBox = new DialogBox();
				isDialogBoxShowing = true;
				dBox.addTitle(Translator.map("Warning!"));
//				dBox.addText(PosChecker.sharedManager().getErr().split("\r\n\r\n").join("\r\n").split("\r\n\t\r\n").join("\r\n"));
				dBox.addText(PosChecker.warningmsg.split("\r\n\r\n").join("\r\n").split("\r\n\t\r\n").join("\r\n"));
				dBox.addButton("OK", cancelHandle)
					function cancelHandle():void{
							isDialogBoxShowing = false;
							dBox.cancel();		
						}
					//Diaplay Dbox
					dBox.showOnStage(app.stage);
				dBox.fixLayout();	
			}
	//
		
				
		private function combineBlocks(block_type: String, raw_code: String):String{
			var new_code: String = "";
			var block_pos: int;
			var block_layer_cnt: int;
			//			raw_code = "\""+block_type+"\":";
			var bracelet_pos_l: int;
			var bracelet_pos_r: int;
			
			
			block_pos = raw_code.search("\""+block_type+"\":");
			if(block_pos != -1){
				block_layer_cnt = 0;
				
				
				//					Check bracelet "{" and "}" until all the pairs are finished, so that pattern searching can't go into "{ ...}"
				do{
					bracelet_pos_l = raw_code.indexOf("{", block_pos);
					if(bracelet_pos_l != -1){
						block_layer_cnt++;
					}
					
					bracelet_pos_r = raw_code.indexOf("}", block_pos);
					if(bracelet_pos_r != -1){
						block_layer_cnt--;
					}
					
					if(bracelet_pos_l > bracelet_pos_r)
						block_pos = bracelet_pos_l;
					else block_pos = bracelet_pos_r;
					
					
				}while(block_layer_cnt != 0)
				
			}else{
				
			}
			return new_code;
		}
		
		public function showArduinoCode(arg:String=""):Boolean{
			var retcode:String = util.JSON.stringify(app.stagePane);
			var formatCode:String = ArduinoManager.sharedManager().jsonToBASH(retcode);
			//13. Malcolm LOG
			var formatLOG:String = ArduinoManager.sharedManager().jsonToLOG(retcode);
			//
			//16. Malcolm check postition After code generate
//			PosChecker.checkPosSHELL();
				var poschecker:Boolean = PosChecker.checker;
			if (poschecker == true){
					if (isDialogBoxShowing == false){
							showWarningDialog();
						}
				}
			//
			uploadBt.visible = !ArduinoManager.sharedManager().hasUnknownCode;
			if(formatCode==null){
				return false;
			}
			if(!app.stageIsArduino){
				app.toggleArduinoMode();
			}
			//auto_acbefore, auto_acafter in main program. "voice" should be changed to a variable, unique variable.
//			formatCode = auto_acbefore(formatCode, "voice");
//			formatCode = auto_acafter(formatCode, "voice");
			
			for(var i:uint=0;i<5;i++){
				formatCode = formatCode.split("\r\n\r\n").join("\r\n").split("\r\n\t\r\n").join("\r\n");
				//13. Malcolm LOG
				formatLOG = formatLOG.split("\r\n\r\n").join("\r\n").split("\r\n\t\r\n").join("\r\n");
				//
			}
			var codes:Array = formatCode.split("\n");
			arduinoTextPane.setText(formatCode);
			//13. Malcolm LOG
			messageTextPane.setText(formatLOG);
			saveLOG = formatLOG;
			//
			var fontGreen:TextFormat = new TextFormat("Arial",14,0x006633);
			var fontYellow:TextFormat = new TextFormat("Arial",14,0x999900);
			var fontOrange:TextFormat = new TextFormat("Arial",14,0x996600);
			var fontRed:TextFormat = new TextFormat("Arial",14,0x990000);
			var fontBlue:TextFormat = new TextFormat("Arial",14,0x000099);
			formatKeyword(arduinoTextPane.textField,"String ",fontRed,0,1);
			formatKeyword(arduinoTextPane.textField,"int ",fontRed,0,1);
			formatKeyword(arduinoTextPane.textField,"char ",fontRed,0,1);
			formatKeyword(arduinoTextPane.textField,"double ",fontRed,0,1); 
			formatKeyword(arduinoTextPane.textField,"boolean ",fontRed,0,1);
			formatKeyword(arduinoTextPane.textField,"false",fontRed,0,0);
			formatKeyword(arduinoTextPane.textField,"true",fontRed,0,0);
			formatKeyword(arduinoTextPane.textField,"void ",fontOrange,0,1);
			formatKeyword(arduinoTextPane.textField,"for(",fontOrange,0,1);
			formatKeyword(arduinoTextPane.textField,"if(",fontOrange,0,1);
			formatKeyword(arduinoTextPane.textField,"else{",fontOrange,0,1);
			formatKeyword(arduinoTextPane.textField,"while(",fontOrange,0,1);
			formatKeyword(arduinoTextPane.textField,"#include ",fontRed,0,1);
			
			formatKeyword(arduinoTextPane.textField," setup()",fontRed,1,2);
			formatKeyword(arduinoTextPane.textField," loop()",fontRed,1,2);
			formatKeyword(arduinoTextPane.textField,"Serial.",fontRed,0,1);
			formatKeyword(arduinoTextPane.textField,".begin(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,".available(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,".println(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,".print(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,".read(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,".length(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,"return ",fontOrange,0,1);
			formatKeyword(arduinoTextPane.textField,".run(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,".runSpeed(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,".setMaxSpeed(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,".move(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,".moveTo(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,".attach(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,".charAt(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,"memset",fontOrange,0,0);
			formatKeyword(arduinoTextPane.textField,".write(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,".display(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,".setColorAt(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,".show(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,".dWrite1(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,".dWrite2(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,".dRead1(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,".dRead2(",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,"(M1)",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,"(M2)",fontOrange,1,1);
			formatKeyword(arduinoTextPane.textField,"SLOT_1",fontOrange,0,0);
			formatKeyword(arduinoTextPane.textField,"SLOT_2",fontOrange,0,0);
			formatKeyword(arduinoTextPane.textField,"PORT_1",fontOrange,0,0);
			formatKeyword(arduinoTextPane.textField,"PORT_2",fontOrange,0,0);
			formatKeyword(arduinoTextPane.textField,"PORT_3",fontOrange,0,0);
			formatKeyword(arduinoTextPane.textField,"PORT_4",fontOrange,0,0);
			formatKeyword(arduinoTextPane.textField,"PORT_5",fontOrange,0,0);
			formatKeyword(arduinoTextPane.textField,"PORT_6",fontOrange,0,0);
			formatKeyword(arduinoTextPane.textField,"PORT_7",fontOrange,0,0);
			formatKeyword(arduinoTextPane.textField,"PORT_8",fontOrange,0,0);
			formatKeyword(arduinoTextPane.textField,"delay(",fontRed,0,1);
			formatKeyword(arduinoTextPane.textField,"OUTPUT)",fontOrange,0,1);
			formatKeyword(arduinoTextPane.textField,"INPUT)",fontOrange,0,1);
			formatKeyword(arduinoTextPane.textField,"pinMode(",fontRed,0,1);
			formatKeyword(arduinoTextPane.textField,"digitalWrite(",fontRed,0,1);
			formatKeyword(arduinoTextPane.textField,"digitalRead(",fontRed,0,1);
			formatKeyword(arduinoTextPane.textField,"analogWrite(",fontRed,0,1);
			formatKeyword(arduinoTextPane.textField,"analogRead(",fontRed,0,1);
			formatKeyword(arduinoTextPane.textField,"getAngle(",fontRed,0,1);
			formatKeyword(arduinoTextPane.textField,"refresh(",fontRed,0,1);
			formatKeyword(arduinoTextPane.textField,"update(",fontRed,0,1);
			
			formatKeyword(arduinoTextPane.textField,"tone(",fontRed,0,1);
			formatKeyword(arduinoTextPane.textField,"noTone(",fontRed,0,1);
			formatKeyword(arduinoTextPane.textField,"Wire.",fontGreen,0,1);
			
			for each(var clsName:String in classNameList){
				formatKeyword(arduinoTextPane.textField, clsName, fontGreen, 0, 0);
			}
			
			
			lineNumText.text = "";
			var preS:String = "";
			var t:Number = arduinoTextPane.textField.numLines;
			var tt:uint = 0;
			while(t>1){
				t/=10;
				preS+="0";
				tt++;
			}
			lineNumWidth = tt*10;
			fixlayout();

			for(i = 0;i<arduinoTextPane.textField.numLines;i++){
				lineNumText.appendText((preS+(i+1)).substr(-tt,tt)+".\n");
			}
			//12. Malcolm Arduino Append checker that checks for unrecognised blocks
			/*if(ArduinoManager.sharedManager().hasUnknownCode){
			if(!isDialogBoxShowing){
			isDialogBoxShowing = true;
			var dBox:DialogBox = new DialogBox();
			dBox.addTitle(Translator.map("unsupported block found, remove them to continue."));
			for each(var b:Block in ArduinoManager.sharedManager().unknownBlocks){
			b.mouseEnabled = false;
			b.mouseChildren = false;
			dBox.addBlock(b);
			}
			function cancelHandle():void{
			isDialogBoxShowing = false;
			dBox.cancel();
			}
			dBox.addButton("OK",cancelHandle);
			dBox.showOnStage(app.stage);
			dBox.fixLayout();
			}
			arduinoFrame.visible = false;
			if(app.stageIsArduino){
			app.toggleArduinoMode();
			}
			}*/ /*else{*/
			arduinoFrame.visible = true;
			//}
			textpaneCode = formatCode;//save it as global String
			//			
			var pattern_wav:RegExp = /\"[0-9a-zA-Z()\s\-\,\.\!]+\#music\.log/gi ; //
			var result:Array = new Array;
			try{
				while (result != null) 			
				{ 
					result = pattern_wav.exec(textpaneCode); //need to check repetitive file name			
					textpaneCode = textpaneCode.split(result[0]).join(result[0].replace(/\s/gi, "[*space]"));
				} 
			}catch(e: TypeError)
			{}
			
			var pattern_leg:RegExp = /\"[0-9a-zA-Z()\s\-\,\.\!]+\#leg\.log/gi ; //
			var result:Array = new Array;
			try{
				while (result != null) 			
				{ 
					result = pattern_leg.exec(textpaneCode); //need to check repetitive file name			
					textpaneCode = textpaneCode.split(result[0]).join(result[0].replace(/\s/gi, "[*space]"));
				} 
			}catch(e: TypeError)
			{}
			
//			textpaneCode = textpaneCode.replace(/^\s*|\s*$/g,"").split(" ").join("");//remove white space, a little bit coufusing...
			textpaneCode = eat_space(textpaneCode);

			return true;
		}
		
		//Steven delete all space, CF, etc... and give music space!
		private function eat_space(org_code:String):String{
			var pattern_1:RegExp = /\s/g;
			org_code = org_code.replace(pattern_1,"");
			org_code = org_code.split("[*space]").join(" ");
			org_code = org_code.split("!!*space!!").join(" ");//don't use [ ] for space marker in "answer"
			return org_code;
		}
		
				//14. Malcolm Save button
	private var file:File = new File();
	private var blockName:String = "";

	private function onSaveLog(evt:MouseEvent):void{
			//17. Malcolm Implement Pos Checker before save	
				if (PosChecker.sharedManager().checkforLimitConflict() == true){
						createcplexMovWarningDialog();
					}
			else{
					createSaveDialog(); //Pop Up dialog window, propmt for user input				
					blockName = "";
				}
		}
	
//power on/off motors
	private function onmotor_pwon(evt:MouseEvent):void{
		EchoOK = "Power on OK!";
		EchoNOK = "Error Power on!";
		adb_function(2000, preWaiting_function, afterWaiting_function, "Broadcast completed", "motor_pwon.cmd");	//
	}
	
	private function onmotor_pwof(evt:MouseEvent):void{
		EchoOK = "Power off OK!";
		EchoNOK = "Error Power off!";
		adb_function(2000, preWaiting_function, afterWaiting_function, "Broadcast completed", "motor_pwof.cmd");	
	}
	
	
		private function saveXML():void{
				var thisXML:XML = openXML();
				var xmlString:String = '<command category="9" type=" " opcode="GTMovement.'+ blockName + '.log" spec="' + blockName + '">["'+ blockName+'.log'+'"]</command>';//.movlog to .log
				var xmlXML:XML;
				xmlXML = new XML(xmlString)
				thisXML.appendChild(xmlXML);
				var filePath:String = File.applicationDirectory.resolvePath("assets" + "\\blockSpec.xml").nativePath;
				var file:File = new File(filePath);
				var stream:FileStream = new FileStream();
				stream.open(file, FileMode.WRITE);
				stream.writeUTFBytes(thisXML);
				stream.close();			
				

			}
	
		private function saveLogFile(filename: String):void{
//				var filePath:String = File.applicationDirectory.resolvePath( "movlog\\" + blockName + ".log").nativePath;
				var filePath:String = File.applicationDirectory.resolvePath( "movlog\\" + filename + ".log").nativePath;//for input var
				var file:File = new File(filePath);
				var fileStream:FileStream = new FileStream();
				fileStream.open(file, FileMode.WRITE);
//				fileStream.writeUTFBytes(saveLOG);
				var pattern_pos_g:RegExp = /P\-?\d*/g; //gi 
				var pattern_pos:RegExp = /P\-?\d*/; //gi 
				//
				showArduinoCode("");//just to reset saveLOG
				var result:Object =  pattern_pos_g.exec(saveLOG);
				var savelog1: String = new String;
				var savelog2: String= new String;
				try{
					while (result != null) 			
					{ 
						if(result[0].indexOf("P-")==-1 )
						result[0] = "P"+Math.floor(parseInt((result[0]).replace("P", ""))*3.7926).toString();
						else if(result[0].indexOf("P-")!=-1 ){
							// negtive
							var temp:Number = parseInt((result[0]).replace("P-", ""))*-3.7926;
							temp = Math.floor(temp);
							result[0] = "P"+temp.toString();
							
						}
							//result[0] = "P"+(Math.floor(parseInt((result[0]).replace("P-", ""))*-2.8444)).toString();
						
						savelog1 = saveLOG.slice(0, result.index);
						savelog2 = saveLOG.slice( result.index);
						
						saveLOG = savelog1 +savelog2.replace(pattern_pos, result[0] );
						result =  pattern_pos_g.exec(saveLOG);
//						result = pattern_pos.exec(saveLOG); //need to check repetitive file name			
//						textpaneCode = textpaneCode.split(result[0]).join(result[0].replace(/\s/gi, "[*space]"));
					} 
				}catch(e: TypeError)
				{}
		
				
				fileStream.writeUTFBytes(saveLOG);
				
				fileStream.close();
			}
	
		private function createSaveDialog():void{
				var dBox:DialogBox = new DialogBox();
				isDialogBoxShowing = true;
				dBox.addTitle(Translator.map("Give a name for your new block!"));
				dBox.addField("BlockName", 300, null, true);
				
				dBox.addButton("Done", cancelHandle)
				function cancelHandle():void{
						blockName = dBox.getField("BlockName"); //Grab block name from text box
						//19. Call Check for duplicate names
							if (blockNameChecker(blockName) == true){ //check block name first
									createOverwriteDialog();
									dBox.cancel();
//									createOverwriteDialog();
								}
						else{
								saveXML(); //Add new block into the XML
								saveLogFile(blockName); //Save log file in the background
								
									
								}
						
							//reset block
							resetblocks();
						isDialogBoxShowing = false;
						dBox.cancel();				
						refreshCategory();
					}
				//
					//Diaplay Dbox
					dBox.showOnStage(app.stage);
				dBox.fixLayout();								
			}
	
//19. Check for duplicate names
		private function blockNameChecker(blkName:String):Boolean{
				var checker:Boolean = false;
				var thisXML:XML = openXML();
				var thisString:String = thisXML.toXMLString();
				if (thisString.indexOf(blkName+".log") > -1){
						checker = true;
					}
				
					return checker;
			}
		
			//19. create overwrite dialog
			private function createOverwriteDialog():void{
					var dBox:DialogBox = new DialogBox();
					isDialogBoxShowing = true;
					dBox.addTitle("Warning!");
					dBox.addText("A block of the same name exists \n Do you wish to overwite?");
					dBox.addButton("Yes",yesOverwrite);
					function yesOverwrite():void{
							saveLogFile(blockName);
							isDialogBoxShowing = false;
							dBox.cancel();				
						}
						dBox.addButton("No",noOverwrite);
					function noOverwrite():void{
							isDialogBoxShowing = true;
							dBox.cancel();
						}
						
						dBox.showOnStage(app.stage);
					dBox.fixLayout();
				}
		
			//17. Malcolm Complex warning Dialog
			private function createcplexMovWarningDialog():void{
				var dBox:DialogBox = new DialogBox();
				isDialogBoxShowing = true;
				dBox.addTitle(Translator.map("Warning! You have out of Range block(s)"));
//				dBox.addText(PosChecker.sharedManager().getComplexErr());
				dBox.addText(PosChecker.complexWarning);
				dBox.addButton("OK", cancelHandle)
				function cancelHandle():void{
					dBox.cancel();
				}
				//Diaplay Dbox
				dBox.showOnStage(app.stage);
				dBox.fixLayout();	
			}
		//
		
			
		//22. Malcolm Movement Warning Dialog		
		private function createMovConflWarningDialog():void{		
			var dBox:DialogBox = new DialogBox();		
			isDialogBoxShowing = true;		
			dBox.addTitle(Translator.map("Warning! Your custom movement has conflicts!"));		
			dBox.addText(PosChecker.sharedManager().getComplexMovErr());		
			dBox.addButton("OK", cancelHandle)		
			function cancelHandle():void{		
				dBox.cancel();		
			}		
			//Display Dbox		
			dBox.showOnStage(app.stage);		
			dBox.fixLayout();			
		}
			
		public function resetblocks():void{
				app.updateFalsePalette();
				Specs.commands = [];
				Specs.Init();
			}
	
		private function openXML():XML{
				var content:String = FileUtil.ReadString(File.applicationDirectory.resolvePath("assets/blockSpec.xml"));
				var xml:XML = XML(content);
				var item:XML;		
				return xml;
			}
	//
		
		//15. Malcolm Del button 
		
	private var delfilename:String = "";
	private var delfile:FileReference = new FileReference;
	private var delfile_test:File = new File;
	private function onDelLog(evt:MouseEvent):void{
			createDeleteDialog();
		}
	
	
	private function onT(evt:MouseEvent):void{
		saveLogFile("testmovement999");
		tm_onuploadwbBt_timer();
	}
	
	private var TM: int;
	private function onM(evt:MouseEvent):void{
		saveLogFile("testmovement999");
		tm_onuploadwbBt_timer();
		TM =1;
//		run_tm_file_timer();
	}
	
		private function createDeleteDialog():void{
//				var dBox:DialogBox = new DialogBox();
//				isDialogBoxShowing = true;
//				dBox.addTitle(Translator.map("Which Movement do you want to delete?"));
				
				/*dBox.addButtonExt("choose","test",choose);
					function choose():void{
						//delfile.browse();		
					}*/

					//dBox.addBlock();
					//Diaplay Dbox
					var fileFilters:Array = [
						new FileFilter('Movement Log', '*.log'),
						//			new FileFilter('Scratch 1.4 Project', '*.sb')
					];
					
//					var delfile:File = new File;
//					delfile.addEventListener(Event.SELECT, deleteInxml);
					delfile_test = File.applicationDirectory.resolvePath("movlog\\");
					delfile_test.browse(fileFilters);
					
					delfile = delfile_test;
					delfile.addEventListener(Event.SELECT,deleteInxml)
	//					dBox.addButton("Done", cancelHandle)
	//					function cancelHandle():void{
	//						if(delfile.name.indexOf(".movlog") != -1)	//check if file selected
	//							deleteInxml();
	//						
	//						isDialogBoxShowing = false;
	//						dBox.cancel();
	//					}
//					function fileSaved(e:Event):void {
//						var file:File = e.target as File;
//						FileUtil.WriteBytes(file, projIO.encodeProjectAsZipFile(stagePane));
//						
//						saveNeeded = false;
//						setProjectFile(file);
//						if(postSaveAction!=null){
//							postSaveAction();
//						}
//					}
//					delfile.browse(fileFilters);		
	//				dBox.showOnStage(app.stage);
	//				dBox.fixLayout();								
			}
	
		private function get name():String{
				return delfile.name.toString();
			}
	
		private function deleteInxml(e:Event):void{//e:Event
//			var delfile:File = e.target as File;
				var thisXML:XML = openXML();
				//thisXML.item.((text()=='["testdel1.log"]') && (delete parent().children()[valueOf().childIndex()]));
				//delete thisXML.variable.(command=="[\"\testdel1.log\"\]")[0] as XML;
				
				////try convert to string
				var thisString:String = thisXML.toXMLString();
				//<command category="9" type=" " opcode="GTMovement.'+ blockName + '.log" spec="' + blockName + '">["'+ blockName+'.log'+'"]</command>
				//<command category="9" type=" " opcode="GTMovement.'+delfile.name+'" spec="'+ delfile.name.replace(".log","")+'">["'+delfile.name+'"]</command>
				var editedstring:String = "";
				var inspecstring:String = '<command category="9" type=" " opcode="GTMovement.'+delfile.name+'" spec="'+ delfile.name.replace(".log","")+'">["'+delfile.name+'"]<\/command>';
				trace(editedstring = thisString.replace('<command category="9" type=" " opcode="GTMovement.'+delfile.name+'" spec="'+ delfile.name.replace(".log","")+'">["'+delfile.name+'"]<\/command>',""));
				////conver back
				var editedXML:XML = XML(editedstring);
				
				//delete thisXML.xml.command.(@spec==["testdel3"])[0];
				//save edited xml
				var filePath:String = File.applicationDirectory.resolvePath("assets" + "\\blockSpec.xml").nativePath;
				var file:File = new File(filePath);
				var stream:FileStream = new FileStream();
				stream.open(file, FileMode.WRITE);
				stream.writeUTFBytes(editedXML);
				stream.close();			
				
				//del actual log file
				var dellogfilepath:String = File.applicationDirectory.resolvePath("movlog\\" + delfile.name).nativePath;
				var dellogfile:File = new File(dellogfilepath);
				dellogfile.deleteFile();
				resetblocks();
				refreshCategory();
				

			}
	
		private var nofile:FileReference = new File();
	
		private function pickFile(event:MouseEvent):void {
				nofile.addEventListener(Event.SELECT, openFile);              
				nofile.browse();
			}
	
		private function openFile(event:Event):void{
				//folderPath.text = file.nativePath;
				}
	//

		
		static private var isDialogBoxShowing:Boolean;
		private function formatKeyword(txt:TextField,word:String,format:TextFormat,subStart:uint=0,subEnd:uint=0):void
		{
			var index:int = 0;
			var msg:String = txt.text;
			for(;;){
				index = msg.indexOf(word, index);
				if(index < 0){
					break;
				}
				txt.setTextFormat(format, index + subStart, index + word.length - subEnd);
				index += word.length;
			}
		}
		public function resetCategory():void { selector.select(Specs.motionCategory) }
		public function refreshCategory():void { selector.select(Specs.dataCategory) }
		
		public function updatePalette():void {
			selector.updateTranslation();
			if(!MBlock.app.stageIsArduino && MBlock.app.viewedObj() is ScratchStage){
				if(selector.selectedCategory == Specs.motionCategory){
					selector.selectedCategory = Specs.looksCategory;
				}
			}
			selector.select(selector.selectedCategory);
		}
		
		public function updateFalsePalette():void {
			selector.updateTranslation();
				if(selector.selectedCategory == Specs.motionCategory){
						selector.selectedCategory = Specs.looksCategory;
					}		
			selector.select(selector.selectedCategory);
		}
		
		//Following function needs to be modified!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		public function updateTranslation():void{
			backBt.setLabel(Translator.map("Back"));
			uploadBt.setLabel(Translator.map("Upload to Arduino"));
			//		openBt.setLabel(Translator.map("Edit with Arduino IDE"));
			//		sendBt.setLabel(Translator.map("Send"));
			//		displayModeBtn.setLabel(Translator.map(isByteDisplayMode ? "binary mode" :  "char mode"));
			//		inputModeBtn.setLabel(Translator.map(isByteInputMode ? "binary mode" :  "char mode"));
			//Steven, WiFi connect
			//		wifiBt.setLabel(Translator.map("Init"));
			
			
		}
		public function updateSpriteWatermark():void {
			var target:ScratchObj = app.viewedObj();
			if (target && !target.isStage) {
				//9. Malcolm
				//spriteWatermark.bitmapData = target.currentCostume().thumbnail(40, 40, false);
			} else {
				//9. Malcolm
				//spriteWatermark.bitmapData = null;
			}
		}
		
		public function step():void {
			// Update the mouse reaadouts. Do nothing if they are up-to-date (to minimize CPU load).
			var target:ScratchObj = app.viewedObj();
			if (target.isStage) {
				if (xyDisplay.visible) xyDisplay.visible = false;
			} else {
				//9. Malcolm 
				//if (!xyDisplay.visible) xyDisplay.visible = true;
				
				var spr:ScratchSprite = target as ScratchSprite;
				if (!spr) return;
				if (spr.scratchX != lastX) {
					lastX = spr.scratchX;
					//9. Malcolm
					//xReadout.text = String(lastX);
				}
				if (spr.scratchY != lastY) {
					lastY = spr.scratchY;
					//9. Malcolm
					//yReadout.text = String(lastY);
				}
			}
			updateExtensionIndicators();
		}
		
		private var lastUpdateTime:uint;
		private var ListAccImpl:Object;
		
		private function updateExtensionIndicators():void {
			if ((getTimer() - lastUpdateTime) < 500) return;
			for (var i:int = 0; i < app.palette.numChildren; i++) {
				var indicator:IndicatorLight = app.palette.getChildAt(i) as IndicatorLight;
				if (indicator) app.extensionManager.updateIndicator(indicator, indicator.target);
			}		
			lastUpdateTime = getTimer();
		}
		
		public function setWidthHeight(w:int, h:int):void {
			this.w = w;
			this.h = h;
			fixlayout();
			redraw();
		}
		
		public static var wf_icon_x: int = 1000;
		public function fixlayout():void {
			selector.x = 1;
			selector.y = 5;
			paletteFrame.x = selector.x;
			paletteFrame.y = selector.y + selector.height + 2;
			paletteFrame.setWidthHeight(selector.width + 1, h - paletteFrame.y - 2); // 模块滚动区域宽度
			scriptsFrame.x = selector.x + selector.width + 2;
			scriptsFrame.y = selector.y + 1;
//			var arduinoWidth:uint = app.stageIsArduino?(w/2-150):0;
			var arduinoWidth:uint = 330;
			var arduinoHeight:uint = h - 10;
			arduinoFrame.visible = app.stageIsArduino;
			scriptsFrame.setWidthHeight(w - scriptsFrame.x - 15-arduinoWidth, h - scriptsFrame.y - 5);//代码区
			arduinoFrame.x = scriptsFrame.x+ (w - scriptsFrame.x - 15-arduinoWidth)+10;
			arduinoFrame.y = scriptsFrame.y;
			arduinoFrame.setWidthHeight(arduinoWidth, arduinoHeight);
			lineNumText.x = 4;
			lineNumText.y = 45;
			lineNumText.width = lineNumWidth;
			lineNumText.height = arduinoHeight-255;
			
			wf_icon_x = arduinoFrame.x + arduinoWidth-messageTextPane.x;
			//text label
			MBlock.app.topBarPart.updateTranslation();
			fl_label.x = 0;
			fl_label.y = 0;
			fl_label.setSize(arduinoWidth-messageTextPane.x-2,20);
			fl_label.text = " No.  |      Voice Command  |    Project";
//			fl_label.textField.text = " No.  |      Voice Command";
			var font:String = Resources.chooseFont([
				'Lucida Grande', 'Verdana', 'Arial', 'DejaVu Sans']);
//			myFormat = new TextFormat(font, 12, 0x505050, false);
			myFormat = new TextFormat(font, 12, CSS.textNoVCProject, false); // dark grey
			fl_label.textField.defaultTextFormat = myFormat; 
			
			fl_panel.x = 4;
			fl_panel.y = 0;//was 37
			fl_panel.setSize(arduinoWidth-messageTextPane.x-2,19);
			fl_panel.content.addChild(fl_label);
			
			//for debug
			arduinoTextPane.setWidthHeight(300,250);//from 255 to 455
			arduinoTextPane.x = 350;//;10 for debug, 350 for release
			arduinoTextPane.y = 150;//arduinoHeight - 53;
				
			messageTextPane.x = lineNumText.x;
			messageTextPane.y = 60+UIfile_list.height - 37;
			messageTextPane.setWidthHeight(arduinoWidth-messageTextPane.x,arduinoHeight -430);
			messageTextPane.textField.backgroundColor = 0xF1F1F1;
			//		openBt.x = arduinoWidth - openBt.width - 10;
			//		sendTextPane.x = 8 + displayModeBtn.width;
			sendTextPane.x = 4;
			sendTextPane.y = arduinoHeight - 80;
			sendTextPane.width = (arduinoWidth-messageTextPane.x);
			sendTextPane.height = (20);
			sendTextPane.selectable = false
				
			//show sendTextPane_translated one, hide sendTextPane
			sendTextPane_translated.x = 4;
			sendTextPane_translated.y = arduinoHeight - 20;
			sendTextPane_translated.width = (arduinoWidth-messageTextPane.x);
			sendTextPane_translated.height = (20);
			sendTextPane_translated.selectable = false
				
				
			//Buttons
			wifiBt.x = arduinoWidth-messageTextPane.x-150+50;
			wifiBt.y = 50;
			uploadwbBt.x = arduinoWidth-messageTextPane.x-150+50;
			uploadwbBt.y = 90;
			filerunBt.x = arduinoWidth-messageTextPane.x-150+50;
			filerunBt.y = 130;
			filedelBt.x = arduinoWidth-messageTextPane.x-150+50;
			filedelBt.y = 170;
			
			startBt_icon.x = 10;
			startBt_icon.y = 2;
			SimpleTooltips.add(startBt_icon, {text: 'Start Project Selected', direction: 'bottom'});
			
			stopBt_icon.x = 64;
			stopBt_icon.y = 6;
			SimpleTooltips.add(stopBt_icon, {text: 'Stop Project', direction: 'bottom'});
			
			uploadBt_icon.x = 110;
			uploadBt_icon.y = 2;
			SimpleTooltips.add(uploadBt_icon, {text: 'Upload Project Selected', direction: 'bottom'});
			
			connectBt_icon.x = 170;
			connectBt_icon.y = -14;
			SimpleTooltips.add(connectBt_icon, {text: 'Init WiFi connection with Robot', direction: 'bottom'});		
			
			deleteBt_icon.x = 260;
			deleteBt_icon.y = 2;
			SimpleTooltips.add(deleteBt_icon, {text: 'Delete Project Selected', direction: 'bottom'});
			
			//Steven file_list !!
			UIfile_list.x = 4;
//			UIfile_list.y = arduinoHeight-380;
			UIfile_list.y = 20;//was 57
			UIfile_list.width = arduinoWidth-messageTextPane.x;
			UIfile_list.height = arduinoHeight - 600 + 37;
			UIfile_list.mouseEnabled = true;
			UIfile_list.listItemHeight = 25;
			
			UIfile_list.addEventListener(Event.SELECT, onIndexChange);
			UIfile_list.defaultColor = CSS.listColor; // list color
		
			wb_panel_pic.x = lineNumText.x-1;//lineNumText.x+10+30;
			wb_panel_pic.y = 60+UIfile_list.height+10-1-37;//60+UIfile_list.height+10;
			wb_panel_pic.width = arduinoWidth-messageTextPane.x;//arduinoWidth-messageTextPane.x-35-40;
			wb_panel_pic.height = 500-40;//500-80;
			
//			wbpic.x = lineNumText.x;//lineNumText.x+10+30;
//			wbpic.y = 60+UIfile_list.height+10;//60+UIfile_list.height+10;
			wbpic.x = 0;//lineNumText.x+10+30;
			wbpic.y = 0;//60+UIfile_list.height+10;
			wbpic.width = arduinoWidth-messageTextPane.x;//arduinoWidth-messageTextPane.x-35-40;
			wbpic.height = 500-40;//500-80;
			
			//12 text
			myFormat = new TextFormat(font, 10, CSS.textNoVCProject, false);
			input_lb_1.defaultTextFormat =  myFormat; 
			input_lb_2.defaultTextFormat =  myFormat; 
			input_lb_3.defaultTextFormat =  myFormat;
			input_lb_4.defaultTextFormat =  myFormat;
			input_lb_5.defaultTextFormat =  myFormat;
			input_lb_6.defaultTextFormat =  myFormat;
			input_lb_7.defaultTextFormat =  myFormat;
			input_lb_8.defaultTextFormat =  myFormat;
			input_lb_9.defaultTextFormat =  myFormat;
			input_lb_10.defaultTextFormat =  myFormat;
			input_lb_11.defaultTextFormat =  myFormat;
			input_lb_12.defaultTextFormat =  myFormat;

			input_text_1.textField.defaultTextFormat = myFormat; 
			input_text_2.textField.defaultTextFormat = myFormat; 
			input_text_3.textField.defaultTextFormat = myFormat; 
			input_text_4.textField.defaultTextFormat = myFormat; 
			input_text_5.textField.defaultTextFormat = myFormat; 
			input_text_6.textField.defaultTextFormat = myFormat; 
			input_text_7.textField.defaultTextFormat = myFormat; 
			input_text_8.textField.defaultTextFormat = myFormat; 
			input_text_9.textField.defaultTextFormat = myFormat; 
			input_text_10.textField.defaultTextFormat = myFormat; 
			input_text_11.textField.defaultTextFormat = myFormat; 
			input_text_12.textField.defaultTextFormat = myFormat; 
			
			wb_panel_1.x = wb_panel_pic.x+3;//wbpic.x + 15 ;
			wb_panel_1.y = wb_panel_pic.y+15;//wbpic.y + 15 + 400;
			wb_panel_1.setSize(70, 33);
			wb_panel_1.content.addChild(input_text_1);
			wb_panel_1.content.addChild(input_lb_1);	
			input_text_1.width = wb_panel_1.width;
			input_text_1.maxChars = 4;
			input_text_1.x = 0;
			input_text_1.y = 17;
			input_lb_1.width = wb_panel_1.width;
			input_lb_1.x = 0;
			input_lb_1.y = 0;
			input_lb_1.text = "M1(-39~39)";
			SimpleTooltips.add(wb_panel_1, {text: 'Shake Head Motor', direction: 'right'});
			
			wb_panel_2.x = wbpic.x +190 + 15 + 15;
			wb_panel_2.y = wb_panel_pic.y+65;
			wb_panel_2.setSize(70, 33);
			wb_panel_2.content.addChild(input_text_2);
			wb_panel_2.content.addChild(input_lb_2);	
			input_text_2.width = wb_panel_2.width;
			input_text_2.maxChars = 4;
			input_text_2.x = 0;
			input_text_2.y = 17;
			input_lb_2.width = wb_panel_2.width;
			input_lb_2.x = 0;
			input_lb_2.y = 0;
			input_lb_2.text = "M2(-7~2)";
			SimpleTooltips.add(wb_panel_2, {text: 'Nod Head Motor', direction: 'left'});
			
			wb_panel_3.x = wb_panel_pic.x+3;//wbpic.x -40 + 15 + 15 + 15;
			wb_panel_3.y = wb_panel_pic.y+105;//wbpic.y + 105+ 395;
			wb_panel_3.setSize(85, 33);
			wb_panel_3.content.addChild(input_text_3);
			wb_panel_3.content.addChild(input_lb_3);	
			input_text_3.width = wb_panel_3.width;
			input_text_3.maxChars = 4;
			input_text_3.x = 0;
			input_text_3.y = 17;
			input_lb_3.width = wb_panel_3.width;
			input_lb_3.x = 0;
			input_lb_3.y = 0;
			input_lb_3.text = "M3(-110~105)";
			SimpleTooltips.add(wb_panel_3, {text: 'Motor No.3', direction: 'right'});
			
			wb_panel_4.x = wbpic.x +210 + 15+8+8;
			wb_panel_4.y = wb_panel_pic.y+105;
			wb_panel_4.setSize(85, 33);
			wb_panel_4.content.addChild(input_text_4);
			wb_panel_4.content.addChild(input_lb_4);	
			input_text_4.width = wb_panel_4.width;
			input_text_4.maxChars = 4;
			input_text_4.x = 0;
			input_text_4.y = 17;
			input_lb_4.width = wb_panel_4.width;
			input_lb_4.x = 0;
			input_lb_4.y = 0;
			input_lb_4.text = "M4(-105~110)";
			SimpleTooltips.add(wb_panel_4, {text: 'Motor No.4', direction: 'left'});
			
			wb_panel_5.x = wb_panel_pic.x+3;//wbpic.x -40 + 15 + 15 + 15;
			wb_panel_5.y = wb_panel_pic.y+105+50;//wbpic.y + 155+ 395;
			wb_panel_5.setSize(70, 33);
			wb_panel_5.content.addChild(input_text_5);
			wb_panel_5.content.addChild(input_lb_5);	
			input_text_5.width = wb_panel_5.width;
			input_text_5.maxChars = 4;
			input_text_5.x = 0;
			input_text_5.y = 17;
			input_lb_5.width = wb_panel_5.width;
			input_lb_5.x = 0;
			input_lb_5.y = 0;
			input_lb_5.text = "M5(-79~26)";
			SimpleTooltips.add(wb_panel_5, {text: 'Motor No.5', direction: 'right'});
			
			wb_panel_6.x = wbpic.x +210 + 15+23+8;
			wb_panel_6.y = wb_panel_pic.y+105+50;
			wb_panel_6.setSize(70, 33);
			wb_panel_6.content.addChild(input_text_6);
			wb_panel_6.content.addChild(input_lb_6);	
			input_text_6.width = wb_panel_6.width;
			input_text_6.maxChars = 4;
			input_text_6.x = 0;
			input_text_6.y = 17;
			input_lb_6.width = wb_panel_6.width;
			input_lb_6.x = 0;
			input_lb_6.y = 0;
			input_lb_6.text = "M6(-26~79)";
			SimpleTooltips.add(wb_panel_6, {text: 'Motor No.6', direction: 'left'});
			
			wb_panel_7.x = wb_panel_pic.x+3;//wbpic.x -40 + 15 + 15 + 15 ;
			wb_panel_7.y = wb_panel_pic.y+295;//wbpic.y + 295+ 395;
			wb_panel_7.setSize(85, 33);
			wb_panel_7.content.addChild(input_text_7);
			wb_panel_7.content.addChild(input_lb_7);	
			input_text_7.width = wb_panel_7.width;
			input_text_7.maxChars = 4;
			input_text_7.x = 0;
			input_text_7.y = 17;
			input_lb_7.width = wb_panel_7.width;
			input_lb_7.x = 0;
			input_lb_7.y = 0;
			input_lb_7.text = "M7(-105~123)";
			SimpleTooltips.add(wb_panel_7, {text: 'Motor No.7', direction: 'right'});
	
			wb_panel_8.x = wbpic.x +210 + 15+8 +8;
			wb_panel_8.y = wb_panel_pic.y+295;
			wb_panel_8.setSize(85, 33);
			wb_panel_8.content.addChild(input_text_8);
			wb_panel_8.content.addChild(input_lb_8);	
			input_text_8.width = wb_panel_8.width;
			input_text_8.maxChars = 4;
			input_text_8.x = 0;
			input_text_8.y = 17;
			input_lb_8.width = wb_panel_8.width;
			input_lb_8.x = 0;
			input_lb_8.y = 0;
			input_lb_8.text = "M8(-123~105)";
			SimpleTooltips.add(wb_panel_8, {text: 'Motor No.8', direction: 'left'});
			
			wb_panel_9.x = wb_panel_pic.x+3;//wbpic.x -40 + 15 + 15 + 15;
			wb_panel_9.y = wb_panel_pic.y+355;//wbpic.y + 355+ 395;
			wb_panel_9.setSize(85, 33);
			wb_panel_9.content.addChild(input_text_9);
			wb_panel_9.content.addChild(input_lb_9);	
			input_text_9.width = wb_panel_9.width;
			input_text_9.maxChars = 4;
			input_text_9.x = 0;
			input_text_9.y = 17;
			input_lb_9.width = wb_panel_9.width;
			input_lb_9.x = 0;
			input_lb_9.y = 0;
			input_lb_9.text = "M9(-87~92)";
			SimpleTooltips.add(wb_panel_9, {text: 'Motor No.9', direction: 'right'});
			
			wb_panel_10.x = wbpic.x +210 + 15+8 ;
			wb_panel_10.y = wb_panel_pic.y+355;
			wb_panel_10.setSize(93, 33);
			wb_panel_10.content.addChild(input_text_10);
			wb_panel_10.content.addChild(input_lb_10);	
			input_text_10.width = wb_panel_10.width;
			input_text_10.maxChars = 4;
			input_text_10.x = 0;
			input_text_10.y = 17;
			input_lb_10.width = wb_panel_10.width;
			input_lb_10.x = 0;
			input_lb_10.y = 0;
			input_lb_10.text = "M10(-92~87)";
			SimpleTooltips.add(wb_panel_10, {text: 'Motor No.10', direction: 'left'});
			
			wb_panel_11.x = wb_panel_pic.x+3;//wbpic.x -40 + 15 + 15 + 15;
			wb_panel_11.y = wb_panel_pic.y+415;//wbpic.y + 415+ 395 ;
			wb_panel_11.setSize(85, 33);
			wb_panel_11.content.addChild(input_text_11);
			wb_panel_11.content.addChild(input_lb_11);	
			input_text_11.width = wb_panel_11.width;
			input_text_11.maxChars = 4;
			input_text_11.x = 0;
			input_text_11.y = 17;
			input_lb_11.width = wb_panel_11.width;
			input_lb_11.x = 0;
			input_lb_11.y = 0;
			input_lb_11.text = "M11(-10~31)";
			SimpleTooltips.add(wb_panel_11, {text: 'Motor No.11', direction: 'right'});
			
			wb_panel_12.x = wbpic.x +210 + 15 +8+8;
			wb_panel_12.y = wb_panel_pic.y+415;
			wb_panel_12.setSize(85, 33);
			wb_panel_12.content.addChild(input_text_12);
			wb_panel_12.content.addChild(input_lb_12);	
			input_text_12.width = wb_panel_12.width;
			input_text_12.maxChars = 4;
			input_text_12.x = 0;
			input_text_12.y = 17;
			input_lb_12.width = wb_panel_12.width;
			input_lb_12.x = 0;
			input_lb_12.y = 0;
			input_lb_12.text = "M12(-10~31)";
			SimpleTooltips.add(wb_panel_12, {text: 'Motor No.12', direction: 'left'});


			
			function onIndexChange(event:Event):void
			{
				trace(dp[UIfile_list.selectedIndex]);
				UIrunfelstr = dp[UIfile_list.selectedIndex];
				UIdelfelstr = dp[UIfile_list.selectedIndex];
			}
			
//			Steven, Script test
//			messageTextPane.x = lineNumText.x;
//			messageTextPane.y = h -210
			pullBt.x = 180;
			pullBt.y = h -390+155;
//			pullBt.addEventListener(MouseEvent.CLICK,pull_filelog);
//			arduinoFrame.addChild(pullBt);
			
			filelistBt.x = 250;
			filelistBt.y = h -390+155;
//			filelistBt.addEventListener(MouseEvent.CLICK,check_filelog);
//			arduinoFrame.addChild(filelistBt);
			
			filedelBt.addEventListener(MouseEvent.CLICK,delete_file_timer);
			filerunBt.addEventListener(MouseEvent.CLICK,run_file_timer);

			startBt_icon.addEventListener(MouseEvent.CLICK,run_file_timer);
			startBt_icon.isMomentary = true;
			stopBt_icon.addEventListener(MouseEvent.CLICK,stop_file_timer);
			stopBt_icon.isMomentary = true;
			deleteBt_icon.addEventListener(MouseEvent.CLICK,delete_file_timer);
			deleteBt_icon.isMomentary = true;
			uploadBt_icon.addEventListener(MouseEvent.CLICK,onuploadwbBt_timer);
			uploadBt_icon.isMomentary = true;
			connectBt_icon.addEventListener(MouseEvent.CLICK,onWiFiConnect_timer);
			connectBt_icon.isMomentary = true;			
			
			saveMovBt.isMomentary = true;		
			delMovBt.isMomentary = true;		
			testMovBt.isMomentary = true;		
			testMovBt2.isMomentary = true;		
						
			motor_pwon.isMomentary = true;		
			motor_pwof.isMomentary = true;		
			
			//power buttons
			motor_pwon.height = 28;
			motor_pwon.width = 28;
			motor_pwon.x = 118;
			motor_pwon.y = arduinoHeight - 51;
			SimpleTooltips.add(motor_pwon, {text: 'Power on all motors', direction: 'bottom'});		
			
			motor_pwof.height = 28;
			motor_pwof.width = 28;
			motor_pwof.x = 172;
			motor_pwof.y = arduinoHeight - 51;
			SimpleTooltips.add(motor_pwof, {text: 'Power off all motors', direction: 'bottom'});		
			
			
			//14. Malcolm save btn
			saveMovBt.height = 28;
			saveMovBt.width = 28;
			saveMovBt.x = 10;
			saveMovBt.y = arduinoHeight - 51;
			SimpleTooltips.add(saveMovBt, {text: 'Save edited movements as a block in Movement section, so that it can be reused', direction: 'bottom'});		
//			saveMovBt.x = arduinoWidth - saveMovBt.width;
//			saveMovBt.y = arduinoHeight - 35;
			// 14. 
			//15. Malcolm del btn
			delMovBt.height = 28;
			delMovBt.width = 28;
			delMovBt.x = 64;
			delMovBt.y = arduinoHeight - 51;
			SimpleTooltips.add(delMovBt, {text: 'Delete movements block from Movement section', direction: 'bottom'});		
			testMovBt.height = 28;
			testMovBt.width  = 28;
			testMovBt.x = 118;
			testMovBt.y = arduinoHeight - 51;
			SimpleTooltips.add(testMovBt, {text: 'Upload current movements(Orange "My Movement" blocks)', direction: 'bottom'});		
			testMovBt2.height  = 28;
			testMovBt2.width  = 28;
			testMovBt2.x = 64;//172;
			testMovBt2.y = arduinoHeight - 51;
			SimpleTooltips.add(testMovBt2, {text: 'Run current movements to check its behaviour', direction: 'bottom'});		
			
//			delMovBt.x = arduinoWidth - delMovBt.width - 55;
//			delMovBt.y = arduinoHeight - 35;
			//		sendBt.x = arduinoWidth - sendBt.width - 10;
			//		sendBt.y = arduinoHeight - 35;
			//		displayModeBtn.x = messageTextPane.x + messageTextPane.width - displayModeBtn.width;
			//		displayModeBtn.y = messageTextPane.y;
			//		inputModeBtn.x = lineNumText.x;
			//		inputModeBtn.y = sendBt.y;
			arduinoTextPane.updateScrollbar(null);
			messageTextPane.updateScrollbar(null);
			//9. Malcolm
			//spriteWatermark.x = w - arduinoWidth - 60;
			//spriteWatermark.y = scriptsFrame.y + 10;
			//9. Malcolm
			//xyDisplay.x = spriteWatermark.x + 1;
			//xyDisplay.y = spriteWatermark.y + 43;
			zoomWidget.x = w - arduinoWidth - zoomWidget.width - 30;
			zoomWidget.y = h - zoomWidget.height - 15;
		}
		
		private function redraw():void {
			var paletteW:int = paletteFrame.visibleW();
			var paletteH:int = paletteFrame.visibleH();
			var scriptsW:int = scriptsFrame.visibleW();
			var scriptsH:int = scriptsFrame.visibleH();
			
			var g:Graphics = shape.graphics;
			g.clear();
			g.lineStyle(1, CSS.borderColor, 1, true);
			g.beginFill(CSS.tabColor);
			g.drawRect(0, 0, w, h);
			g.endFill();
			
			var lineY:int = selector.y + selector.height;
			var darkerBorder:int = CSS.borderColor - 0x141414;
			var lighterBorder:int = 0xF2F2F2;
			g.lineStyle(1, darkerBorder, 1, true);
			hLine(g, paletteFrame.x + 8, lineY, paletteW - 20);
			g.lineStyle(1, lighterBorder, 1, true);
			hLine(g, paletteFrame.x + 8, lineY + 1, paletteW - 20);
			
			g.lineStyle(1, darkerBorder, 1, true);
			g.drawRect(scriptsFrame.x - 1, scriptsFrame.y - 1, scriptsW + 1, scriptsH + 1);
		}
		
		private function hLine(g:Graphics, x:int, y:int, w:int):void {
			g.moveTo(x, y);
			g.lineTo(x + w, y);
		}
		
		//9. Malcolm
		//private function addXYDisplay():void {
		//xyDisplay = new Sprite();
		//xyDisplay.addChild(xLabel = makeLabel('x:', readoutLabelFormat, 0, 0));
		//xyDisplay.addChild(xReadout = makeLabel('-888', readoutFormat, 15, 0));
		//xyDisplay.addChild(yLabel = makeLabel('y:', readoutLabelFormat, 0, 13));
		//xyDisplay.addChild(yReadout = makeLabel('-888', readoutFormat, 15, 13));
		//addChild(xyDisplay);
		//}
		
	}}
import ui.parts;