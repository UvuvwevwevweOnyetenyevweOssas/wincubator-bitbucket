package extensions
{
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.NativeProcessExitEvent;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	import flash.system.Capabilities;
	import flash.utils.getQualifiedClassName;
	
	import blocks.Block;
	import blocks.BlockIO;
	
	import cc.makeblock.mbot.util.StringUtil;
	
	import translation.Translator;
	
	import ui.parts.ScriptsPart;
	
	import uiwidgets.DialogBox;
	
	import util.ApplicationManager;
	import util.JSON;
	import util.LogManager;
	import util.SharedObjectManager;
	
	public class ArduinoManager extends EventDispatcher
	{	
		private static var _instance:ArduinoManager;
		public var _scratch:MBlock;
		public var jsonObj:Object;
		
		public var hexCode:String;
		public var token:String;
		public var output:String;
		public var ccode:String = "";
		public var hexPath:String;
		public var isUploading:Boolean = false;
		
		private var process:NativeProcess;
		public var hasUnknownCode:Boolean = false;
		private var ccode_setup:String = "";
		private var ccode_setup_fun:String = "";
		private var ccode_setup_def:String = "";
		private var ccode_loop:String = ""
		private var ccode_def:String = ""
		private var ccode_inc:String = ""
		private var ccode_pointer:String="setup"
		private var ccode_func:String = "";
		private var mathOp:Array=["+","-","*","/","%",">","=","<","&","|","!","not","rounded"];
		private var varList:Array = [];
		private var varStringList:Array = [];
		private var varListWrite:Array=[]
		private var paramList:Array=[]
		private var moduleList:Array=[];
		private var funcList:Array = [];
		
		public var unknownBlocks:Array = [];
		
		public var scriptsPart:ScriptsPart;
		
		
		// maintance of project and arduino path
		private var arduinoPath:String;
		private var avrPath:String = "";
		private var arduinoLibPath:String = "";
		private var projectPath:String = "";
		private var _currentDevice:String;
		private var _extSrcPath:String = "";
		private var leoPortMap:Array=[[null,null],["11","A8"],["13","A11"],["10","9"],["1","0"],["MISO","SCK"],["A0","A1"],["A2","A3"],["A4","A5"],["6","7"],["5","4"]]
		private var portEnum:Object = {"Port1":1,"Port2":2,"Port3":3,"Port4":4,"Port5":5,"Port6":6,"Port7":7,"Port8":8,"M1":9,"M2":10}
		private var portPortEnum:Object={"Port1":"PORT_1","Port2":"PORT_2","Port3":"PORT_3","Port4":"PORT_4","Port5":"PORT_5","Port6":"PORT_6","Port7":"PORT_7","Port8":"PORT_8","M1":"M1","M2":"M2"}
		private var slotSlotEnum:Object={"Slot1":"SLOT_1","Slot2":"SLOT_2"}
		private var slotEnum:Object = {"Slot1":0,"Slot2":1}
		private var noteEnum:Object = {"B0":31,"C1":33,"D1":37,"E1":41,"F1":44,"G1":49,"A1":55,"B1":62,
			"C2":65,"D2":73,"E2":82,"F2":87,"G2":98,"A2":110,"B2":123,
			"C3":131,"D3":147,"E3":165,"F3":175,"G3":196,"A3":220,"B3":247,
			"C4":262,"D4":294,"E4":330,"F4":349,"G4":392,"A4":440,"B4":494,
			"C5":523,"D5":587,"E5":659,"F5":698,"G5":784,"A5":880,"B5":988,
			"C6":1047,"D6":1175,"E6":1319,"F6":1397,"G6":1568,"A6":1760,"B6":1976,
			"C7":2093,"D7":2349,"E7":2637,"F7":2794,"G7":3136,"A7":3520,"B7":3951,
			"C8":4186,"D8":4699,"Half":500,"Quater":250,"Eighth":125,"Whole":1000,"Double":2000,"Zero":0};
		private var EVENT_NATIVE_DONE:String = "EVENT_NATIVE_DONE"
		private var EVENT_LIBCOMPILE_DONE:String = "EVENT_LIBCOMPILE_DONE"
		private var EVENT_COMPILE_DONE:String = "EVENT_COMPILE_DONE"
		
		public var mainX:int = 0;
		public var mainY:int = 0;
		
		//malcolm add in
		private var motionCode:String = "";
		private var controlCode:String = "";
		private var loopCode:String = "";
		//for log 13.
		private var logCode:String = "";
		//Steven 
		private var ledCode:String = "";
		private var soundCode:String = "";
		
	   public static var file_name_FLAG: Boolean = false;
	   public static var file_name_FLAG_result: Boolean = false;
		
		/*
		
		char buffer[64];
		String lastLine;
		int bufferIndex = 0;
		boolean dataLineAvailable(){
		if(Serial.available()){
		char c = Serial.read();
		if(c=='\n'){
		buffer[bufferIndex] = 0;
		return true;
		}else{
		buffer[bufferIndex]=c;
		bufferIndex++;
		}
		}
		return false;
		}
		String readDataLine(){
		if(bufferIndex>0){
		lastLine = buffer;
		}
		bufferIndex = 0;
		memset(buffer,0,64);
		return lastLine;
		}
		String concatenateWith(String s1,String s2){
		return s1+s2;
		}
		char letterOf(int i,String s){
		return s.charAt(i);
		}
		int stringLength(String s){
		return s.length();
		}		
		*/
		
		private var codeTemplate:String = ( <![CDATA[#include <Arduino.h>
#include <Wire.h>
#include <Servo.h>
#include <SoftwareSerial.h>

//include
double angle_rad = PI/180.0;
double angle_deg = 180.0/PI;
//define
//serialParser
//function
void setup(){
//setup
}

void loop(){
//serialParserCall
//loop
}

]]> ).toString();//delay(50);

		
//		Original godlike code after CDATA: #!/bin/bash
//		/data/local/tmp/busybox
//		am broadcast -a com.android.tdc.motor.poweron
		private var codeTemplateBASH:String = ( <![CDATA[{
//include
//define
//serialParser
//function
//setup
//serialParserCall
//loop
}]]> ).toString();//delay(50);
		
		private var codeTemplateLOG:String = ( <![CDATA[//movement
]]> ).toString();//delay(50); //#
//		#
//		#
//		#
//		#
//		#
//		#
		
		private var codeTemplateJSON:String = ( <![CDATA[
{
//include
//define
//serialParser
//function
//setup
//serialParserCall
//loop
}
]]> ).toString();//delay(50);
		
		private var codeSerialParser:String = ( <![CDATA[
char inputBuf[64];
int inputIndex;
void parseSerialInput(){
if(Serial.available()){
char c = Serial.read();
inputBuf[inputIndex++] = c;
if(c=='\\n'){
int value;
//parseList
memset(inputBuf,0,64);
inputIndex = 0;
}
}
}
]]> ).toString();
		
		private var codeSerialScanf:String = ( <![CDATA[
if(sscanf(inputBuf,"param=%d",&value)){
param = value;
//Serial.printf("param=%d\\n",value);
}
]]> ).toString();
		
		private var serialParserInoFile:String = ( <![CDATA[
char buf[64];
char readLine[64];
bool lineParsed = true;
int bufIndex = 0;

void updateVar(char * varName,double * var)
{
  char tmp[16];
  int value,i;
  while(Serial.available()){
	char c = Serial.read();
	buf[bufIndex++] = c;
	if(c=='\n'){
	  memset(readLine,0,64);
	  memcpy(readLine,buf,bufIndex);
	  memset(buf,0,64);
	  bufIndex = 0;
	  lineParsed = false;
	}
  }
  if(!lineParsed){
	char * tmp;
	char * str;
	str = strtok_r(readLine, "=", &tmp);
	if(str!=NULL && strcmp(str,varName)==0){
	  float v = atof(tmp);
	  *var = v;
	  lineParsed = true;
	}
  }
}
]]> ).toString();
		
		public static function sharedManager():ArduinoManager{
			if(_instance==null){
				_instance = new ArduinoManager;
			}
			return _instance;
		} 
		
		public function ArduinoManager()
		{
			addEventListener(EVENT_NATIVE_DONE, gotoNextNativeCmd)
			addEventListener(EVENT_LIBCOMPILE_DONE,runToolChain,false)
			addEventListener(EVENT_COMPILE_DONE,uploadHex,false);
		}
		
		public function clearTempFiles():void
		{
			/*
			var workdir:File = File.applicationStorageDirectory.resolvePath("scratchTemp");
			if(workdir.exists){
				workdir.deleteDirectory(true);
			}
			//*/
			if(File.applicationStorageDirectory.exists){
				File.applicationStorageDirectory.deleteDirectory(true);
			}
		}
		
		public function setScratch(scratch:MBlock):void{
			_scratch = scratch;
		}
		
		private function portMapPin(port:String,slot:String):String{
			var pin:String = leoPortMap[portEnum[port]][slotEnum[slot]]
			return pin
		}
		
		private function portMapPort(port:String):String{
			return portPortEnum[port]
		}
		
		private function slotMapSlot(slot:String):String{
			return slotSlotEnum[slot]
		}
		
		private function parseMath(blk:Object):CodeObj{
			var op:Object= blk[0]
			var mp1:CodeBlock=getCodeBlock(blk[1]);
			var mp2:CodeBlock=getCodeBlock(blk[2]);
			if(op=="="){
				op="==";
			}
			if(mp1.type=="string"){
				if(!isNaN(Number(mp1.code))){
					mp1.type = "number";
					mp1.code = Number(mp1.code);
				}
			}
			if(mp2.type=="string"){
				if(!isNaN(Number(mp2.code))){
					mp2.type = "number";
					mp2.code = Number(mp2.code);
				}
			}
			//			var isStringValue:Boolean = false;
			//			if(getQualifiedClassName(mp1) == "Array")
			//				mp1 = getCodeBlock(blk[1])
			//			if(getQualifiedClassName(mp2) == "Array")
			//				mp2 = getCodeBlock(blk[2]);
			//			else
			//			{
			//				if(mp1 is String){
			//					isStringValue = mp1.indexOf("readDataLine")>-1;
			//				}
			//			}
			var code:String = StringUtil.substitute("({0}) {1} ({2})",mp1.type=="obj"?mp1.code.code:mp1.code ,op,mp2.type=="obj"?mp2.code.code:mp2.code);
			if(op=="=="){
				if(mp1.type=="string"&&mp2.type=="string"){
					code = StringUtil.substitute("({0}.equals(\"{1}\"))",mp1.code,mp2.code);
				}else{
					code = StringUtil.substitute("(({0})==({1}))",mp1.type=="obj"?mp1.code.code:mp1.code,mp2.type=="obj"?mp2.code.code:mp2.code);
				}
			}else if(op=="%"){
				code = StringUtil.substitute("fmod({0},{1})",mp1.type=="obj"?mp1.code.code:mp1.code,mp2.type=="obj"?mp2.code.code:mp2.code);
			}else if(op=="not"){
				code = StringUtil.substitute("!({0})",mp1.type=="obj"?mp1.code.code:mp1.code);
			}else if(op=="rounded"){
				code = StringUtil.substitute("round({0})",mp1.type=="obj"?mp1.code.code:mp1.code);
			}
			return new CodeObj(code);
		}
		
		
		private function parseVarRead(blk:Object):CodeObj{
			var varName:Object = blk[1]
			if(varList.indexOf(varName)==-1){
				varList.push(varName);
			}
			var code:CodeObj = new CodeObj(StringUtil.substitute("{0}",castVarName(varName.toString())));
			return code;
		}
		
		private function parseVarSet(blk:Object):String{
			var varName:String = blk[1]
			if(varList.indexOf(varName)==-1)
				varList.push(varName)
			var varValue:* = blk[2] is CodeObj?blk[2].code:blk[2];
			if(getQualifiedClassName(varValue) == "Array"){
				varValue = getCodeBlock(varValue);
				if(varValue.type=="obj"){
					if(varValue.code.code.indexOf("ir.getString()")>-1){
						varStringList.push(varName);
					}
				}else if(varValue.type=="string"){
					if(varStringList.indexOf(varName)==-1){
						varStringList.push(varName);
					}
				}
				return (StringUtil.substitute("{0} = {1};\n",castVarName(varName),varValue.type=="obj"?varValue.code.code:varValue.code))
			}else{
				return (StringUtil.substitute("{0} = {1};\n",castVarName(varName),varValue is CodeObj?varValue.code:varValue));
			}
		}
		
		private function parseVarShow(fun:Object):CodeObj{
			var param:Object = fun[1]
			if(paramList.indexOf(param)==-1)
				paramList.push(param)
			var funcode:CodeObj=new CodeObj(StringUtil.substitute("Serial.print(\"{0}=\");Serial.println(\"{1}\");\n",param,param));
			return funcode;
		}
		
		private function parseDelay(fun:Object):String{
			var cBlk:CodeBlock=getCodeBlock(fun[1]);
			var funcode:String=(StringUtil.substitute("delay(1000*{0});\n",cBlk.type=="obj"?cBlk.code.code:cBlk.code));
			return funcode;
		}
		private function parseDoRepeat(blk:Object):String{
			var initCode:CodeBlock = getCodeBlock(blk[1]);
			var repeatCode:String=StringUtil.substitute("for(int i=0;i<{0};i++)\n{\n",initCode.type=="obj"?initCode.code.code:initCode.code);
			if(blk[2]!=null){
				for(var i:int=0;i<blk[2].length;i++){
					var b:Object = blk[2][i]
					var cBlk:CodeBlock=getCodeBlock(b);
					repeatCode+=cBlk.type=="obj"?cBlk.code.code:cBlk.code;
				}
			}
			repeatCode+="}\n";
			return repeatCode;
		}
		private function parseDoWaitUntil(blk:Object):String{
			var initCode:CodeBlock = getCodeBlock(blk[1]);
			var untilCode:String=StringUtil.substitute("while(!({0}));\n",initCode.type=="obj"?initCode.code.code:initCode.code);
			return (untilCode);
		}
		private function parseDoUntil(blk:Object):String{
			var initCode:CodeBlock = getCodeBlock(blk[1]);
			var untilCode:String=StringUtil.substitute("while(!({0}))\n{\n",initCode.type=="obj"?initCode.code.code:initCode.code);
			if(blk[2]!=null){
				for(var i:int=0;i<blk[2].length;i++){
					var b:Object = blk[2][i]
					var cBlk:CodeBlock=getCodeBlock(b);
					untilCode+=cBlk.type=="obj"?cBlk.code.code:cBlk.code;
				}
			}
			untilCode+="}\n";
			return (untilCode);
		}
		private function parseCall(blk:Object):String{
			
			var vars:String = "";
			var funcName:String = blk[1];
			if(funcName.indexOf("%")==0){
				funcName = "func "+funcName;
			}
			var ps:Array = funcName.split(" ");
			var tmp:Array = [castVarName(ps[0], true)];
			for(var i:uint=0;i<ps.length;i++){
				if(i>0){
					if(ps[i].indexOf("%")>-1){
						tmp.push(ps[i].substr(1,1));
					}
				}
			}
			ps = tmp;
			var params:Array = blk as Array;
			var cBlk:CodeBlock;
			for(i = 2;i<params.length;i++){
				cBlk = getCodeBlock(params[i]);
				//				trace("p:",params[i],cBlk.type,"end");
				if(i>2){
					vars +=",";
				}
				if(cBlk.type=="obj"){
					vars += cBlk.code.code;//(isNaN(Number(params[i]))?'"'+params[i]+'"':(params[i]==""?(ps[i-1]=="s"?'"s"':"false"):params[i]))+(i<params.length-1?", ":"");
				}else if(cBlk.type == "string"){
					vars += '"' + cBlk.code + '"';
				}else{
					vars += cBlk.code;
					
				}
			}
			var callCode:String = StringUtil.substitute("{0}({1});\n",ps[0],vars);
			return (callCode);
		}
		private function addFunction(blks:Array):void{
			var funcName:String = blks[0][1].split("&").join("_");
			for each(var o:Object in funcList){ 
				if(o.name==funcName){
					return;
				}
			}
			if(funcName.indexOf("%")==0){
				funcName = "func "+funcName;
			}
			var params:Array = funcName.split(" ");
			var tmp:Array = [params[0]];
			for(var i:uint=0;i<params.length;i++){
				if(i>0){
					if(params[i].indexOf("%")>-1){
						tmp.push(params[i].substr(1,1));
					}
				}
			}
			params = tmp;
			var vars:String = "";
			for(i = 1;i<params.length;i++){
				vars += (params[i]=='n'?("double"):(params[i]=='s'?"String":(params[i]=='b'?"boolean":"")))+" "+castVarName(blks[0][2][i-1].split(" ").join("_"))+(i<params.length-1?", ":"");
			}
			var defFunc:String = "void "+castVarName(params[0], true)+"("+vars+");\n";
			if(ccode_def.indexOf(defFunc)==-1){
				ccode_def+=defFunc;
			}
			var funcCode:String = "void "+castVarName(params[0], true)+"("+vars+")\n{\n";
			for(i=0;i<blks.length;i++){
				if(i>0){
					
					var b:CodeBlock = getCodeBlock(blks[i],blks[0][2]);
					var code:String = (b.type=="obj"?b.code.code:b.code);
					funcCode+=code+"\n";
				}
			}
			funcCode+="}\n";
			funcList.push({name:funcName,code:funcCode});
		}
		private function parseIfElse(blk:Object):String{
			var codeIfElse:String = ""
			var logiccode:CodeBlock = getCodeBlock(blk[1]);
			codeIfElse+=StringUtil.substitute("if({0}){\n",logiccode.type=="obj"?logiccode.code.code:logiccode.code);
			if(blk[2]!=null){
				for(var i:int=0;i<blk[2].length;i++){
					var b:CodeBlock = getCodeBlock(blk[2][i]);
					var ifcode:String=(b.type=="obj"?b.code.code:b.code)+""
					codeIfElse+=ifcode
				}
			}
			codeIfElse+="}else{\n";
			if(blk[3]!=null){
				for(i=0;i<blk[3].length;i++){
					b = getCodeBlock(blk[3][i]);
					var elsecode:String=(b.type=="obj"?b.code.code:b.code)+"";
					codeIfElse+=elsecode;
				}
			}
			codeIfElse+="}\n"
			return codeIfElse
		}
		
		private function parseIf(blk:Object):String{
			var codeIf:String = ""
			var logiccode:String = getCodeBlock(blk[1]).code;
			codeIf+=StringUtil.substitute("if({0}){\n",logiccode)
			if(blk is Array){
				if(blk.length>2){
					if(blk[2]!=null){
						for(var i:int=0;i<blk[2].length;i++){
							var b:CodeBlock = getCodeBlock(blk[2][i]);
							var ifcode:String=(b.type=="obj"?b.code.code:b.code)+"";
							codeIf+=ifcode;
						}
					}
				}
			}
			codeIf+="}\n"
			return codeIf
		}
		
		private function parseVarWrite(blk:Object):String{
			var varName:String = blk[2]
			if (varList.indexOf(varName)==-1){
				varList.push(varName)
			}
			if (varListWrite.indexOf(varName)==-1){
				varListWrite.push(varName)
			}	
			return ""
		}
		private function parseComputeFunction(blk:Object):String{
			var cBlk:CodeBlock = getCodeBlock(blk[2]);
			if(blk[1]=="10 ^"){
				return StringUtil.substitute("pow(10,{0})",cBlk.code);
			}else if(blk[1]=="e ^"){
				return StringUtil.substitute("exp({0})",cBlk.code);
			}else if(blk[1]=="ceiling"){
				return StringUtil.substitute("ceil({0})",cBlk.code);
			}else if(blk[1]=="log"){
				return StringUtil.substitute("log10({0})",cBlk.code);
			}else if(blk[1]=="ln"){
				return StringUtil.substitute("log({0})",cBlk.code);
			}
			
			return StringUtil.substitute("{0}({1})",getCodeBlock(blk[1]).code,cBlk.code).split("sin(").join("sin(angle_rad*").split("cos(").join("cos(angle_rad*").split("tan(").join("tan(angle_rad*");
		}
		private function buildCode(modtype:String,iotype:String,modport:*,modslot:*,valuestring:*):Object{
			var workcode:String = ""
			var setupcode:String = ""
			var defcode:String = ""    
			var inccode:String = ""
			var loopcode:String = ""
			var portcode:String = modport is CodeObj?modport.code:modport;
			var slotcode:String = modslot is CodeObj?modslot.code:modslot;
			var valuecode:String = valuestring is CodeObj?valuestring.code:valuestring;
			if(modtype=="available"){
				if(iotype=="serial"){
					setupcode = StringUtil.substitute("Serial.begin(115200);\n")
					workcode=StringUtil.substitute("dataLineAvailable()");
				}
			}else if(modtype=="read"){
				if(iotype=="serial"){
					setupcode = StringUtil.substitute("Serial.begin(115200);\n")
					workcode=StringUtil.substitute("readDataLine()");
				}
			}else if(modtype=="write"){
				if(iotype=="serial"){
					setupcode = StringUtil.substitute("Serial.begin(115200);\n");
					if(modslot=="command"){
						workcode = StringUtil.substitute("Serial.print(\"{0}=\");Serial.println({1});\n",portcode,valuecode);
					}else if(modslot=="update"){
						workcode = StringUtil.substitute("updateVar(\"{0}\",&{1});\n",portcode,portcode);
					}else{
						workcode=StringUtil.substitute("Serial.println("+(modport is CodeObj?"{0}":"\"{0}\"")+");\n",portcode);
					}
				}
			}else if(modtype=="clear"){
				
			}else{
				hasUnknownCode = true;
				trace("Unknow Module:"+modtype)
			}
			var codeObj:Object = {setup:setupcode,work:workcode,def:defcode,inc:inccode,loop:loopcode};		
			return codeObj;
		}
		
		private function buildModule(mname:String,mport:*,mslot:*,mtype:String,mindex:int,mvalue:*):Object{
			var modDict:Object = {name:mname,port:mport,slot:mslot,type:mtype,index:mindex,value:mvalue}
			modDict.code = buildCode(mname,mtype,mport,mslot,mvalue)
			return modDict;
		}
		
		private function getModule(mod:Object):Object{
			var mdescript:Array = mod[0].split('/')
			var mtype:String = mdescript[0].split('.')[1]
			var mname:String = mdescript[1]
			var mport:CodeBlock = getCodeBlock(mod[1]);
			var mslot:CodeBlock = new CodeBlock();
			var mvalue:CodeBlock = new CodeBlock();
			if(mod.length<=3){
				if(mtype=="run"){
					mvalue = getCodeBlock(mod[mod.length-1]);
				}
				if(mtype=="get"){
					mslot = getCodeBlock(mod[2]);
				}
				if(mtype=="serial"){
					mvalue = getCodeBlock(mod[mod.length-1])
					mslot.code = mdescript[2]
				}
			}else if(mod.length==4){
				mslot = getCodeBlock(mod[2]);
				mvalue = getCodeBlock(mod[3]);
			}else if(mod.length==6){
				if(getQualifiedClassName(mod[3]) == "Array"){
					mod[3] = getCodeBlock(mod[3]).code;
				}			
				if(getQualifiedClassName(mod[4]) == "Array"){
					mod[4] = getCodeBlock(mod[4]).code;
				}
				if(getQualifiedClassName(mod[5]) == "Array"){
					mod[5] = getCodeBlock(mod[5]).code; 
				}
				mvalue.code = StringUtil.substitute("{0},{1},{2},{3}",mod[2],mod[3],mod[4],mod[5])
			}
			for(var i:int = 0;i<moduleList.length;i++){
				var m:Object = moduleList[i]
				if(m.name==mname && m.port==mport.code){
					if(getQualifiedClassName(mvalue.code) == "Array"){
						mvalue = getCodeBlock(mvalue.code)
					}
					m.code=buildCode(mname,mtype,mport.code,mslot.code,mvalue.code) //update work function
					return m
				}
			}
			if(getQualifiedClassName(mvalue.code) == "Array"){
				mvalue = getCodeBlock(mvalue.code)
			}
			moduleList.push(buildModule(mname,mport,mslot,mtype,moduleList.length,mvalue))
			return moduleList[moduleList.length-1]
		}
		
		private function appendFun(funcode:*):void{
			//			if (c!="\n" && c!="}")
			//funcode+=";\n"
			var allowAdd:Boolean = funcode is CodeObj;
			funcode = funcode is CodeObj?funcode.code:funcode;
			
			if(funcode==null) return;
			if(funcode.length==0) return;
			var c:String =  funcode.charAt(funcode.length-1)
			if(ccode_pointer=="setup"){
				if((ccode_setup.indexOf(funcode)==-1&&ccode_setup_fun.indexOf(funcode)==-1)||funcode.indexOf("delay")>-1||allowAdd){
//					if((funcode.indexOf(" = ")>-1&&funcode.indexOf("drawTemp")==-1&&funcode.indexOf("lastTime = ")==-1)&&funcode.indexOf("while")==-1&&funcode.indexOf("for")==-1){
//						ccode_setup_def = funcode + ccode_setup_def;
//					}else{
						ccode_setup_fun += funcode;
//					}
				}
			}
			else if(ccode_pointer=="loop"){
				ccode_loop+=funcode;
			}
		}
		
		private function getCodeBlock(blk:Object,params:Array=null):CodeBlock{
			var code:CodeObj;
			var codeBlock:CodeBlock = new CodeBlock;
			if(blk==null||blk==""){
				codeBlock.type = "number";
				codeBlock.code = "0";
				return codeBlock;
			}
			if(!(blk is Array)){
				codeBlock.code = ""+blk;
				codeBlock.type = isNaN(Number(blk))?"string":"number";
				return codeBlock;
			}
			if(blk.length==0){
				codeBlock.type = "string";
				codeBlock.code = "";
				return codeBlock;
			}else if(blk.length==16){
				codeBlock.type = "array";
				codeBlock.code = blk;
				return codeBlock;
			}
			if(mathOp.indexOf(blk[0])>=0){
				codeBlock.type = "obj";
				codeBlock.code = parseMath(blk);
				return codeBlock;
			}
			else if(blk[0]=="readVariable"){
				codeBlock.type = "obj";
				codeBlock.code = parseVarRead(blk);
				return codeBlock;
			}
			else if(blk[0]=="setVar:to:"){
				codeBlock.type = "string";
				codeBlock.code = parseVarSet(blk);
				return codeBlock;
			}
			else if(blk[0]=="readVariable:")
				code = parseVarShow(blk);
			else if(blk[0]=="wait:elapsed:from:"){
				codeBlock.type = "string";
				codeBlock.code = parseDelay(blk);
				return codeBlock;
			}
			else if(blk[0]=="doIfElse"){
				codeBlock.type = "string";
				codeBlock.code = parseIfElse(blk);
				return codeBlock;
			}
			else if(blk[0]=="doIf"){
				codeBlock.type = "string";
				codeBlock.code = parseIf(blk);
				return codeBlock;
			}
			else if(blk[0]=="writeVariable:")
			{
				codeBlock.type = "string";
				codeBlock.code = parseVarWrite(blk);
				return codeBlock;
			}
			else if(blk[0]=="doRepeat"){
				codeBlock.type = "string";
				codeBlock.code = parseDoRepeat(blk);
				return codeBlock;
			}else if(blk[0]=="doWaitUntil"){
				codeBlock.type = "string";
				codeBlock.code = parseDoWaitUntil(blk);
				return codeBlock;
			}else if(blk[0]=="doUntil"){
				codeBlock.type = "string";
				codeBlock.code = parseDoUntil(blk);
				return codeBlock;
			}else if(blk[0]=="call"){
				codeBlock.type = "string";
				codeBlock.code = parseCall(blk);
				return codeBlock;
			}else if(blk[0]=="randomFrom:to:"){
				codeBlock.type = "number";
				codeBlock.code = StringUtil.substitute("random({0},{1})",getCodeBlock(blk[1]).code,getCodeBlock(blk[2]).code);
				return codeBlock;
			}else if(blk[0]=="computeFunction:of:"){
				codeBlock.type = "number";
				codeBlock.code = parseComputeFunction(blk);
				return codeBlock;
			}else if(blk[0]=="concatenate:with:"){
				var s1:CodeBlock = getCodeBlock(blk[1]);
				var s2:CodeBlock = getCodeBlock(blk[2]);
				codeBlock.type = "obj";
				codeBlock.code = new CodeObj(StringUtil.substitute("{0}+{1}",(s1.type=="obj")?s1.code.code:"String(\""+s1.code+"\")",(s2.type=="obj")?s2.code.code:"String(\""+s2.code+"\")"));
				return codeBlock;
			}else if(blk[0]=="letter:of:"){
				s2 = getCodeBlock(blk[2]);
				codeBlock.type = "obj";
				codeBlock.code = new CodeObj(StringUtil.substitute("{1}.charAt({0}-1)",getCodeBlock(blk[1]).code,(s2.type=="obj")?"String("+s2.code.code+")":"String(\""+s2.code+"\")"));
				return codeBlock;
			}else if(blk[0]=="castDigitToString:"){
				codeBlock.type = "obj";
				codeBlock.code = new CodeObj(StringUtil.substitute('String({0})',getCodeBlock(blk[1]).code));
				return codeBlock;
			}else if(blk[0]=="stringLength:"){
				s1 = getCodeBlock(blk[1]);
				codeBlock.type = "obj";
				codeBlock.code = new CodeObj(StringUtil.substitute("String({0}).length()",(s1.type != "obj")?"\""+s1.code+"\"":s1.code.code));
				return codeBlock;
			}else if(blk[0]=="changeVar:by:"){
				codeBlock.type = "string";
				codeBlock.code = StringUtil.substitute("{0} += {1};\n",getCodeBlock(castVarName(blk[1])).code,getCodeBlock(blk[2]).code);
				return codeBlock;
			}
				//			else if(blk[0].indexOf("Makeblock")>=0||blk[0].indexOf("Arduino")>=0||blk[0].indexOf("Communication")>=0){
				//				code = new CodeObj(getModule(blk)["code"]["work"]);
				//			}
			else{
				var objs:Array = MBlock.app.extensionManager.specForCmd(blk[0]);
				if(objs!=null){
					var obj:Object = objs[objs.length-1];
					obj = obj[obj.length-1];
					if(typeof obj == "object"){
						var ext:ScratchExtension = MBlock.app.extensionManager.extensionByName(blk[0].split(".")[0]);
						var codeObj:Object = {code:{setup:substitute(obj.setup,blk as Array,ext),work:substitute(obj.work,blk as Array,ext),def:substitute(obj.def,blk as Array,ext),inc:substitute(obj.inc,blk as Array,ext),loop:substitute(obj.loop,blk as Array,ext)}};	
						if(!availableBlock(codeObj)){
							if(ext!=null){
								if(srcDocuments.indexOf(ext.srcPath)==-1){
									srcDocuments.push(ext.srcPath);
								}
							}
							moduleList.push(codeObj);
						}
						codeBlock.type = "obj";
						codeBlock.code = new CodeObj(codeObj.code.work);
						return codeBlock;
					}
				}
				var b:Block = BlockIO.arrayToStack([blk]);
				if(b.op=="getParam"){
					codeBlock.type = "number";
					codeBlock.code = castVarName(b.spec.split(" ").join("_"));
					return codeBlock;
				}
				if(b.op=="procDef"){
					return codeBlock;
				}
				unknownBlocks.push(b);
				hasUnknownCode = true;
				codeBlock.type = "string";
				codeBlock.code = StringUtil.substitute("//unknow {0}{1}",blk[0],b.type=='r'?"":"\n");
				return codeBlock;
			}
			codeBlock.type = "obj";
			codeBlock.code = code;
			return codeBlock;
		}
		private function substitute(str:String,params:Array,ext:ScratchExtension=null,offset:uint = 1):String{
			for(var i:uint=0;i<params.length-offset;i++){
				var o:CodeBlock = getCodeBlock(params[i+offset]);
				
				var v:*=o.type=="string"?(ext.values[o.code]==undefined?o.code:ext.values[o.code]):null;
//				if(str.indexOf("sendString")>-1){
//					v = o.code;
//				}
				var s:CodeBlock = new CodeBlock();
				if(ext==null||(v==null||v==undefined)){
					
					s = getCodeBlock(params[i+offset]);
					s.type = (s.type=="obj"&&s.code.type!="code")?"string":"number";
					
				}else{
					s.type = isNaN(Number(v))?"string":"number";
					s.code = v;
				}
				if((s.code==""||s.code==" ")&&s.code!=0&&s.type == "number"){
					s.type = "string";
				}
				if(str.indexOf(".drawStr(")>-1){
					if(i==3 && s.type == "number" && s.code is String){
						s.type = "string";
					}
				}else if(str.indexOf("ir.sendString(") == 0){
					if(s.type == "number" && s.code is String){
						s.type = "string";
					}
				}
				str = str.split("{"+i+"}").join(( s.type == "string")?('"'+s.code+'"'):(( s.type == "number")?s.code:s.code.code));
			}
			return str;
		}
		private function availableBlock(obj:Object):Boolean{
			for each(var o:Object in moduleList){
				if(o.code.def==obj.code.def&&o.code.setup==obj.code.setup){
					return true;
				}
			}
			return false;
		}
		private function parseLoop(blks:Object):void{
			ccode_pointer="loop";
			if(blks!=null){
				for(var i:int;i<blks.length;i++){
					var b:Object = blks[i]
					var cBlk:CodeBlock = getCodeBlock(b);
					appendFun(cBlk.code);
				}
			}
		}
		private function parseModules(blks:Object):void{
			var isArduinoCode:Boolean = false;
			for(var i:int;i<blks.length;i++){
				var b:Object = blks[i];
				var objs:Array = MBlock.app.extensionManager.specForCmd(blks[0]);
				if(objs!=null){
					var obj:Object = objs[objs.length-1];
					obj = obj[obj.length-1];
					if(typeof obj == "object"&&obj!=null){
						var codeObj:Object = {code:{setup:obj.setup,work:obj.work,def:obj.def,inc:obj.inc,loop:obj.loop}};	
						moduleList.push(codeObj);
					}
				}
			}
		}
		private function parseCodeBlocks(blks:Object):Boolean{
			var isArduinoCode:Boolean = false;
			for(var i:int;i<blks.length;i++){
				var b:Object = blks[i];
				if(b[0].indexOf("runArduino")>-1){
					ccode_pointer="setup";
					isArduinoCode = true;
					
					var objs:Array = MBlock.app.extensionManager.specForCmd(blks[0]);
					var n:String = blks[0];
					var ext:ScratchExtension = MBlock.app.extensionManager.extensionByName(n.split(".")[0]);
					if(ext!=null){
						if(srcDocuments.indexOf(ext.srcPath)==-1){
							srcDocuments.push(ext.srcPath);
						}
					}
					if(objs!=null){
						var obj:Object = objs[objs.length-1];
						obj = obj[obj.length-1];
						if(typeof obj == "object"&&obj!=null){
							var codeObj:Object = {code:{setup:obj.setup,work:obj.work,def:obj.def,inc:obj.inc,loop:obj.loop}};	
							moduleList.push(codeObj);
						}
					}
				}else if(b[0].indexOf("runMotor")>-1) {
					ccode_pointer="setup";
					isArduinoCode = true;
					
					var objs:Array = MBlock.app.extensionManager.specForCmd(blks[0]);
					var n:String = blks[0];
					var ext:ScratchExtension = MBlock.app.extensionManager.extensionByName(n.split(".")[0]);
					if(ext!=null){
						if(srcDocuments.indexOf(ext.srcPath)==-1){
							srcDocuments.push(ext.srcPath);
						}
					}
					if(objs!=null){
						var obj:Object = objs[objs.length-1];
						obj = obj[obj.length-1];
						if(typeof obj == "object"&&obj!=null){
							var codeObj:Object = {code:{setup:obj.setup,work:obj.work,def:obj.def,inc:obj.inc,loop:obj.loop}};	
							moduleList.push(codeObj);
						}
					}
					}else if(b[0]=="doForever"){			
					ccode_pointer="loop";
					parseLoop(b[1]);
				}else{
					var cBlk:CodeBlock = getCodeBlock(b);
					appendFun(cBlk.code);
				}
			}
			return isArduinoCode;
		}
		
		private function buildSerialParser(code:String):String{
			if(varListWrite.length==0){
				code = code.replace("//serialParserCall","").replace("//serialParser","")
				return code;
			}
			var codeParser:String=""
			for(var i:int=0;i<varListWrite.length;i++){
				var p:String = varListWrite[i]
				codeParser+=codeSerialScanf.replace("param", p)
			}			
			codeParser = codeSerialParser.replace("//parseList", codeParser)
			code = code.replace("//serialParserCall","parseSerialInput();").replace("//serialParser",codeParser);
			return code;
		}
		
		private function fixTabs(code:String):String{
			var tmp:String = "";
			var tabindex:int=0
			var newLineList:Array = []
			var lines:Array = code.split('\n')
			for(var i:int=0;i<lines.length;i++){
				var l:String = lines[i]
				if(l.indexOf("}")>=0)
					tabindex-=1
				tmp = ""
				for(var j:int=0;j<tabindex;j++)
					tmp+="    "
				newLineList.push(tmp+l)
				if(l.indexOf("{")>=0)
					tabindex+=1
			}
			code = newLineList.join("\n")
			code = code.replace(new RegExp("\r\n", "gi"),"\n") // replace windows type end line
			return code;
		}
		private function fixVars(code:String):String{
			for each(var s:String in varStringList){
				code = code.split("double " +s).join("String "+s);
			}
			return code;
		}
		private var requiredCpp:Array=[];
		
		public function jsonToBASH(code:String):String{
			// reset code buffers 
			var retcode:String
			ccode_setup=""
			ccode_setup_fun = "";
			ccode_setup_def = "";
			ccode_loop=""
			ccode_inc=""
			ccode_def=""
			ccode_func="";
			hasUnknownCode = false;
			//malcolm
			motionCode = "";
			controlCode = "";
			loopCode ="";
			//
			// reset arrays
			varList=[];
			varStringList=[];
			varListWrite=[]
			paramList=[]
			moduleList=[]
			funcList = [];
			unknownBlocks = [];
			// params for compiler
			requiredCpp=[];
			var buildSuccess:Boolean = false;
			var objs:Object = util.JSON.parse(code);
			var childs:Array = objs.children.reverse();
			for(var i:int=0;i<childs.length;i++){
				buildSuccess = parseScripts(childs[i].scripts);			
			}
			if(!buildSuccess){
				parseScripts(objs.scripts);
			}
			
			ccode_func+=buildFunctions();
			retcode = codeTemplateBASH.replace("//setup","").replace("//loop", loopCode).replace("//define", controlCode).replace("//include", motionCode).replace("//function",ccode_func);
			retcode = add_comma(retcode);
//			retcode = add_comma2(retcode);
			retcode = buildSerialParser(retcode);
			retcode = fixTabs(retcode);
			retcode = fixVars(retcode);
			//malcolm add in (String to file)
			//var byte:ByteArray = new ByteArray();
			//var fileref:FileReference = new FileReference();
			//fileref.save(retcode,"testfile.txt")
			//
			requiredCpp = getRequiredCpp()
			// now go into compile process
			if(!NativeProcess.isSupported) return "";
			return (retcode);
			//			buildAll(retcode, requiredCpp);
		}
		
				//13. Malcolm JSON to LOG

		public function jsonToLOG(code:String):String{
					// reset code buffers 
			var retcode:String
			logCode = "";
					// reset arrays
					// params for compiler
			var buildSuccess:Boolean = false;
			var objs:Object = util.JSON.parse(code);
			var childs:Array = objs.children.reverse();
			for(var i:int=0;i<childs.length;i++){
					buildSuccess = parseScriptsLOG(childs[i].scripts);
				}
			if(!buildSuccess){
					parseScriptsLOG(objs.scripts);
				}
			
			ccode_func+=buildFunctions();
			retcode = codeTemplateLOG.replace("//movement",logCode);
			retcode = buildSerialParser(retcode);
			retcode = fixTabs(retcode);
			retcode = fixVars(retcode);
			requiredCpp = getRequiredCpp()
				// now go into compile process
				if(!NativeProcess.isSupported) return "";
			return (retcode);
			//			buildAll(retcode, requiredCpp);
		}
		//
		
		
		
	/*	public function jsonToCpp(code:String):String{
			// reset code buffers 
			var retcode:String
			ccode_setup=""
			ccode_setup_fun = "";
			ccode_setup_def = "";
			ccode_loop=""
			ccode_inc=""
			ccode_def=""
			ccode_func="";
			hasUnknownCode = false;
			// reset arrays
			varList=[];
			varStringList=[];
			varListWrite=[]
			paramList=[]
			moduleList=[]
			funcList = [];
			unknownBlocks = [];
			// params for compiler
			requiredCpp=[];
			var buildSuccess:Boolean = false;
			var objs:Object = util.JSON.parse(code);
			var childs:Array = objs.children.reverse();
			for(var i:int=0;i<childs.length;i++){
				buildSuccess = parseScripts(childs[i].scripts);
			}
			if(!buildSuccess){
				parseScripts(objs.scripts);
			}
			ccode_func+=buildFunctions();
			retcode = codeTemplate.replace("//setup",ccode_setup).replace("//loop", ccode_loop).replace("//define", ccode_def).replace("//include", ccode_inc).replace("//function",ccode_func);
			retcode = buildSerialParser(retcode);
			retcode = fixTabs(retcode);
			retcode = fixVars(retcode);
			requiredCpp = getRequiredCpp()
			// now go into compile process
			if(!NativeProcess.isSupported) return "";
			return (retcode);
			//			buildAll(retcode, requiredCpp);
		} */
		
		private function parseScripts(scripts:Object):Boolean
		{
			if(null == scripts){
				return false;
			}
			var result:Boolean = false;
			for(var j:uint=0;j<scripts.length;j++){
				var scr:Object = scripts[j][2];
				if(scr[0][0].indexOf("runArduino")==-1){
					if(scr[0][0]=="procDef"){
						addFunction(scr as Array);
						parseModules(scr);
						buildCodes(scr);
					}
					continue;
				}//选中的Arduino主代码
				
				if(!parseCodeBlocks(scr)){
					continue;
				}
				buildCodes(scr);
				if(_scratch!=null){
					_scratch.dispatchEvent(new RobotEvent(RobotEvent.CCODE_GOT,""));
				}
				result = true;
				//break; // only the first entrance is parsed
			}
			return result;
		}
		
		public static var movement_bk: Boolean = new Boolean; 
		//13. malcolm paRSE SCRIPTS LOG
		private function parseScriptsLOG(scripts:Object):Boolean
		{
				if(null == scripts){
						return false;
					}
				var result:Boolean = false;
				// 13. Malcolm LOG Block Code Testing
					for(var j:uint=0;j<scripts.length;j++){
							var scr:Object = scripts[j][2];
							if(scr[0][0].indexOf("runMotor") > -1){
										buildLOG(scr);	
										result = true;
										movement_bk = true;
								}
							else movement_bk = false;
							//Steven
							//break; // only the first entrance is parsed
							}
				return result;
			}
		
	/*private function buildCodes():void{
			buildInclude();			
			buildDefine();
			buildSetup();
			ccode_setup+=ccode_setup_def;
			//buildSetup();
			ccode_setup+=ccode_setup_fun;
			ccode_setup_fun = "";
			ccode_loop+=buildLoopMaintance();
		}
		*/
		
		//Malcolm start
		private function buildCodes(blk:Object):void{
			buildInclude();			
			buildDefine();
			buildSetup();
			motionCode = buildSHELLCode(blk);
			
			//change {v v v} to [ \s ]
			motionCode  = motionCode.split("{v").join("[");
			motionCode  = motionCode.split("v\n").join(" \n");
			motionCode  = motionCode.split("v}").join("]");
//			motionCode = add_comma(motionCode);
			ccode_setup+=ccode_setup_def;
			//buildSetup();
			ccode_setup+=ccode_setup_fun;
			ccode_setup_fun = "";
			ccode_loop+=buildLoopMaintance();
			
			if(file_name_FLAG) file_name_FLAG_result = true;
			else file_name_FLAG_result = false;
			file_name_FLAG = false;
			
			if(file_name_FLAG_result)
				trace("Cap Flag True");
		}
		
		private function buildLOG(blk:Object):void{
				logCode = "#\n#\n#\n#\n#\n#\n#\n" + buildMovementLOG(blk);
		}
		
		//This is used for cet file name generation.
		public static var filename_voice: String;
		public static var filename_language: String;
		
		//Malcolm
		//Steven, modification for 
		public static var Bool_POS: Boolean = false;
		public static var Bool_TOUCH: Boolean = false;
		public static var Bool_VOICE: Boolean = false;
		
		public static var Bool_POS_stand: Boolean = false;
		public static var Bool_POS_lying: Boolean = false;
		public static var Bool_POS_other: Boolean = false;
				
		public static var Bool_TOUCH_1: Boolean = false;
		public static var Bool_TOUCH_2: Boolean = false;
		public static var Bool_TOUCH_3: Boolean = false;
		public static var Bool_TOUCH_long: Boolean = false;
				
		public var Bool_VOICE_1: Boolean = false;
		public var Bool_VOICE_2: Boolean = false;
		
		public static var Bool_Current_BLK_ONLY_CELL: Boolean = false;
		
		private function buildSHELLCode(blk:Object):String{
			var Codes:String = "";
			Bool_POS= false;
			Bool_TOUCH = false;
			Bool_VOICE = false;
			
			Bool_POS_stand = false;
			Bool_POS_lying = false;
			Bool_POS_other = false;
			
			Bool_TOUCH_1 = false;
			Bool_TOUCH_2 = false;
			Bool_TOUCH_3 = false;
			Bool_TOUCH_long = false;
			
			Bool_VOICE_1 = false;
			Bool_VOICE_2 = false;
					
			Bool_Current_BLK_ONLY_CELL = false;
			
			function update_Bool_POS(pos: Boolean): Boolean{
				Bool_POS = Bool_POS_stand || Bool_POS_lying || Bool_POS_other;
			}
			
			function update_Bool_TOUCH(touch: Boolean): Boolean{
				Bool_TOUCH = Bool_TOUCH_1 || Bool_TOUCH_2 || Bool_TOUCH_3 ||Bool_TOUCH_long;
			}
			
			function update_Bool_VOICE(voice: Boolean): Boolean{
				Bool_VOICE = Bool_VOICE_1 || Bool_VOICE_2;
			}
					
			
			
			
			for (var i:int = 0; i < blk.length; i++)
			{	
				if (blk[i][0].indexOf("GTmotion") > -1)
				{
					Codes +=  buildMotion(blk[i]);
				}
				else if (blk[i][0].indexOf("cell_json.SHELL") > -1)
				{
					Codes += buildBashControl(blk[i]);
				}
				else if (blk[i][0].indexOf("voice_json.SHELL") > -1)
				{
					update_Bool_VOICE(Bool_VOICE_1=true);
//					Codes += buildBashControl(blk[i]);
				Codes = combine_touch(Codes, buildExtendedBlockControl(blk[i]), "voice\":", "{v", "v},");//test for combine voice
				
				}
				else if (blk[i][0].indexOf("voice_json_ifelse.SHELL") > -1)
				{
					update_Bool_VOICE(Bool_VOICE_2=true);
//					Codes += buildBashControl(blk[i]);
					Codes = combine_touch(Codes, buildExtendedBlockControl(blk[i]), "voice\":", "{v", "v},");//test for combine voice
				}
				else if (blk[i][0].indexOf("LED") > -1)
				{
					Codes += buildLED(blk[i]);
				}
				else if ((blk[i][0].indexOf(".sound") > -1)||(blk[i][0].indexOf("playSound:") > -1))
				{
					Codes += buildSound(blk[i]);
				}
				else if (blk[i][0].indexOf("Sense") > -1)
				{
					Codes += buildSense(blk[i]);
				}
				else if (blk[i][0].indexOf(".log") > -1)
				{
					Codes += buildCustomMovement(blk[i]);
				}
				else if (blk[i][0].indexOf("runArduino") > -1)
				{
					trace("runArduino", blk[i]);
					file_name_FLAG = true;
					//var name used as file name.
					filename_voice = blk[i][1];
					//Solution to solve the default "Hello" problem when language is changed from English to Chinese. If following code is not done, even in Chinese version the "voice command" name is in Engilish
					if((filename_voice=="Hello")&&(MBlock.app.lang_flag == 'zh_CN'))
						filename_voice = "你好";
					
					//language selection
					if(blk[i][2]=="English")
						filename_language = "en";
					if(blk[i][2]=="简体中文")
						filename_language = "zh";
					else
						filename_language = "en";//problem!!
			
//					filename_voice = filename_voice.replace("Arduino.runArduino,", "");
					
				}
				else if (blk[i][0].indexOf("wheel") > -1)
				{
					Codes += buildWheels(blk[i]);
				}
				else if (blk[i][0].indexOf("1_touch_json.touch") > -1)
				{
					update_Bool_TOUCH(Bool_TOUCH_1 = true);
//					Codes += buildTouch(blk[i]);
					Codes = combine_touch(Codes, buildTouch(blk[i]), "touch\":", "{", "},");
				}
				else if (blk[i][0].indexOf("2_touch_json.touch") > -1)
				{
					update_Bool_TOUCH(Bool_TOUCH_2 = true);
					//					Codes += buildTouch(blk[i]);
					Codes = combine_touch(Codes, buildTouch(blk[i]), "touch\":", "{", "},");
				}
				else if (blk[i][0].indexOf("3_touch_json.touch") > -1)
				{
					update_Bool_TOUCH(Bool_TOUCH_3 = true);
					//					Codes += buildTouch(blk[i]);
					Codes = combine_touch(Codes, buildTouch(blk[i]), "touch\":", "{", "},");
				}
				else if (blk[i][0].indexOf("long_touch_json.touch") > -1)
				{
					update_Bool_TOUCH(Bool_TOUCH_long = true);
					//					Codes += buildTouch(blk[i]);
					Codes = combine_touch(Codes, buildTouch(blk[i]), "touch\":", "{", "},");
				}
				else if (blk[i][0].indexOf("pos_standing_json.posture") > -1)
				{
					update_Bool_POS(Bool_POS_stand = true);
//					Codes += buildPosture(blk[i]);
					Codes = combine_touch(Codes, buildPosture(blk[i]), "posture\":", "{", "},");
				}
				else if (blk[i][0].indexOf("pos_lying_json.posture") > -1)
				{
					update_Bool_POS(Bool_POS_lying = true);
					//					Codes += buildPosture(blk[i]);
					Codes = combine_touch(Codes, buildPosture(blk[i]), "posture\":", "{", "},");
				}
				else if (blk[i][0].indexOf("pos_other_json.posture") > -1)
				{
					update_Bool_POS(Bool_POS_other = true);
					//					Codes += buildPosture(blk[i]);
					Codes = combine_touch(Codes, buildPosture(blk[i]), "posture\":", "{", "},");
				}
				
				if (blk[i][0].indexOf("cell_json.SHELL") > -1)
				{
					Bool_Current_BLK_ONLY_CELL = true;
				}
				else 
					Bool_Current_BLK_ONLY_CELL = false;
				
				
			}
			
			
			//Automatically add acbefore, acafter when there is voice block.

			Codes = auto_acbefore(Codes, "voice");
//			Codes = auto_acbefore(Codes, "touch");
			trace("!!!!!!!!!!!!!!!!!auto_acafter raw codes", Codes);
			Codes = auto_acafter(Codes, "voice");
//			Codes = auto_acafter(Codes, "touch");
			
			return Codes;
		}
		//
//		var Codes:String = buildSHELLCode_withinCF(blk);	
		private function buildSHELLCode_withinCF(blk:Object):String{
			var Codes:String = "";
			for (var i:int = 0; i < blk.length; i++)
			{	
				if (blk[i][0].indexOf("GTmotion") > -1)
				{
//					Codes +=  buildMotion(blk[i]);
					Codes = combine_current(Codes, buildMotion(blk[i]), "leg", "[", "]");
				}
				if(blk[i][0].indexOf("GTMovement") > -1)
				{
					//					Codes +=  buildMotion(blk[i]);
					Codes = combine_current(Codes, buildCustomMovement(blk[i]), "leg", "[", "]");
				}
				else if (blk[i][0].indexOf("SHELL") > -1)
				{
					Codes += buildBashControl(blk[i]);
					
				}
				else if (blk[i][0].indexOf("LED") > -1)
				{
//					Codes += buildLED(blk[i]);
					Codes = combine_current(Codes, buildLED(blk[i]), "led", "[", "]");
				}
				else if (blk[i][0].indexOf(".sound") > -1)
				{
//					Codes += buildSound(blk[i]);
					Codes = combine_current(Codes, buildSound(blk[i]), "answer", "[", "]");
				}
				else if (blk[i][0].indexOf("playSound:") > -1)
				{
					//					Codes += buildSound(blk[i]);
					Codes = combine_current(Codes, buildSound(blk[i]), "music", "[", "]");
				}
				else if (blk[i][0].indexOf("Sense") > -1)
				{
					Codes += buildSense(blk[i]);
				}
//				else if (blk[i][0].indexOf(".log") > -1)
//				{
//					Codes += buildCustomMovement(blk[i]);
//				}//Steven Movement
				else if (blk[i][0].indexOf("wheel") > -1)
				{
//					Codes += buildWheels(blk[i]);
					Codes = combine_current(Codes, buildWheels(blk[i]), "wheel", "[", "]");
				}
				
			}
			
			//Automatically add acbefore, acafter when there is voice block.
			
//			Codes = auto_acbefore(Codes, "voice");
			trace("!!!!!!!!!!!!!!!!!auto_acafter raw codes", Codes);
			Codes = auto_acafter(Codes, "voice");
//			Codes = auto_acafter(Codes, "touch");
			Codes = add_comma2(Codes);
			return Codes;
		}
		
//		code is orginal Code,  new_code is the piece just generated, type is "leg".   This function should be used only inside buildshellcode_V1
//		left right: [], 
		private function combine_current(code:String, new_code: String, type: String, left: String, Right: String):String{
			var code_slice1: String = "";
			var code_slice2: String = "";
			var code_slice3: String = "";
			var newcode_slice1: String = "";
			var newcode_slice2: String = "";
			var newcode_slice3: String = "";
			var type_pos: int;
			var newtype_pos: int;
			var rightbr_pos: int;
			var newleftbr_pos: int;
			var newrightbr_pos: int;
			trace("code",code);
			trace("new_code",new_code);
			type_pos = code.search(type);
			newtype_pos = new_code.search(type);
			
			if((type_pos != -1)&&(newtype_pos != -1)){
				code_slice1 = code.slice(0, type_pos);
				rightbr_pos = code.indexOf(Right, type_pos);
				code_slice2 = code.slice(type_pos, rightbr_pos);
				code_slice3 = code.slice(rightbr_pos);
				
				newleftbr_pos = new_code.indexOf(left);
				newcode_slice1 = new_code.slice(0, newleftbr_pos);
				newrightbr_pos = new_code.indexOf(Right, newleftbr_pos);
				newcode_slice2 = new_code.slice(newleftbr_pos+1, newrightbr_pos);
				newcode_slice3 = new_code.slice(newrightbr_pos);

				
				code = code_slice1 + code_slice2 + "," + newcode_slice2 +code_slice3;
				
				trace("code_slice1",code_slice1);
				trace("newcode_slice1",newcode_slice1);
				trace("newcode_slice2",newcode_slice2);
				trace("newcode_slice3",newcode_slice3);
				trace("code",code);
			}
			else
				code += new_code;
			return code;
			
		}
		
		private function position_right_brk(code: String, type: String):int{
			var block_layer_cnt: int;
			var bracelet_pos_l: int;
			var bracelet_pos_r: int;
			var rightbr_pos: int;
			var type_pos: int;
			type_pos = code.indexOf(type);
			rightbr_pos = type_pos;
			//find rightbr_pos	
			do{
				trace("block_pos",type_pos);
				
				bracelet_pos_l = code.indexOf("{", rightbr_pos);
				trace("bracelet_pos_l",bracelet_pos_l);
				
				bracelet_pos_r = code.indexOf("}", rightbr_pos);
				trace("bracelet_pos_r",bracelet_pos_r);
				
				//					break when no bracelet found
				if((bracelet_pos_r == -1)&&(bracelet_pos_l == -1)){
					break;
				}
				
				//					cnt++ when most left is "{", otherwise cnt--
				if((bracelet_pos_l < bracelet_pos_r)&&(bracelet_pos_l != -1)){
					block_layer_cnt++;
					rightbr_pos = bracelet_pos_l + 1;
				}
				else {
					block_layer_cnt--;
					rightbr_pos = bracelet_pos_r + 1;
				}
				
				trace("block_layer_cnt",block_layer_cnt);
				
				//max loop limit
				if(block_layer_cnt == 0)//
					break;
				
			}while(block_layer_cnt > 0)
			
			return rightbr_pos;
		}
		
		private function combine_touch(code:String, new_code: String, type: String, left: String, Right: String):String{
			var code_slice1: String = "";
			var code_slice2: String = "";
			var code_slice3: String = "";
			var newcode_slice1: String = "";
			var newcode_slice2: String = "";
			var newcode_slice3: String = "";
			var type_pos: int;
			var newtype_pos: int;
			var rightbr_pos: int;
			var newleftbr_pos: int;
			var newrightbr_pos: int;

			type_pos = code.search(type);
			newtype_pos = new_code.search(type);
			trace("var code",code);
			trace("new_code",new_code);
			trace("type_pos",type_pos);
			trace("newtype_pos",newtype_pos);
			
			if((type_pos != -1)&&(newtype_pos != -1)){
				code_slice1 = code.slice(0, type_pos);
				
				rightbr_pos = position_right_brk(code, type);
				
				code_slice2 = code.slice(type_pos, rightbr_pos-2);
				code_slice3 = code.slice(rightbr_pos);
				
				newleftbr_pos = new_code.indexOf(left);
				newcode_slice1 = new_code.slice(0, newleftbr_pos);
				//
				newrightbr_pos = position_right_brk(new_code, type);
//				newrightbr_pos = new_code.indexOf(Right, newleftbr_pos);
				newcode_slice2 = new_code.slice(newleftbr_pos+1, newrightbr_pos);
				newcode_slice3 = new_code.slice(newrightbr_pos);
				
				code = code_slice1 + code_slice2 + newcode_slice2 +code_slice3;
				
				trace("code_slice1",code_slice1);
				trace("code_slice2",code_slice2);
				trace("newcode_slice1",newcode_slice1);
				trace("newcode_slice2",newcode_slice2);
				trace("newcode_slice3",newcode_slice3);
				trace("code",code);
			}
			else
				code += new_code;
			
			trace("code",code);
			return code;
			
		}
		
		//Add actionbefore bracelets for the part before functional block("voice", "posture", "touching", "answer", "wheel"), only for Main program.(all blocks version will be done later)
		public function auto_acbefore(raw_code: String, func_block_type: String):String{
			var new_code: String = "";
			var block_pos: int;
			var block_pos_voice: int;
			var block_pos_touch: int;
			var block_pos_posture: int;
			var block_layer_cnt: int;
			//			raw_code = "\""+block_type+"\":";
			var bracelet_pos_l: int;
			var bracelet_pos_r: int;
			var acbefore_pos: int;
			var acafter_pos: int;
			
			
			block_pos_voice = raw_code.search("\""+func_block_type+"\":");// here is "voice" now
			block_pos_touch = raw_code.search("\""+"touch"+"\":");
			block_pos_posture = raw_code.search("\""+"posture"+"\":");
			
			if((block_pos_voice==-1)&&(block_pos_touch==-1)&&(block_pos_posture==-1))
				block_pos=-1;
			
			if(block_pos != -1){
				var array_pos: Array = new Array;
				if(block_pos_voice>-1)
					array_pos.push(block_pos_voice);
				if(block_pos_touch>-1)
					array_pos.push(block_pos_touch);
				if(block_pos_posture>-1)
					array_pos.push(block_pos_posture);
				
				array_pos.sort(Array.DESCENDING | Array.NUMERIC);
				
				block_pos = array_pos.pop();
				trace("push pos 1", block_pos);
				if(block_pos ==0){
					block_pos = array_pos.pop();
					trace("push pos 2", block_pos);
					if(block_pos ==0){
						block_pos = array_pos.pop();
						trace("push pos 3", block_pos);					
					}
				}
		
				
			}
			
			
			acbefore_pos = raw_code.search("\""+"actionsBefore"+"\":");
			acafter_pos = raw_code.indexOf("\""+"actionsAfter"+"\":", block_pos);
			
			if((block_pos == -1)){			//add actionbefore block when there is no voice block 
				if(acbefore_pos == -1)
				new_code = "\n\"actionsBefore\":[\n" + raw_code + "\n]\n" ;
				else if(acbefore_pos != -1){
					trace("acbefore_pos replace acbefore", acbefore_pos);
					new_code = raw_code.replace("\n\"actionsBefore\":[\n","");
					new_code = new_code.replace("\n]\n","");
					new_code = "\n\"actionsBefore\":[\n" + new_code + "\n]\n" ;
					return new_code;
				}
					
			}
			//			condition: actionbefore block is already done
//			else 
//				
//				
//				if((acbefore_pos != -1) && (block_pos != -1) && (acbefore_pos < block_pos))
//				return raw_code;			
			else if(block_pos != -1){			//			condition: actionbefore block needs to be done	
				
				
//				if(raw_code.search("{") == 1)
//					raw_code = raw_code.replace("{","");
				
				var splitted_partA: String = raw_code.slice(0 , block_pos-1);
				var splitted_partB: String = raw_code.slice(block_pos);
				
				splitted_partA = splitted_partA.replace("\n\"actionsBefore\":[\n","");
				splitted_partA = splitted_partA.replace("\n]\n","");
				
				new_code = "\n\"actionsBefore\":[\n" + splitted_partA + "\n],\n" + splitted_partB;
				//				var splitted:Array = new_code.split("\""+func_block_type+"\":");
				trace("0",splitted_partA);
				trace("1",splitted_partB);
				trace("2",new_code);
				//				new_code = splitted[0] + "\n],\n" + "\""+func_block_type+"\":" + splitted[1];
				
			}else{
				return raw_code;
			}
			return new_code;//must write return???
		}
		
		//Add actionafter bracekets for the part before "}" of functional block("voice", "posture", "touching"), only for Main program.(all blocks version will be done later)
		public function auto_acafter(raw_code: String, func_block_type: String):String{
			var new_code: String = "";
			var block_pos: int;
			var block_layer_cnt: int;
			//			raw_code = "\""+block_type+"\":";
			var bracelet_pos_l: int;
			var bracelet_pos_r: int;
			var acbefore_pos: int;
			var acafter_pos: int;
			
//			block_pos = raw_code.search("\""+func_block_type+"\":");
//			if(block_pos==-1){
//				block_pos = raw_code.search("\""+"touch"+"\":");
//				if(block_pos==-1)
//					block_pos = raw_code.search("\""+"posture"+"\":");
//						if(block_pos==-1)
//							block_pos=-1;
//			}
			var block_pos_voice: int;
			var block_pos_touch: int;
			var block_pos_posture: int;
			
			block_pos_voice = raw_code.search("\""+func_block_type+"\":");// here is "voice" now
			block_pos_touch = raw_code.search("\""+"touch"+"\":");
			block_pos_posture = raw_code.search("\""+"posture"+"\":");
			
			if((block_pos_voice==-1)&&(block_pos_touch==-1)&&(block_pos_posture==-1))
				block_pos=-1;
			
			if(block_pos != -1){
				var array_pos: Array = new Array;
				if(block_pos_voice>-1)
					array_pos.push(block_pos_voice);
				if(block_pos_touch>-1)
					array_pos.push(block_pos_touch);
				if(block_pos_posture>-1)
					array_pos.push(block_pos_posture);
				
				array_pos.sort(Array.DESCENDING | Array.NUMERIC);
				
				block_pos = array_pos.pop();
				trace("push pos 1", block_pos);
				if(block_pos ==0){
					block_pos = array_pos.pop();
					trace("push pos 2", block_pos);
					if(block_pos ==0){
						block_pos = array_pos.pop();
						trace("push pos 3", block_pos);					
					}
				}
				
				
			}
			
			trace("voice pos", block_pos);
			acafter_pos = raw_code.indexOf("\""+"actionsAfter"+"\":", block_pos);
			trace("block_pos",block_pos);
			//condition: actionafter block is already done
//			if((acafter_pos != -1) && (block_pos != -1) && (acafter_pos > block_pos))
//				return raw_code;
//				
//				//condition: actionafter block needs to be done, check"}" of block first.	
//			else 
				if((block_pos != -1)){
				block_layer_cnt = 0;
				
				//Check braceket "{" and "}" until all the pairs are finished, so that pattern searching can't go into "{ ...}"
				do{
					trace("block_pos",block_pos);
					bracelet_pos_l = raw_code.indexOf("{", block_pos);
					trace("bracelet_pos_l",bracelet_pos_l);
					
					bracelet_pos_r = raw_code.indexOf("}", block_pos);
					trace("bracelet_pos_r",bracelet_pos_r);
					
					//					break when no bracelet found
					if((bracelet_pos_r == -1)&&(bracelet_pos_l == -1)){
						break;
					}
					
					//					cnt++ when most left is "{", otherwise cnt--
					if((bracelet_pos_l < bracelet_pos_r)&&(bracelet_pos_l != -1)){
						block_layer_cnt++;
						block_pos = bracelet_pos_l + 1;
					}
					else {
						block_layer_cnt--;
						block_pos = bracelet_pos_r + 1;
					}
					
					trace("block_layer_cnt",block_layer_cnt);
					
					//max loop limit
					if(block_layer_cnt == 0)//if((block_layer_cnt == 5)||(block_layer_cnt == -5)||(block_layer_cnt == 0))
						break;
					
				}while(block_layer_cnt > 0)
						
				//at the end of loop, block_pos turns into position of last "}".
				var splitted_partA: String = raw_code.slice(0, block_pos+1);//+2 includes "," and "\n"
				var splitted_partB: String = raw_code.slice(block_pos + 2);//-1 to omit "}" of original main program
				
				var pattern_1:RegExp = /\n\"actionsBefore\":\[\n/g;
				var pattern_2:RegExp = /\n\]\n/g;
//				splitted_partB = splitted_partB.replace("\n\"actionsBefore\":[\n","");
//				splitted_partB = splitted_partB.replace("\n],\n","");
				splitted_partB = splitted_partB.replace(pattern_1,"");
				splitted_partB = splitted_partB.replace(pattern_2,"");
				
				new_code = splitted_partA + "\n\"actionsAfter\":[" + splitted_partB + "]\n"+ "\n";
				
				trace("block_pos",block_pos);
				trace("A",splitted_partA);
				trace("B",splitted_partB);
				trace("new_code",new_code);
			}else{
				return raw_code;
			}
			return new_code;//must write return???
		}
		
		// Steven add_comma for { }
		private function add_comma(org_code: String):String{
			var new_code: String = "";
			var search_pos: int;
			var block_layer_cnt: int;
			//			raw_code = "\""+block_type+"\":";
			var bracelet_pos_l: int;
			var bracelet_pos_r: int;
			
			var bracelet_pos_next_l: int;
			var bracelet_pos_next_r: int;
			var bracelet_pos_current: int;
			var bracelet_pos_next: int;
			var bracelet_type_current: int;//left 1, right 2
			var bracelet_type_next: int;
			var pos_block: int;
			var bracelet_pos_next_1: String = "";
			
			search_pos = 0;
			
			do{		
				bracelet_pos_l = org_code.indexOf("{", search_pos);
				bracelet_pos_r = org_code.indexOf("}", search_pos);
				trace("bracelet_pos_l",bracelet_pos_l);
				trace("bracelet_pos_r",bracelet_pos_r);
				//break when no bracelet found
				if((bracelet_pos_r == -1)&&(bracelet_pos_l == -1)){
					break;
				}
				
				//cnt++ when most left is "{", otherwise cnt--
				if((bracelet_pos_l < bracelet_pos_r)&&(bracelet_pos_l != -1)){
					block_layer_cnt++;
					bracelet_pos_current = bracelet_pos_l;
					bracelet_type_current = 1;
					search_pos = bracelet_pos_l;
				}
				else if(bracelet_pos_r != -1) {
					block_layer_cnt--;
					bracelet_pos_current = bracelet_pos_r;
					bracelet_type_current = 2;
					search_pos = bracelet_pos_r;
				}
				
				bracelet_pos_next_l = org_code.indexOf("{", search_pos+1);
				bracelet_pos_next_r = org_code.indexOf("}", search_pos+1);
				if((bracelet_pos_next_l<bracelet_pos_next_r)&&(bracelet_pos_next_l !=-1)){
					bracelet_pos_next = bracelet_pos_next_l;
					bracelet_type_next = 1;
				}
				else if((bracelet_pos_next_l>bracelet_pos_next_r)||((bracelet_pos_next_l ==-1)&&(bracelet_pos_next_r !=-1))){
					bracelet_pos_next = bracelet_pos_next_r;
					bracelet_type_next = 2;
				}
				
				if((bracelet_type_current==2)&&(bracelet_type_next==1)){
					var splitted_partA: String = org_code.slice(0, bracelet_pos_current);//
					var splitted_partB: String = org_code.slice(bracelet_pos_current);//"}" is in partB
//					trace("splitted_partA",splitted_partA);
//					trace("splitted_partB",splitted_partB);
					bracelet_pos_next_1 = org_code.slice(bracelet_pos_current+1, bracelet_pos_current+2);
					trace("bracelet_pos_next+1",bracelet_pos_next_1);
					var trueorfalse: Boolean = (bracelet_pos_next_1==',');
					trace("trueorfalse",trueorfalse);
					
//					pos_block = org_code.indexOf("voice", bracelet_pos_current);
//					if(pos_block==-1){
//						pos_block = org_code.indexOf("touch", bracelet_pos_current);
//						if(pos_block==-1)
//							pos_block = org_code.indexOf("posture", bracelet_pos_current);
//								if(pos_block==-1)
//									pos_block=-1;
//					}
					
					var block_pos_voice: int;
					var block_pos_touch: int;
					var block_pos_posture: int;
					var block_pos_actionsAfter: int;//"actionsAfter"
					var block_pos: int;
					
					block_pos_voice = org_code.indexOf("\"voice\"", bracelet_pos_current);// here is "voice" now
					block_pos_touch = org_code.indexOf("\"touch\"", bracelet_pos_current);
					block_pos_posture = org_code.indexOf("\"posture\"", bracelet_pos_current);
//					block_pos_posture = org_code.indexOf("\"actionsAfter\"", block_pos_actionsAfter);//seems wrong 
					block_pos_actionsAfter = org_code.indexOf("\"actionsAfter\"", bracelet_pos_current);//seems wrong 
					
					if((block_pos_voice==-1)&&(block_pos_touch==-1)&&(block_pos_posture==-1)&&(block_pos_actionsAfter==-1))
						block_pos=-1;
					
					if(block_pos != -1){
						var array_pos: Array = new Array;
						if(block_pos_voice>-1)
							array_pos.push(block_pos_voice);
						if(block_pos_touch>-1)
							array_pos.push(block_pos_touch);
						if(block_pos_posture>-1)
							array_pos.push(block_pos_posture);
						if(block_pos_actionsAfter>-1)
							array_pos.push(block_pos_actionsAfter);
						
						array_pos.sort(Array.DESCENDING | Array.NUMERIC);
						
						block_pos = array_pos.pop();
						trace("push pos 1", block_pos);
						if(block_pos ==0){
							block_pos = array_pos.pop();
							trace("push pos 2", block_pos);
							if(block_pos ==0){
								block_pos = array_pos.pop();
								trace("push pos 3", block_pos);		
								if(block_pos ==0){
									block_pos = array_pos.pop();
									trace("push pos 4", block_pos);					
								}
							}
						}
						
						
					}
					pos_block = block_pos;
					
			
					
					if((pos_block!=-1)&&(pos_block<bracelet_pos_next))
					{
						//do nothing, when }{ before actionbefore
					}else if((trueorfalse==true)&&(pos_block==-1))
					{
						//do nothing, when },{ after actionsafter
					} else
					if((trueorfalse==false)||(pos_block==-1)){
					org_code = splitted_partA+ splitted_partB.replace("}","},\n") ;
					search_pos++;
					trace("replace!!");
					}
					trace("bracelet_pos_l",bracelet_pos_l);
					trace("bracelet_pos_r",bracelet_pos_r);
					trace("bracelet_type_current",bracelet_type_current);
					trace("bracelet_pos_current",bracelet_pos_current);
					trace("bracelet_pos_next_l",bracelet_pos_next_l);
					trace("bracelet_pos_next_r",bracelet_pos_next_r);
					trace("bracelet_pos_next",bracelet_pos_next);
					trace("bracelet_type_next",bracelet_type_next);
					trace("search_pos",search_pos);
					trace("pos_block",pos_block);
					
				}
				search_pos++;
				
							
			}while(( bracelet_pos_r != -1)&&(bracelet_pos_l != -1))
			
			
			return org_code;
		}
		
		//combine for leg, music...
	
		// Steven add_comma for [ ]
		private function add_comma2(org_code: String):String{
			var new_code: String = "";
			var search_pos: int;
			var block_layer_cnt: int;
			//			raw_code = "\""+block_type+"\":";
			var bracelet_pos_l: int;
			var bracelet_pos_r: int;
			
			var bracelet_pos_next_l: int;
			var bracelet_pos_next_r: int;
			var bracelet_pos_current: int;
			var bracelet_pos_next: int;
			var bracelet_type_current: int;//left 1, right 2
			var bracelet_type_next: int;
			var pos_block: int;
			var bracelet_pos_next_1: String = "";
			
			search_pos = 0;
			
			do{		
				bracelet_pos_l = org_code.indexOf("[", search_pos);
				bracelet_pos_r = org_code.indexOf("]", search_pos);
				trace("bracelet_pos_l",bracelet_pos_l);
				trace("bracelet_pos_r",bracelet_pos_r);
				//break when no bracelet found
				if((bracelet_pos_r == -1)&&(bracelet_pos_l == -1)){
					break;
				}
				
				//cnt++ when most left is "{", otherwise cnt--
				if((bracelet_pos_l < bracelet_pos_r)&&(bracelet_pos_l != -1)){
					block_layer_cnt++;
					bracelet_pos_current = bracelet_pos_l;
					bracelet_type_current = 1;
					search_pos = bracelet_pos_l;
				}
				else if(bracelet_pos_r != -1) {
					block_layer_cnt--;
					bracelet_pos_current = bracelet_pos_r;
					bracelet_type_current = 2;
					search_pos = bracelet_pos_r;
				}
				
				bracelet_pos_next_l = org_code.indexOf("[", search_pos+1);
				bracelet_pos_next_r = org_code.indexOf("]", search_pos+1);
				if((bracelet_pos_next_l<bracelet_pos_next_r)&&(bracelet_pos_next_l !=-1)){
					bracelet_pos_next = bracelet_pos_next_l;
					bracelet_type_next = 1;
				}
				else if((bracelet_pos_next_l>bracelet_pos_next_r)||((bracelet_pos_next_l ==-1)&&(bracelet_pos_next_r !=-1))){
					bracelet_pos_next = bracelet_pos_next_r;
					bracelet_type_next = 2;
				}
				
				if((bracelet_type_current==2)&&(bracelet_type_next==1)){
					var splitted_partA: String = org_code.slice(0, bracelet_pos_current);//
					var splitted_partB: String = org_code.slice(bracelet_pos_current);//"}" is in partB
					//					trace("splitted_partA",splitted_partA);
					//					trace("splitted_partB",splitted_partB);
					bracelet_pos_next_1 = org_code.slice(bracelet_pos_current+1, bracelet_pos_current+2);
					trace("bracelet_pos_next+1",bracelet_pos_next_1);
					var trueorfalse: Boolean = (bracelet_pos_next_1==',');
					trace("trueorfalse",trueorfalse);
					pos_block = org_code.indexOf("voice", bracelet_pos_current);
					if((pos_block!=-1)&&(pos_block<bracelet_pos_next))
					{
						//do nothing, when }{ before actionbefore
					}else if((trueorfalse==true)&&(pos_block==-1))
					{
						//do nothing, when },{ after actionsafter
					} else
						if((trueorfalse==false)||(pos_block==-1)){
							org_code = splitted_partA+ splitted_partB.replace("]","],\n") ;
							search_pos++;
							trace("replace!!");
						}
					trace("bracelet_pos_l",bracelet_pos_l);
					trace("bracelet_pos_r",bracelet_pos_r);
					trace("bracelet_type_current",bracelet_type_current);
					trace("bracelet_pos_current",bracelet_pos_current);
					trace("bracelet_pos_next_l",bracelet_pos_next_l);
					trace("bracelet_pos_next_r",bracelet_pos_next_r);
					trace("bracelet_pos_next",bracelet_pos_next);
					trace("bracelet_type_next",bracelet_type_next);
					trace("search_pos",search_pos);
					trace("pos_block",pos_block);
					
				}
				search_pos++;
				
				
			}while(( bracelet_pos_r != -1)&&(bracelet_pos_l != -1))
			
			
			return org_code;
		}
		
		
		//16. Malcolm Check Position before save
		private function checkPos(blk:Object):String{
				var errorMsg:String = null;
				
//					PosChecker.sharedManager().checkPosSHELL(blk);
				errorMsg = PosChecker.checkPosSHELL(blk);
					return errorMsg;
			}
		
		private function buildMovementLOG(blk:Object):String{
			var log:String = "";
			var errormsg:String = "";
			//17. Malcolm Complex Range checker
				var arrayForCheck:Array = new Array;
			//
				//17. Malcolm Clear movement
				PosChecker.sharedManager().clearmovementSet();
			//
			for(var c:int=0; c < blk.length; c++)
			{
				//16. Malcolm Basic Check Pos
				
				if(errormsg == '')//when there is an error, show dialog BOX!!
				{
					errormsg = checkPos(blk[c]);	
				}
				
				if (blk[c][0] == "GTMovement")
					{
							log +=  "\n" + "#1P" + checkBuildCodeinOperator(blk[c][1]) + " " + "#2P" + checkBuildCodeinOperator(blk[c][2]) + " " +
									"#3P" + checkBuildCodeinOperator(blk[c][3]) + " " + "#4P" + checkBuildCodeinOperator(blk[c][4]) + " " + 
									"#5P" + checkBuildCodeinOperator(blk[c][5]) + " " + "#6P" + checkBuildCodeinOperator(blk[c][6]) + " " +
									"#7P" + checkBuildCodeinOperator(blk[c][7]) + " " + "#8P" + checkBuildCodeinOperator(blk[c][8]) + " " + 
									"#9P" + checkBuildCodeinOperator(blk[c][9]) + " " + "#10P" + checkBuildCodeinOperator(blk[c][10]) + " " + 

								"#11P" + checkBuildCodeinOperator(blk[c][11]) + " " + "#12P" + checkBuildCodeinOperator(blk[c][12]) + " " + 
									checkBuildCodeinOperator(blk[c][13]);
							
//								var checkthis:Boolean = true; 
							//17. Malcolm Complex Range checker
								arrayForCheck = [checkBuildCodeinOperator(blk[c][1]),checkBuildCodeinOperator(blk[c][2]),
										checkBuildCodeinOperator(blk[c][3]),checkBuildCodeinOperator(blk[c][4]),
										checkBuildCodeinOperator(blk[c][5]),checkBuildCodeinOperator(blk[c][6]),
										checkBuildCodeinOperator(blk[c][7]),checkBuildCodeinOperator(blk[c][8]),
										checkBuildCodeinOperator(blk[c][9]),checkBuildCodeinOperator(blk[c][10]),
										checkBuildCodeinOperator(blk[c][11]),checkBuildCodeinOperator(blk[c][12])];
							// 17. Send to PosChecker.as
								PosChecker.sharedManager().addMovStep(arrayForCheck,c);
							//
						}
						else if (blk[c][0].indexOf(".log") > -1)
							{
									var rawName:String = blk[c][0];
									var trimmedName:String = rawName.replace("GTMovement.","");
									var file:File = File.applicationDirectory.resolvePath("movlog\\" + trimmedName);
									var fileStream:FileStream = new FileStream();
									fileStream.open(file, FileMode.READ);
									var str:String = fileStream.readUTFBytes(file.size);
									fileStream.close();
									//21. Remove #
										trace(str = str.substr(14));
									//
									log += str;
								}
							}
			return log;
		}
		
		private function buildCodewithinCF(blk:Object):String{
			var Codes:String = buildSHELLCode(blk);	
//			var Codes:String = buildSHELLCode_withinCF(blk);	
			return Codes;
		}
		
//		This is used to solve auto_before problem, inside json cell.
		private function buildCodewithinCF_V1(blk:Object):String{
//			var Codes:String = buildSHELLCode(blk);	
			var Codes:String = buildSHELLCode_withinCF(blk);	
			return Codes;
		}
		
		//convert a string in to a sequenced key words, then generate related motions.
		public static var user_mov:Array = new Array(); 
		private function build_fake_NLP(animated_say:String):String{
			var Codes:String = "";
			var found_something : Boolean = false;
			
			//Solution of using array, when new language is used, don't forget to change here!!!!!
			var predefined_mov:Array = new Array(); 
			predefined_mov.push({name:["Walk", "walk","WALK"],name_zh:"走", id:"K0001", number:"-1"}); 
			predefined_mov.push({name:["Hello", "hello","HELLO", "Hi", "hi", "HI"],name_zh:"你好", id:"K0005", number:"-1"}); 
			predefined_mov.push({name:["Stand up", "stand up","STAND UP"],name_zh:["起立", "起来"], id:"K0006", number:"-1"}); 
			predefined_mov.push({name:["Bow1", "bow1", "bow","BOW1","BOW"],name_zh:["鞠躬1", "鞠躬"], id:"K0007", number:"-1"}); 
			predefined_mov.push({name:["Bow2", "bow2","BOW2"],name_zh:"鞠躬2", id:"K0008", number:"-1"});
			predefined_mov.push({name:["Lift right hand", "lift right hand","LIFT RIGHT HAND"],name_zh:["举起右手","举右手"], id:"K0009", number:"-1"});
			predefined_mov.push({name:["Lift left hand", "lift left hand","LIFT LEFT HAND"],name_zh:["举起左手","举左手"], id:"K0010", number:"-1"}); 
			predefined_mov.push({name:["Lift both hands", "lift both hands","LIFT BOTH HANDS"],name_zh:"举起双手", id:"K0011", number:"-1"}); 
			predefined_mov.push({name:["Lift left leg", "lift left leg","LIFT LEFT LEG"],name_zh:["抬起左脚","抬左脚"], id:"K0012", number:"-1"}); 
			predefined_mov.push({name:["Lift right leg", "lift right leg","LIFT RIGHT LEG"],name_zh:["抬起右脚","抬右脚"], id:"K0013", number:"-1"}); 
			predefined_mov.push({name:["Proposal" ,"proposal","PROPOSAL"],name_zh:"求婚", id:"K0014", number:"-1"});
			predefined_mov.push({name:["Soccer", "soccer","SOCCER"],name_zh:["踢球","踢"], id:"K0015", number:"-1"});
			predefined_mov.push({name:["Mega beam", "mega beam","MEGA BEAM"],name_zh:"探照灯", id:"K0016", number:"-1"}); 
			predefined_mov.push({name:["Rock paper scissors", "rock paper scissors","ROCK PAPER SCISSORS"],name_zh:"石头剪子布", id:"K0017", number:"-1"}); 
			predefined_mov.push({name:["Flip", "flip","FLIP"],name_zh:"翻跟头", id:"K0018", number:"-1"}); 
			predefined_mov.push({name:["Sport", "sport","SPORT"],name_zh:"做运动", id:"K0019", number:"-1"}); 
			predefined_mov.push({name:["Explain", "explain","EXPLAIN"],name_zh:"解释", id:"P0026", number:"-1"}); 
			predefined_mov.push({name:["Me", "me","ME", "I "],name_zh:"我", id:"P0031", number:"-1"}); 
			predefined_mov.push({name:["You", "YOU","you"],name_zh:"你", id:"P0033", number:"-1"}); 
			predefined_mov.push({name:["Think", "think","THINK"],name_zh:"想", id:"P0035", number:"-1"}); 
			predefined_mov.push({name:["Despair", "despair","DESPAIR"],name_zh:"消失", id:"P0036", number:"-1"}); 
			predefined_mov.push({name:["Fear", "fear","FEAR"],name_zh:"害怕", id:"P0037", number:"-1"}); 
			predefined_mov.push({name:["Joyful", "joyful","JOYFUL", "Happy", "happy", "HAPPY"],name_zh:["开心", "高兴"], id:"P0038", number:"-1"}); 
			predefined_mov.push({name:["Shame", "shame","SHAME"],name_zh:"害羞", id:"P0039", number:"-1"}); 
			predefined_mov.push({name:["Cannot", "cannot","CANNOT", "can't", "Can't", "CAN'T", "do not", "DO NOT", "Don't", "don't", "DON'T"],name_zh:["不能", "不会"], id:"P0040", number:"-1"}); 
			predefined_mov.push({name:["Yes", "yes","YES"],name_zh:"是", id:"P0049", number:"-1"}); 
			predefined_mov.push({name:["Know", "know","KNOW"],name_zh:"知道", id:"P0055", number:"-1"}); 
			predefined_mov.push({name:["Please", "please","PLEASE"],name_zh:"请", id:"P0060", number:"-1"}); 
			predefined_mov.push({name:["Sing", "sing","SING"],name_zh:"唱", id:"P0063", number:"-1"}); 
			predefined_mov.push({name:["Love", "love","LOVE"],name_zh:"爱", id:"P0064", number:"-1"}); 
			predefined_mov.push({name:["Calm", "calm","CALM"],name_zh:"冷静", id:"P0066", number:"-1"}); 
			predefined_mov.push({name:["Undetermined", "undetermined","UNDETERMINED"],name_zh:"没决定", id:"P0068", number:"-1"}); 
			predefined_mov.push({name:["Hug", "hug","HUG"],name_zh:"拥抱", id:"P0070", number:"-1"}); 
			predefined_mov.push({name:["Nervous", "nervous","NERVOUS"],name_zh:"紧张", id:"P0071", number:"-1"}); 
			predefined_mov.push({name:["We", "we","WE"],name_zh:"我们", id:"P0072", number:"-1"}); 
			predefined_mov.push({name:["Drink", "drink","DRINK"],name_zh:"喝", id:"P0075", number:"-1"}); 
			predefined_mov.push({name:["Eat", "eat","EAT"],name_zh:"吃", id:"P0076", number:"-1"}); 
			predefined_mov.push({name:["You all", "you all","YOU ALL"],name_zh:"你们", id:"P0073", number:"-1"}); 
			predefined_mov = predefined_mov.concat(user_mov);
			
			for each (var item in predefined_mov) 
			{ 
			switch(filename_language)
			{
				case "zh":
					if(animated_say.search(item.name_zh) != -1)
					{
						found_something = true;
						item.number = animated_say.search(item.name_zh).toString();
					}
				break;
				case "en":
					for each (var element in item.name) //modification to adapt to lower case
				{	if(animated_say.search(element) != -1)
					{
						found_something = true;
						item.number = animated_say.search(element).toString();
					}
					
//					if(animated_say.search(item.name) != -1)
//					{
//						found_something = true;
//						item.number = animated_say.search(item.name).toString();
//					}
					
				}
				break;
				default:
					break;
			}

			} 
			
			if(found_something)
			{	
				predefined_mov.sortOn("number", Array.NUMERIC); 
				
				Codes = Codes +  ",\"leg\": [" ;
				
			for each (var item in predefined_mov) 
			{ 
				if(item.number != "-1")
				{
					Codes = Codes + "\""+item.id + ".log\",";
				}
			} 


				Codes = Codes.slice(0, -1) + "\]";
			}
			
			found_something = false;
			return Codes;
		}
		
		private function buildCustomMovement(blk:Object):String{
//			var cusMotion:String = "am broadcast -a com.app.bc.RegSyncWrite --es movlog ";
			var cusMotion:String = "\"leg\": [\"";
			var logName:String = blk[0];
			var logNameTrimmed:String = logName.replace("GTMovement.", "");
			cusMotion += logNameTrimmed + "\"]\n";
			return cusMotion;
		}
		
		private function buildMotion(blk:Object):String{
			var check:String = "";
			
			if (blk == "standby.GTmotion")
			{
				check += "am broadcast -a com.app.bc.RegSyncWrite --es movlog 000.log"+"\n";
			}
			else if (blk == "walk.GTmotion")
			{
				check += "am broadcast -a com.app.bc.RegSyncWrite --es movlog 001.log"+"\n";
			}
			else if (blk == "turnleft.GTmotion")
			{
				check += "am broadcast -a com.app.bc.RegSyncWrite --es movlog 002.log"+"\n";		
			}
			else if (blk == "turnright.GTmotion")
			{
				check += "am broadcast -a com.app.bc.RegSyncWrite --es movlog 003.log"+"\n";		
			}
			else if (blk == "Walk_json.GTmotion")//Mr. Wang's JSON
			{
				check += "\"leg\": [\"K0001.log\"]\n";		
			}
			else if (blk == "Stand up_json.GTmotion")//Mr. Wang's JSON
			{
				check += "\"leg\": [\"K0006.log\"]\n";		
			}
			else if (blk == "Hello_json.GTmotion")//Mr. Wang's JSON
			{
				check += "\"leg\": [\"K0005.log\"]\n";		
			}
			else if (blk == "Bow1_json.GTmotion")//Mr. Wang's JSON
			{
				check += "\"leg\": [\"K0007.log\"]\n";		
			}
			else if (blk == "Bow2_json.GTmotion")//Mr. Wang's JSON
			{
				check += "\"leg\": [\"K0008.log\"]\n";		
			}
			else if (blk == "Lift right hand_json.GTmotion")//Mr. Wang's JSON
			{
				check += "\"leg\": [\"K0009.log\"]\n";		
			}
			else if (blk == "Lift left hand_json.GTmotion")//Mr. Wang's JSON
			{
				check += "\"leg\": [\"K0010.log\"]\n";		
			}
			else if (blk == "Lift both hands_json.GTmotion")//Mr. Wang's JSON
			{
				check += "\"leg\": [\"K0011.log\"]\n";		
			}
			else if (blk == "Lift left leg_json.GTmotion")//Mr. Wang's JSON
			{
				check += "\"leg\": [\"K0012.log\"]\n";		
			}
			else if (blk == "Lift right leg_json.GTmotion")//Mr. Wang's JSON
			{
				check += "\"leg\": [\"K0013.log\"]\n";		
			}
			else if (blk == "Proposal_json.GTmotion")//Mr. Wang's JSON
			{
				check += "\"leg\": [\"K0014.log\"]\n";		
			}
			else if (blk == "Soccer_json.GTmotion")//Mr. Wang's JSON
			{
				check += "\"leg\": [\"K0015.log\"]\n";		
			}
			else if (blk == "Mega beam_json.GTmotion")//Mr. Wang's JSON
			{
				check += "\"leg\": [\"K0016.log\"]\n";		
			}
			else if (blk == "Rock paper scissors.GTmotion")//Mr. Wang's JSON
			{
				check += "\"leg\": [\"K0017.log\"]\n";		
			}
			else if (blk == "Flip_json.GTmotion")//Mr. Wang's JSON
			{
				check += "\"leg\": [\"K0018.log\"]\n";		
			}
			else if (blk == "Sport_json.GTmotion")//Mr. Wang's JSON
			{
				check += "\"leg\": [\"K0019.log\"]\n";		
			}

			
			
			
			
			
			else if (blk == "hello.GTmotion")
			{
				check += "am broadcast -a com.app.bc.RegSyncWrite --es movlog 004.log"+"\n";		
			}
			else if (blk == "standup.GTmotion")
			{
				check += "am broadcast -a com.app.bc.RegSyncWrite --es movlog 005.log"+"\n";		
			}
			else if (blk == "sit.GTmotion")
			{
				check += "am broadcast -a com.app.bc.RegSyncWrite --es movlog 006.log"+"\n";		
			}
			else if (blk == "shame.GTmotion")
			{
				check += "am broadcast -a com.app.bc.RegSyncWrite --es movlog 007.log"+"\n";		
			}
			else if (blk == "cannot.GTmotion")
			{
				check += "am broadcast -a com.app.bc.RegSyncWrite --es movlog 008.log"+"\n";		
			}
			else if (blk == "dance2.GTmotion")
			{
				check += "am broadcast -a com.app.bc.RegSyncWrite --es movlog 102.log"+"\n";		
			}
			else if (blk == "dance4.GTmotion")
			{
				check += "am broadcast -a com.app.bc.RegSyncWrite --es movlog 104.log"+"\n";		
			}	
			
			//motionCode += check;
			return check;
		}
		
		private function buildBashControl(blk:Object):String{
			var modControlCode:String = "";
			
				if (blk.length >1) // modified to 0, it was 1!!!
				{
					modControlCode += buildExtendedBlockControl(blk);
				}
				
				if (blk == "stop.SHELL")
				{
					modControlCode += "am broadcast -a com.android.tdc.motor.poweroff" + "\n" + "return" + "\n";	
				}
				else if (blk  == "cell_json.SHELL.Waltz_chocobo_json")//Mr. Wang's JSON  blk[c] controlCode
				{
					modControlCode += "\n{\n" + "\"leg\": [\"" + "KD0001.log"+ "\"],\n" +  "\"music\": [\"" + "Waltz chocobo#music.log"+ "\"]\n" +"\n}\n";//
					modControlCode= modControlCode.split(" ").join("!!*space!!");
				}
				else if (blk  == "cell_json.SHELL.Uptown_funk_json")//Mr. Wang's JSON
				{
					modControlCode += "\n{\n" + "\"leg\": [\"" + "KD0002.log"+ "\"],\n" +  "\"music\": [\"" + "Uptown funk#music.log"+ "\"]\n" +"\n}\n";//
					modControlCode= modControlCode.split(" ").join("!!*space!!");
				}
				else if (blk  == "cell_json.SHELL.Little_apple_json")//Mr. Wang's JSON
				{
					modControlCode += "\n{\n" + "\"leg\": [\"" + "KD0003.log"+ "\"],\n" +  "\"music\": [\"" + "Little apple#music.log"+ "\"]\n" +"\n}\n";//
					modControlCode= modControlCode.split(" ").join("!!*space!!");
				}
				else if (blk  == "cell_json.SHELL.Hoshino_koi_json")//Mr. Wang's JSON
				{
					modControlCode += "\n{\n" + "\"leg\": [\"" + "KD0005.log"+ "\"],\n" +  "\"music\": [\"" + "Hoshino koi#music.log"+ "\"]\n" +"\n}\n";//
					modControlCode= modControlCode.split(" ").join("!!*space!!");
				}
				
				
				
			//motionCode += modControlCode;
			return modControlCode;
		}
		
	
		private function buildOperator(blk:Object):String 
		{
			var operatorCode:String = "";			
			if( (blk == ""))
			{
				return operatorCode;
			}
			else if(blk[0] == "SHELL>")
			{
				operatorCode += "[ " + checkBuildCodeinOperator(blk[1]) + " -gt " + checkBuildCodeinOperator(blk[2]) +" ]";
			}
			else if(blk[0] == "SHELL<")
			{
				operatorCode += "[ " + checkBuildCodeinOperator(blk[1]) + " -lt " + checkBuildCodeinOperator(blk[2]) + " ]";
			}
			else if(blk[0] == "SHELL=")
			{
				operatorCode += "[ " + checkBuildCodeinOperator(blk[1]) + " -eq " + checkBuildCodeinOperator(blk[2]) + " ]";
			} 
			else if(blk[0] == "+SHELL")
			{
				operatorCode += checkBuildCodeinOperator(blk[1]) + " + " + checkBuildCodeinOperator(blk[2]); 
			}
			else if(blk[0] == "-SHELL")
			{
				operatorCode += checkBuildCodeinOperator(blk[1]) + " - " + checkBuildCodeinOperator(blk[2]);
			}
			else if(blk[0] == "*SHELL")
			{
				operatorCode += checkBuildCodeinOperator(blk[1]) + " * " + checkBuildCodeinOperator(blk[2]);
			}
			else if(blk[0] == "/SHELL")
			{
				operatorCode += checkBuildCodeinOperator(blk[1]) + " / " + checkBuildCodeinOperator(blk[2]);
			}
			else if(blk[0] == "SHELL&")
			{
				operatorCode += " [ " +checkBuildCodeinOperator(blk[1]) + " -a " + checkBuildCodeinOperator(blk[2])+ " ] ";
			}
			else if(blk[0] == "SHELL|")
			{
				operatorCode += checkBuildCodeinOperator(blk[1])+" -o " + checkBuildCodeinOperator(blk[2]);
			}
			else if(blk[0] == "SHELLnot")
			{
				operatorCode += " [ " +"! " + checkBuildCodeinOperator(blk[1])+ " ] ";
			}
			else if(blk[0] == "hear_condition.Sense")
			{
				operatorCode += "grep " + blk[1] + " /mnt/sdcard/console/voice.txt";	//use # to find the exact pattern
			}
			
			return operatorCode;
		}
		
		private function checkBuildCodeinOperator(blk:Object):String{
			var opCode:String = "";
			
			if (!isNaN(Number(blk)))
			{
				opCode += blk; 
			}
			else if (blk.length > 1)
			{
				opCode += "0"; 
				return opCode;// return!!!Steven
				opCode += buildCodeWithinOperator(blk)
			}
			else
			{
				opCode += ""+blk;
			}
			return opCode;
		}
		
		private function buildCodeWithinOperator(blk:Object):String{
			var extOpCode:String = buildOperator(blk);
			return extOpCode;
		}
		
		private function buildExtendedBlockControl(blk:Object):String{
			var controlCode:String = "";
			var testloopcode:String = "";
			//var isCFblock:Boolean = false;
			for (var c:int = 0; c < blk.length; c++)
			{
				if (blk[c] == "doForever.SHELL")
				{
					if (blk[c+1] != null)
					{
						controlCode += "while :" + "\ndo"+ "\n" + buildCodewithinCF(blk[c+1]) + "\ndone\n";
					}
					else 
					{
						controlCode += "while :" + "\ndo"+"\ndone\n";
					}
					//break;  
				}
				else if (blk[c] == "wait:elapsed:from:.SHELL") 
				{   var n:String = ""; 
					if (blk[c+1] != null)
					{
						for (var i:int = 0; i < blk[c+1]+1; i++)
						{
							if (i != 0)
							{
								n += " " + i;
							}
						}					
					}
					controlCode += "for " + /*checkBuildCodeinOperator(blk[c+1])*/ "iw" +  " in" + n + "\ndo" + "\nsleep "+ "1" +"\ndone"+"\n";
					//break;
				}
				else if (blk[c] == "doIf.SHELL")
				{
					if (blk[c+2] != null)
					{
						controlCode += "if " + buildOperator(blk[c+1]) + ";" +  " then" + "\n" + buildCodewithinCF(blk[c+2]) + "fi"+ "\n";	
					}
					else 
					{
						controlCode += "if " + buildOperator(blk[c+1]) + ";" + " then" + "\n" + "fi" + "\n";
					}
				}
				else if (blk[c] == "doIfElse.SHELL")
				{
					//nthyet 
					if (blk[c+2] != null)
					{
						if(blk[c+3] != null)
						{
							controlCode += "if " + buildOperator(blk[c+1])+ ";" + " then" +"\n"+ buildCodewithinCF(blk[c+2]) + "else"+ "\n"+ buildCodewithinCF(blk[c+3]) +"fi"+"\n";
						}
						else
						{
							controlCode += "if " + buildOperator(blk[c+1])+ ";" + " then" +"\n"+ buildCodewithinCF(blk[c+2]) + "else"+ "\n"+ "fi"+"\n";
						}
					}
					else if (blk[c+2] == null && blk[c+3] != null)
					{
						controlCode += "if " + buildOperator(blk[c+1])+ ";" + " then" +"\n" + "else"+ "\n" + buildCodewithinCF(blk[c+3]) +"fi"+"\n";
					}
					else
					{
						controlCode += "if " + buildOperator(blk[c+1])+ ";"  + " then" +"\n" + "else"+ "\n"+ "fi"+"\n";
					}
				}
				else if (blk[c] == "doRepeat.SHELL")
				{
					var dorepeattimes:String = "";
					if (blk[c+1] != null)
					{
						for (var t:int = 0; t < blk[c+1]+1; ++t)
						{
							if (t != 0)
							{
								dorepeattimes += " " + t;
							}
						}					
					}
					if (blk[c+2] != null)
					{
						controlCode += "for " + "ir" + " in" + dorepeattimes + "\ndo\n"+ buildCodewithinCF(blk[c+2]) + "done\n";
					}
					else
					{
						controlCode += "for " + "ir" + " in" + dorepeattimes + "\ndo" + "\ndone\n";
					}
					
				}
				else if(blk[c] == "doWaitUntil.SHELL")
				{
					if (blk[c+1] != false)
					{
						controlCode += "a=0\n" + "\nwhile " + "\ndo" + "\nsleep 0.0005" + "\nif" + buildOperator(blk[c+1])+ "; then" + "\nbreak" 
							+ "\nfi" + "\ndone";
					}
					else
					{
						controlCode += "a=0\n" + "\nwhile " + "\ndo" + "\nsleep 0.0005" + "\nif" + "; then" + "\nbreak" 
							+ "\nfi" + "\ndone";
					}
				}
				else if(blk[c] == "doUntil.SHELL")
				{					
					
					if (blk[c+2] != null)
					{
						controlCode += "\nuntil " + buildOperator(blk[c+1])  + " \n do"+ "\n" + buildCodewithinCF(blk[c+2]) +  " \n done\n";
					}//"[ grep -q " + blk[1] + " /data/local/tmp/scratch/vcmd/voicecmd.vcmd]"
					else 
					{
						controlCode += "\nuntil " + buildOperator(blk[c+1]) + " \n do"+ "\n done\n";
					}
				}

				else if(blk[c] == "cell_json.SHELL")//JSON for Mr. Wang   
				{					
					
					if (blk[c+1] != null)
					{
						controlCode += "\n{\n" + buildCodewithinCF_V1(blk[c+1]) + "\n}\n";//
					}//
					else 
					{
						//
						controlCode += "\n{\n" + "\n}\n";
					}
				}
				
				else if(blk[c] == "cell_json.SHELL.animated_say")//
				{					
					
					if (blk[c+1] != null)
					{
						
						controlCode += "\n{\n" + "\"answer\": [\"" + blk[c+1] + "\"]\n" + build_fake_NLP(blk[c+1])+"\n}\n";//
						controlCode= controlCode.split(" ").join("!!*space!!");
					}//
					else 
					{
						//
						controlCode += "\n{\n" + "\n}\n";
					}
				}

				
				
//				else if(blk[c] == "pos_standing_json.SHELL")//JSON for Mr. Wang
//				{					
//					
//					if (blk[c+1] != null)
//					{
//						controlCode += "\n\"posture\": {\n\"stand\": {\n" + buildCodewithinCF(blk[c+1]) + "\n},\n" + "\n}\n";//
//					}//
//					else 
//					{
//						controlCode += "\n\"posture\": {\n\"stand\": {\n" + "\n},\n" + "\n}\n";//
//					}
//				}
//				else if(blk[c] == "pos_lying_json.SHELL")//JSON for Mr. Wang
//				{					
//					
//					if (blk[c+1] != null)
//					{
//						controlCode += "\n\"posture\": {\n\"lying\": {\n" + buildCodewithinCF(blk[c+1]) + "\n},\n" + "\n}\n";//
//					}//
//					else 
//					{
//						controlCode += "\n\"posture\": {\n\"lying\": {\n" + "\n},\n" + "\n}\n";//
//					}
//				}
//				else if(blk[c] == "pos_other_json.SHELL")//JSON for Mr. Wang
//				{					
//					
//					if (blk[c+1] != null)
//					{
//						controlCode += "\n\"posture\": {\n\"other\": {\n" + buildCodewithinCF(blk[c+1]) + "\n},\n" + "\n}\n";//
//					}//
//					else 
//					{
//						controlCode += "\n\"posture\": {\n\"other\": {\n" + "\n},\n" + "\n}\n";//
//					}
//				}
//				else if(blk[c] == "1_touch_json.SHELL")//JSON for Mr. Wang
//				{					
//					
//					if (blk[c+1] != null)
//					{
//						var newcode: String = "\n\"touch\": {\n\"one\": {\n" + buildCodewithinCF(blk[c+1]) + "\n}\n" + "\n},\n";//
//						trace("controlCode1",controlCode);
//						controlCode += newcode;
//						trace("controlCode2",controlCode);
////						controlCode = combine_touch(controlCode, newcode, "touch", "{", "},");
//					}//
//					else 
//					{
//						var newcode: String = "\n\"touch\": {\n\"one\": {\n" + "\n}\n" + "\n},\n";//
//						trace("controlCode3",controlCode);
//						controlCode = combine_touch(controlCode, newcode, "touch", "{", "},");
//						
////						controlCode += newcode;
//						trace("controlCode4",controlCode);
//
//					}
//				}
				else if(blk[c] == "voice_json.SHELL")//JSON for Mr. Wang  // voice should behind all other json!!
				{					
					
					if((blk[c+1] =="Hi")&&(MBlock.app.lang_flag == 'zh_CN'))
						blk[c+1]  = "你好";

					blk[c+1] = blk[c+1].split(" ").join("!!*space!!");
					if (blk[c+2] != null)
					{
						controlCode += "\n\"voice\": {v\n{\n\"input\": \"" + blk[c+1]  + "\", \n\"do\":{\n" + buildCodewithinCF(blk[c+2]) +  " }\n }\n v},\n";//controlCode += "\n\"voice\": {\n\"input\": \"" + blk[c+1]  + "\", \n\"do\":{\n" + buildCodewithinCF(blk[c+2]) +  " }\n },\n";
						
					}//"[ grep -q " + blk[1] + " /data/local/tmp/scratch/vcmd/voicecmd.vcmd]"
					else 
					{
						//						controlCode += "\nuntil " + buildOperator(blk[c+1]) + " \n do"+ "\n done\n";
						controlCode += "\n\"voice\": {v\n{\n\"input\": \"" + blk[c+1]  + "\", \n\"do\":{\n" + " }\n }\n v},\n";//controlCode += "\n\"voice\": {\n\"input\": \"" + blk[c+1]  + "\", \n\"do\":{\n" + " }\n },\n";
						
					}
				}
				else if(blk[c] == "voice_json_ifelse.SHELL")//JSON for Mr. Wang  // voice should behind all other json!!
				{					
					if((blk[c+1] =="Hi")&&(MBlock.app.lang_flag == 'zh_CN'))
						blk[c+1]  = "你好";
					
					blk[c+1] = blk[c+1].split(" ").join("!!*space!!");
					if (blk[c+2] != null)
					{
						if(blk[c+3] != null)
						{
							controlCode += "\n\"voice\": {v\n{\n\"input\": \"" + blk[c+1]  + "\", \n\"do\":{\n" + buildCodewithinCF(blk[c+2]) +  " },\n" + "\"dont\": {" + buildCodewithinCF(blk[c+3]) + "}\n"+" } v},\n" ;//
							//controlCode += "\n\"voice\": {\n\"input\": \"" + blk[c+1]  + "\", \n\"do\":{\n" + buildCodewithinCF(blk[c+2]) +  " },\n" + "\"dont\": {" + buildCodewithinCF(blk[c+3]) + "}\n"+" },\n" ;//
						}
						else
						{
							controlCode += "\n\"voice\": {v\n{\n\"input\": \"" + blk[c+1]  + "\", \n\"do\":{\n" + buildCodewithinCF(blk[c+2]) +  " }\n } v},\n";//
							//controlCode += "\n\"voice\": {\n\"input\": \"" + blk[c+1]  + "\", \n\"do\":{\n" + buildCodewithinCF(blk[c+2]) +  " }\n },\n";//
						}
					}
					else if (blk[c+2] == null && blk[c+3] != null)
					{
						controlCode += "\n\"voice\": {v\n{\n\"input\": \"" + blk[c+1]  + "\", \n\"do\":{\n"  +  " },\n" + "\"dont\": {" + buildCodewithinCF(blk[c+3]) + "}\n" +"} v},\n" ;//
						//controlCode += "\n\"voice\": {\n\"input\": \"" + blk[c+1]  + "\", \n\"do\":{\n"  +  " },\n" + "\"dont\": {" + buildCodewithinCF(blk[c+3]) + "}\n" +"},\n" ;//
					}
					else
					{
						controlCode += "\n\"voice\": {v\n{\n\"input\": \"" + blk[c+1]  + "\", \n\"do\":{\n" + " }\n } v},\n";
						//controlCode += "\n\"voice\": {\n\"input\": \"" + blk[c+1]  + "\", \n\"do\":{\n" + " }\n },\n";
					}

				}
				else
				{
					//break;
				}
			}
			
			
			return controlCode;				
		}
		//Malcolm end
		
		//Steven, LED
		private function buildLED(blk:Object):String{
			var ledCode:String = "";

			if (blk == "") 
			{
				return ledCode;
			}
			else if(blk[0] == "LED2028off.LED")
			{
				ledCode += "am broadcast -a com.android.tdc.i2c.close.AW2028"+ "\n";
			}
			else if(blk[0] == "LED2028on.LED")
			{
				ledCode += "am broadcast -a com.android.tdc.i2c.close.AW2028"+ "\n" + "am broadcast -a com.android.tdc.i2c.open.AW2028 --ei mode " + blk[1] + " --ei red " + blk[2] + " --ei green " + blk[3] + " --ei blue " + blk[4] + "\n";
			}
			else if(blk[0] == "LED_breath_on.LED")
			{
				ledCode += "\"led\": [\"on" + blk[1] + "#led.log" + "\"]\n";
			}
			else if(blk[0] == "LED_breath_off.LED")
			{
				ledCode += "\"led\": [\"off" + blk[1]+ "#led.log" + "\"]\n";
			}
			return ledCode;
		}
		
		//Steven, Sound  playSound.Sound:
		private function buildSound(blk:Object):String{
			var soundCode:String = "";

			if (blk == "") 
			{
				return soundCode;
			}
//			else if(blk[0] == "playSound.Sound")
			else if(blk[0] == "playSound:")
			{
				soundCode += "\"music\": [\"" + blk[1] + "#music.log" + "\"]\n";
			}
			else if(blk[0] == "answer.sound")
			{
				if((blk[1] =="Hi")&&(MBlock.app.lang_flag == 'zh_CN'))
					blk[1]  = "你好";
				blk[1] = blk[1].split(" ").join("!!*space!!");
				soundCode += "\"answer\": [\"" + blk[1] + "\"]\n";
			}

			return soundCode;
		}
		//
		
		//Steven, buildWheels
		private function buildWheels(blk:Object):String{
			var wheelsCode:String = "";
			
			if (blk == "") 
			{
				return wheelsCode;
			}
				//			else if(blk[0] == "playSound.Sound")
			else if(blk[0] == "forward.wheel")
			{
				wheelsCode += "\"wheel\": [\"forward" + blk[1] + "#wheel.log" + "\"]\n";
			}
			else if(blk[0] == "backward.wheel")
			{
				wheelsCode += "\"wheel\": [\"backward" + blk[1] + "#wheel.log" + "\"]\n";
			}
			
			return wheelsCode;
		}
		//

		
		//Steven, buildTouch
		private function buildTouch(blk:Object):String{
			var touchCode:String = "";
			if (blk == "") 
			{
				return touchCode;
			}
				//			else if(blk[0] == "playSound.Sound")
			else if(blk[0] == "1_touch_json.touch")
			{
				if(blk[1]!=null)
				touchCode += "\n\"touch\": {\n\"one\": {\n" + buildCodewithinCF(blk[1]) + "\n}\n" + "\n},\n";
				else if(blk[1]==null)
					touchCode += "\n\"touch\": {\n\"one\": {\n" + "\n}\n" + "\n},\n";
			}
			else if(blk[0] == "2_touch_json.touch")
			{
				if(blk[1]!=null)
					touchCode += "\n\"touch\": {\n\"two\": {\n" + buildCodewithinCF(blk[1]) + "\n}\n" + "\n},\n";
				else if(blk[1]==null)
					touchCode += "\n\"touch\": {\n\"two\": {\n" + "\n}\n" + "\n},\n";
			}
			else if(blk[0] == "3_touch_json.touch")
			{
				if(blk[1]!=null)
					touchCode += "\n\"touch\": {\n\"three\": {\n" + buildCodewithinCF(blk[1]) + "\n}\n" + "\n},\n";
				else if(blk[1]==null)
					touchCode += "\n\"touch\": {\n\"three\": {\n" + "\n}\n" + "\n},\n";
			}
			else if(blk[0] == "long_touch_json.touch")
			{
				if(blk[1]!=null)
					touchCode += "\n\"touch\": {\n\"long\": {\n" + buildCodewithinCF(blk[1]) + "\n}\n" + "\n},\n";
				else if(blk[1]==null)
					touchCode += "\n\"touch\": {\n\"long\": {\n" + "\n}\n" + "\n},\n";
			}

			
			return touchCode;
		}
		
		//Steven, buildPosture
		private function buildPosture(blk:Object):String{
			var postureCode:String = "";
			if (blk == "") 
			{
				return postureCode;
			}
				//			else if(blk[0] == "playSound.Sound")
			else if(blk[0] == "pos_standing_json.posture")
			{
				if(blk[1]!=null)
					postureCode += "\n\"posture\": {\n\"stand\": {\n" + buildCodewithinCF(blk[1]) + "\n}\n" + "\n},\n";
				else if(blk[1]==null)
					postureCode += "\n\"posture\": {\n\"stand\": {\n" + "\n}\n" + "\n},\n";
			}
			else if(blk[0] == "pos_lying_json.posture")
			{
				if(blk[1]!=null)
					postureCode += "\n\"posture\": {\n\"lying\": {\n" + buildCodewithinCF(blk[1]) + "\n}\n" + "\n},\n";
				else if(blk[1]==null)
					postureCode += "\n\"posture\": {\n\"lying\": {\n" + "\n}\n" + "\n},\n";
			}
			else if(blk[0] == "pos_other_json.posture")
			{
				if(blk[1]!=null)
					postureCode += "\n\"posture\": {\n\"other\": {\n" + buildCodewithinCF(blk[1]) + "\n}\n" + "\n},\n";
				else if(blk[1]==null)
					postureCode += "\n\"posture\": {\n\"other\": {\n" + "\n}\n" + "\n},\n";
			}
	
			
			return postureCode;
		}
		
		
		
		//Sense wait_voice_cmd
		private function buildSense(blk:Object):String{
			var senseCode:String = "";
			
			if (blk == "") 
			{
				return senseCode;
			}
				//			else if(blk[0] == "playSound.Sound")
			else if(blk[0] == "wait_voice_cmd.Sense")
			{
				senseCode += "voice=0\n"  
							+"while [ $voice -eq 0 ]\n"
							+"do\n"
							+"sleep 0.005\n"
							+"echo $voice\n"
							+"grep " + blk[1] + " /mnt/sdcard/console/voice.txt\n"		//use # to find the exact pattern
							+"if [ $? -eq 0 ]; then\n"
							+"voice=1\n"
							+"fi\n"
							+"done\n";
			}//hear_condition.Sense
			else if(blk[0] == "reset_voice_buffer.Sense")
			{
				senseCode += "$ echo > /mnt/sdcard/console/voice.txt\n"  
			}//$ echo > filename   reset_voice_buffer.Sense
			return senseCode;
		}
		
		private function buildSetup():String{
			var modInitCode:String = "";
			for(var i:int=0;i<moduleList.length;i++){
				var m:Object = moduleList[i];
				var code:* = m["code"]["setup"];
				code = code is CodeObj?code.code:code;
				if(code!=""){
					if(ccode_setup.indexOf(code)==-1&&ccode_setup_fun.indexOf(code)==-1){
						ccode_setup+=code+"";
					}
				}
			}
			return modInitCode;
		}
		static private const varNamePattern:RegExp = /^[_A-Za-z][_A-Za-z0-9]*$/;
		static private function castVarName(name:String, isFunction:Boolean=false):String
		{
			if(varNamePattern.test(name)){
				return name;
			}
			var newName:String = isFunction ? "__func_" : "__var_";
			for(var i:int=0; i<name.length; ++i){
				newName += "_" + name.charCodeAt(i).toString();
			}
			return newName;
		}
		
		private function buildDefine():String{
			var modDefineCode:String = ""
			for(var i:int=0;i<varList.length;i++){
				var v:String = varList[i];
				var code:* = StringUtil.substitute("double {0};\n" ,castVarName(v))
				if(ccode_def.indexOf(code)==-1){
					ccode_def+=code;
				}
			}
			for(i=0;i<moduleList.length;i++){
				var m:Object = moduleList[i]
				code = m["code"]["def"];
				code = code is CodeObj?code.code:code;
				if(code!=""){
					var array:Array = code.split("\n");
					for(var j:uint=0;j<array.length-1;j++){
						if(ccode_def.indexOf(array[j])==-1){
							ccode_def+=array[j]+"\n";
						}
					}
				}
			}
			return modDefineCode;
		}
		
		private function buildInclude():String{
			var modIncudeCode:String = ""
			for(var i:int=0;i<moduleList.length;i++){
				var m:Object = moduleList[i]
				var code:* = m["code"]["inc"];
				code = code is CodeObj?code.code:code;
				if(code!=""){
					if(ccode_inc.indexOf(code)==-1)
						ccode_inc+=code+"";
				}
			}
			return modIncudeCode;
		}
		
		private function buildLoopMaintance():String{
			var modMaintanceCode:String = ""
			for(var i:int=0;i<moduleList.length;i++){
				var m:Object = moduleList[i]
				var code:* = m["code"]["loop"];
				code = code is CodeObj?code.code:code;
				if(code!=""){
					if(modMaintanceCode.indexOf(code)==-1){
						modMaintanceCode+=code+"\n";
					}
				}
			}
			return modMaintanceCode
		}
		private function buildFunctions():String{
			var funcCodes:String = ""
			for(var i:int=0;i<funcList.length;i++){
				var m:Object = funcList[i]
				var code:* = m["code"];
				code = code is CodeObj?code.code:code;
				if(code!=""){
					if(funcCodes.indexOf(code)==-1)
						funcCodes+=code+"\n";
				}
			}
			return funcCodes;
		}
		private function getRequiredCpp():Array{
			return [];
			/*
			var modMapCpp:Object={"motor":"MeDCMotor","ultrasonic":"MeUltrasonic","servo":"MeServo","temperature":"MeTemperature","led":"MeRGBLed","gyro":"MeGyro","infrared":"MeInfraredReceiver","sevseg":"Me7SegmentDisplay"}
			var cppList:Array=[];
			return cppList;
			for(var i:int=0;i<moduleList.length;i++){
				var m:Object = moduleList[i]
				if(m["name"] in modMapCpp){
					var meModule:String = modMapCpp[m["name"]]
					if(cppList.indexOf(meModule)==-1)
						cppList.push(meModule)
				}
			}
			//			if(cppList.length>0 || moduleList.length>0)
			//cppList.push("MePort")
			return cppList
			*/
		}
		
		public function uploadCode(code:String):void{
			var url:String = "http://192.168.1.251:8080/";
			var request:URLRequest = new URLRequest(url);
			var requestVars:URLVariables = new URLVariables();
			requestVars.code = code;
			requestVars.sessionTime = new Date().getTime();
			request.data = requestVars;
			request.method = URLRequestMethod.POST;
			
			var urlLoader:URLLoader = new URLLoader();
			urlLoader = new URLLoader();
			urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
			urlLoader.addEventListener(Event.COMPLETE, uploadCompleteHandler,false,0,true);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler, false, 0, true);
			
			try{
				urlLoader.load(request);
			}catch(e:Error){
				trace(e);
			}
			
		}
		
		private function saveHexFile(token:String,hexString:String):void{
			var f:File = new File();
			f.addEventListener(Event.COMPLETE, _onRfComplete);
			f.save(hexString,token+".hex");
		}
		
		private function _onRfComplete(e:Event):void{
			hexPath = e.target.nativePath
			_scratch.dispatchEvent(new RobotEvent(RobotEvent.HEX_SAVED,hexPath));
		}
		
		private function uploadCompleteHandler(e:Event):void{
			var response:String = String(e.target.data);
			//trace("response:"+response);
			jsonObj = util.JSON.parse(response);
			hexCode = jsonObj["hex"]
			ccode = jsonObj["code"]
			token = jsonObj["hash"]
			output = jsonObj["output"]
			_scratch.dispatchEvent(new RobotEvent(RobotEvent.CCODE_GOT,ccode));
			_scratch.dispatchEvent(new RobotEvent(RobotEvent.COMPILE_OUTPUT,output));
			if(hexCode)
				saveHexFile(token,hexCode);
		}
		
		private function ioErrorHandler(e:Event):void{
			
		}
		
		
		
		
		/****** *****************************
		 * compiler ralated functions 
		 * **********************************/
		
		
		
		private var tc_projCpp:*;
		private var tc_workdir:*;
		private var tc_cppList:*;
		private var nativeDoneEvent:String;
		private var nativeWorkList:Array=[];
		private var srcDocuments:Array = [];
		private var numOfProcess:uint = 0;
		private var numOfSuccess:uint = 0;
		private var _projectDocumentName:String = "";
		private function prepareProjectDir(ccode:String):void{
			_currentDevice = DeviceManager.sharedManager().currentDevice;
			
			var cppList:Array =  requiredCpp;
			// get building direcotry ready
			var workdir:File = File.applicationStorageDirectory.resolvePath("scratchTemp");
			if(!workdir.exists){
				workdir.createDirectory(); 
			}
			//			var srcdir:File = File.applicationDirectory.resolvePath("compiler"); 
			if(!workdir.exists){
				return;
			}
			// copy firmware directory
			workdir = workdir.resolvePath(projectDocumentName);
//			var srcdir:File = ApplicationManager.sharedManager().documents.resolvePath("mBlock/libraries/"+_extSrcPath+"/src");
//			if(srcdir.exists && srcdir.getDirectoryListing().length > 0){
//				srcdir.copyTo(workdir,true);
//			}
			//*
			for each(var path:String in srcDocuments){
				var srcdir:File = new File(path);
				if(srcdir.exists && srcdir.isDirectory){
					copyCompileFiles(srcdir.getDirectoryListing(),workdir);
				}
			}
			//*/
			var projCpp:File = File.applicationStorageDirectory.resolvePath("scratchTemp/"+projectDocumentName+"/"+projectDocumentName+".ino")
			LogManager.sharedManager().log("projCpp:"+projCpp.nativePath);
			var outStream:FileStream = new FileStream();
			outStream.open(projCpp, FileMode.WRITE);
			outStream.writeUTFBytes(ccode)
			outStream.close()
			if(ccode.indexOf("updateVar")>-1){
				// aux ino file for serial variable parser
				projCpp = File.applicationStorageDirectory.resolvePath("scratchTemp/"+projectDocumentName+"/MeComm.ino")
				outStream = new FileStream();
				outStream.open(projCpp, FileMode.WRITE);
				outStream.writeUTFBytes(serialParserInoFile)
				outStream.close()
			}
			
			projectPath = workdir.nativePath;
			LogManager.sharedManager().log("projectPath:"+projectPath);
		}
		
		
		public function uploadHex(evt:*):void{
			//			if(SerialManager.sharedManager().device=="mbot"){
			//				ArduinoUploader.sharedManager().start(projectPath+"\\build\\"+projectDocumentName+".ino.hex");
			//			}else{
			SerialManager.sharedManager().upgrade(projectPath+"/build/"+projectDocumentName+".ino.hex");
			//			}
		}
		
		private var compileErr:Boolean = false;
		//*
		private function copyCompileFiles(files:Array, workdir:File):void
		{
			for(var i:int = 0; i < files.length; ++i){
				var file:File = files[i];
				switch(file.extension){
					case "cpp":
					case "c":{
						var fileName:String = file.name.split(".")[0];
						if(requiredCpp.indexOf(fileName) < 0){
							requiredCpp.push(fileName);
						}
					}
						//fall through
					case "h":
						file.copyTo(workdir.resolvePath(file.name), true);
						break;
				}
			}
		}
		//*/
		public function get projectDocumentName():String{
			var now:Date = new Date;
			var pName:String = MBlock.app.projectName().split(" ").join("").split("(").join("").split(")").join("");
			for(var i:uint=0;i<pName.length;i++){
				if(pName.charCodeAt(i)>100){
					pName = pName.split(pName.charAt(i)).join("_");
				}
			}
			_projectDocumentName = "project_"+pName+ (now.getMonth()+"_"+now.getDay());
			if(_projectDocumentName=="project_"){
				_projectDocumentName = "project";
			}
			return _projectDocumentName;
		}
		public function buildAll(ccode:String):String
		{
			if(isUploading){
				return "uploading";
			}
			/*
			if(arduinoInstallPath==""){
				var dialog:DialogBox = new DialogBox();
				dialog.addTitle("Message");
				dialog.addText("Arduino IDE not found,\nClick 'Set Path' to find the install path of Arduino,\nor Click 'Download' to install the Arduino IDE.");
				function onCancel():void{
					dialog.cancel();
				}
				
				function onSetPath():void{
					var fileRef:File = new File();
					function onPathSelected(evt:Event):void{
						var f:File = evt.target as File;
						arduinoPath = ApplicationManager.sharedManager().system==ApplicationManager.WINDOWS?f.url:(f.url+"/Arduino.app/Contents/Resources/Java");
					}
					fileRef.browseForDirectory(Translator.map("Arduino IDE"));
					fileRef.addEventListener(Event.SELECT,onPathSelected);
					dialog.cancel();
				}
				function onDownload():void{
					flash.net.navigateToURL(new URLRequest("http://learn.makeblock.cc/learning-arduino/"));
					dialog.cancel();
				}
				dialog.addButton("Cancel",onCancel);
				dialog.addButton("Set Path",onSetPath);
				dialog.addButton("Download",onDownload);
				dialog.showOnStage(MBlock.app.stage);
				return "Arduino IDE not found.";
			}
			*/
			_currentDevice = DeviceManager.sharedManager().currentDevice;
			// get building direcotry ready
			var workdir:File = File.applicationStorageDirectory.resolvePath("scratchTemp")
			if(!workdir.exists){
				workdir.createDirectory(); 
			} 
			
			if(!workdir.exists){
				return "workdir not exists";
			}
			nativeWorkList = []
			// copy firmware directory
			workdir = workdir.resolvePath(projectDocumentName);
//			var srcdir:File = ApplicationManager.sharedManager().documents.resolvePath("mBlock/libraries/"+_extSrcPath+"/src");
//			if(srcdir.exists && srcdir.getDirectoryListing().length > 0){
//				srcdir.copyTo(workdir,true);
//			}
			//*
			for each(var path:String in srcDocuments){
				var srcdir:File = new File(path);
				if(srcdir.exists && srcdir.isDirectory){
					copyCompileFiles(srcdir.getDirectoryListing(), workdir);
				}
			}
			//*/
			var projCpp:File = File.applicationStorageDirectory.resolvePath("scratchTemp/"+projectDocumentName+"/"+projectDocumentName+".ino")
			var outStream:FileStream = new FileStream();
			outStream.open(projCpp, FileMode.WRITE);
			outStream.writeUTFBytes(ccode)
			outStream.close()
			if(ccode.indexOf("updateVar")>-1){
				// aux ino file for serial variable parser
				projCpp = File.applicationStorageDirectory.resolvePath("scratchTemp/"+projectDocumentName+"/MeComm.ino")
				outStream = new FileStream();
				outStream.open(projCpp, FileMode.WRITE);
				outStream.writeUTFBytes(serialParserInoFile)
				outStream.close()
				ccode = ccode.replace("void setup(){",serialParserInoFile+"\nvoid setup(){"); // too tricky here?
			}
			
			// get MeModule source list
			var files:Array = workdir.getDirectoryListing()
			projectPath = workdir.nativePath
			// get build dir ready
			workdir = workdir.resolvePath("build")
			workdir.createDirectory()
			// yzj, don't use pre-build object any more, build from arduino libs
			/*
			// prepare build directory
			if(boardType=="leonardo")
			srcdir = srcdir.resolvePath("../gcc_template")
			else
			srcdir = srcdir.resolvePath("../gcc_template_uno")
			workdir = workdir.resolvePath("build")
			//workdir.deleteDirectory(true)
			srcdir.copyTo(workdir,true)
			*/
			// prebuild arduino lib
			buildArduinoLib(workdir);
			copyCompileFiles(files, workdir);
			
			// copy project.ino to ./build/project.ino.cpp
			// combine aux ino and main ino into 1 cpp file
			var dstFile:File = workdir.resolvePath(projectDocumentName+".ino.cpp")
			outStream = new FileStream();
			outStream.open(dstFile, FileMode.WRITE);
			outStream.writeUTFBytes(ccode);
			outStream.close();
			// start building arduino libs
			nativeDoneEvent = EVENT_LIBCOMPILE_DONE
			numOfProcess = nativeWorkList.length
			numOfSuccess = 0
			compileErr = false;
			isUploading = true;
			dispatchEvent(new Event(EVENT_NATIVE_DONE));
			tc_projCpp = projCpp
			tc_workdir = workdir
			tc_cppList = requiredCpp;
			return ""
		}
		
		
		public function openArduinoIDE(ccode:String):String{
			/*
			if(arduinoInstallPath==""){
				var dialog:DialogBox = new DialogBox();
				dialog.addTitle("Message");
				dialog.addText("Arduino IDE not found,\nClick 'Set Path' to find the install path of Arduino,\nor Click 'Download' to install the Arduino IDE.");
				function onCancel():void{
					dialog.cancel();
				}
				function onSetPath():void{
					var fileRef:File = new File();
					function onPathSelected(evt:Event):void{
						var f:File = evt.target as File;
						arduinoPath = f.url;
					}
					fileRef.browseForDirectory(Translator.map("Arduino IDE"));
					fileRef.addEventListener(Event.SELECT,onPathSelected);
					dialog.cancel();
				}
				function onDownload():void{
					flash.net.navigateToURL(new URLRequest("http://learn.makeblock.cc/learning-arduino/"));
					dialog.cancel();
				}
				dialog.addButton("Cancel",onCancel);
				dialog.addButton("Set Path",onSetPath);
				dialog.addButton("Download",onDownload);
				dialog.showOnStage(MBlock.app.stage);
				return "Arduino IDE not found.";
			}
			*/
			prepareProjectDir(ccode)
			var file:File;
			if(ApplicationManager.sharedManager().system==ApplicationManager.WINDOWS){
				file = new File(arduinoInstallPath+"/arduino.exe");
			}else{
//				file = new File(arduinoInstallPath+"/../../MacOS/JavaApplicationStub");
//				if(!file.exists){
					file = new File(arduinoInstallPath+"/../MacOS/Arduino");
//				}
			}
			
			var processArgs:Vector.<String> = new Vector.<String>(); 
			//trace(contents[i].name, contents[i].size);
			var nativeProcessStartupInfo:NativeProcessStartupInfo =new NativeProcessStartupInfo();
			nativeProcessStartupInfo.executable = file;
			processArgs.push(projectPath+"/"+projectDocumentName+".ino")
			nativeProcessStartupInfo.arguments = processArgs;
			process = new NativeProcess();
			process.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, function(e:ProgressEvent):void{}); 
			process.addEventListener(ProgressEvent.STANDARD_ERROR_DATA, function(e:ProgressEvent):void{});
			process.addEventListener(NativeProcessExitEvent.EXIT, function(e:NativeProcessExitEvent):void{});
			process.start(nativeProcessStartupInfo);
			return ""
		}
		
		private function runToolChain(evt:*):String{
			var cpp:File = tc_projCpp
			var dir:File = tc_workdir
			var cppList:Array = tc_cppList
			archOutputFiles(cpp,dir)
			compileCpp(projectDocumentName+".ino",dir);
			var elf:Array=[projectDocumentName+".ino.o"]
			for(var i:int=0;i<cppList.length;i++){
				var moduleCpp:String = cppList[i]
				compileCpp(moduleCpp,dir)
				elf.push(moduleCpp+".o")
			}
			compileElf(projectDocumentName+".ino",dir,elf);
			generateHex(projectDocumentName+".ino",dir);
			
			nativeDoneEvent = EVENT_COMPILE_DONE
			numOfProcess = nativeWorkList.length
			numOfSuccess = 0
			compileErr = false
			dispatchEvent(new Event(EVENT_NATIVE_DONE));
			return ""
		}
		
		private var arduinoCppList:Array;
		private var arduinoCList:Array;
		private function buildArduinoLib(buildDir:File):void{
			arduinoCppList = []
			arduinoCList = []
			// enum arduino core
			var file:File = new File(arduinoInstallPath+"/hardware/arduino/avr");
			if(file.exists){
				avrPath = "/hardware/arduino/avr" // v1.5
			}else{
				avrPath = "/hardware/arduino" // v1.0
			}
			
			listArduinoLib(new File(arduinoInstallPath+avrPath+"/cores/arduino"));
			// enum arduino libs
			file.url = new File(arduinoInstallPath+avrPath+"/libraries").url;
			if(file.exists){
				arduinoLibPath = avrPath+"/libraries";
			}else{
				arduinoLibPath = "/libraries";
			}
			
			listArduinoLib(new File(arduinoInstallPath+arduinoLibPath+"/Wire"))
			var servoFile:File = new File(arduinoInstallPath+"/libraries/Servo/src/avr");
			//support 1.6.5
			if(servoFile.exists){
				listArduinoLib(new File(arduinoInstallPath+"/libraries/Servo/src/avr"));
			}else{
				listArduinoLib(new File(arduinoInstallPath+"/libraries/Servo"));
			}
			
			listArduinoLib(new File(arduinoInstallPath+arduinoLibPath+"/SoftwareSerial"));
			
			if(_currentDevice=="uno" || _currentDevice=="leonardo"){
				listArduinoLib(new File(arduinoInstallPath+"/libraries/makeblock"));
			}
			
			for (var i:uint = 0; i < arduinoCppList.length; i++)  
			{ 
				compileCpp(arduinoCppList[i],buildDir)
			}
			if(servoFile.exists){
				compileS(buildDir);
			}
			for (i = 0; i < arduinoCList.length; i++)  
			{ 
				compileC(arduinoCList[i],buildDir)
			}
		}
		
		private function listArduinoLib(dir:File):void{
			var files:Array = dir.getDirectoryListing()
			for (var i:uint = 0; i < files.length; i++)  
			{ 
				if(files[i].extension=="cpp")
					arduinoCppList.push(dir.nativePath+"/"+files[i].name)
				if(files[i].extension=="c")
					arduinoCList.push(dir.nativePath+"/"+files[i].name)
				if(files[i].isDirectory){
					listArduinoLib(files[i])
				}
			}
		}
		
		public function get arduinoInstallPath():String{
			if(null == arduinoPath){
				if(Capabilities.os.indexOf("Windows") == 0){
					arduinoPath = File.applicationDirectory.resolvePath("Arduino").nativePath;
				}else{
					arduinoPath = File.applicationDirectory.resolvePath("Arduino/Arduino.app/Contents/Java").nativePath;
				}
			}
			return arduinoPath;
		}
		
		private function archOutputFiles(cpp:File,dir:File):void{
			var file:File = new File(arduinoInstallPath+"/hardware/tools/avr/bin/avr-ar"+(ApplicationManager.sharedManager().system==ApplicationManager.WINDOWS?".exe":"")); 
			
			var cmd:Array = ["rcs","core.a","file.o"]
			var contents:Array = dir.getDirectoryListing();
			for (var i:uint = 0; i < contents.length; i++)  
			{ 
				if(contents[i].name.indexOf(".o")>=0){
					var processArgs:Vector.<String> = new Vector.<String>(); 
					//trace(contents[i].name, contents[i].size);
					var nativeProcessStartupInfo:NativeProcessStartupInfo =new NativeProcessStartupInfo();
					nativeProcessStartupInfo.executable = file;
					nativeProcessStartupInfo.workingDirectory = dir
					processArgs.push(cmd[0])
					processArgs.push(cmd[1])
					processArgs.push(contents[i].name)
					nativeProcessStartupInfo.arguments = processArgs;
					nativeWorkList.push(nativeProcessStartupInfo)
					//process.start(nativeProcessStartupInfo);
				}
			}
		}
		
		private function compileCpp(cpp:String,dir:File):void{
			var nativeProcessStartupInfo:NativeProcessStartupInfo =new NativeProcessStartupInfo();
			var file:File = new File(arduinoInstallPath+"/hardware/tools/avr/bin/avr-g++"+(ApplicationManager.sharedManager().system==ApplicationManager.WINDOWS?".exe":"")); 
			//			file.url = arduinoInstallPath+"/hardware/tools/avr/bin/avr-g++.exe";// todo: read the arduino path from setup profile
			nativeProcessStartupInfo.executable = file
			nativeProcessStartupInfo.workingDirectory = dir
			// todo: leonardo and use global arduino path
			var path:String = arduinoInstallPath;
			path=path.split("file:///").join("");//.split("/").join("\\");
			var cmd:String = "";
//			trace("currentDevice:",_currentDevice);
			if(_currentDevice=="uno"){
				cmd = " -c -g -Os -w -fno-exceptions -ffunction-sections -fdata-sections -MMD -mmcu=atmega328p -DF_CPU=16000000L -DARDUINO=10605 -DARDUINO_AVR_UNO -DARDUINO_ARCH_AVR -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/standard -I"+path+"/libraries/Servo/src -I"+path+"/libraries/Servo -I"+path+arduinoLibPath+"/Wire -I"+path+arduinoLibPath+"/Wire/utility -I"+path+arduinoLibPath+"/SoftwareSerial -I" + path+"/libraries/makeblock/src"
			}else if(_currentDevice=="leonardo"){
				cmd = " -c -g -Os -w -fno-exceptions -ffunction-sections -fdata-sections -MMD -mmcu=atmega32u4 -DF_CPU=16000000L -DARDUINO=10605 -DARDUINO_AVR_LEONARDO -DARDUINO_ARCH_AVR -DUSB_VID=0x2341 -DUSB_PID=0x8036 -DUSB_MANUFACTURER=\"Unknown\" -DUSB_PRODUCT=\"Arduino Leonardo\" -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/leonardo -I"+path+"/libraries/Servo/src -I"+path+"/libraries/Servo -I"+path+arduinoLibPath+"/Wire -I"+path+arduinoLibPath+"/Wire/utility -I"+path+arduinoLibPath+"/SoftwareSerial -I" + path+"/libraries/makeblock/src"
			}else if(_currentDevice=="mega1280"){
				cmd = " -c -g -Os -w -fno-exceptions -ffunction-sections -fdata-sections -MMD -mmcu=atmega1280 -DF_CPU=16000000L -DARDUINO=10605 -DARDUINO_AVR_MEGA -DARDUINO_ARCH_AVR -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/mega -I"+path+"/libraries/Servo/src -I"+path+"/libraries/Servo -I"+path+arduinoLibPath+"/Wire -I"+path+arduinoLibPath+"/Wire/utility -I"+path+arduinoLibPath+"/SoftwareSerial"
			}else if(_currentDevice=="mega2560"){
				cmd = " -c -g -Os -w -fno-exceptions -ffunction-sections -fdata-sections -MMD -mmcu=atmega2560 -DF_CPU=16000000L -DARDUINO=10605 -DARDUINO_AVR_MEGA2560 -DARDUINO_ARCH_AVR -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/mega -I"+path+"/libraries/Servo/src -I"+path+"/libraries/Servo -I"+path+arduinoLibPath+"/Wire -I"+path+arduinoLibPath+"/Wire/utility -I"+path+arduinoLibPath+"/SoftwareSerial"
			}else if(_currentDevice=="nano328"){
				cmd = " -c -g -Os -w -fno-exceptions -ffunction-sections -fdata-sections -MMD -mmcu=atmega328p -DF_CPU=16000000L -DARDUINO=10605 -DARDUINO_AVR_NANO -DARDUINO_ARCH_AVR -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/eightanaloginputs -I"+path+"/libraries/Servo/src -I"+path+"/libraries/Servo -I"+path+arduinoLibPath+"/Wire -I"+path+arduinoLibPath+"/Wire/utility -I"+path+arduinoLibPath+"/SoftwareSerial"
			}else if(_currentDevice=="nano168"){
				cmd = " -c -g -Os -w -fno-exceptions -ffunction-sections -fdata-sections -MMD -mmcu=atmega168 -DF_CPU=16000000L -DARDUINO=10605 -DARDUINO_AVR_NANO -DARDUINO_ARCH_AVR -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/eightanaloginputs -I"+path+"/libraries/Servo/src -I"+path+"/libraries/Servo -I"+path+arduinoLibPath+"/Wire -I"+path+arduinoLibPath+"/Wire/utility -I"+path+arduinoLibPath+"/SoftwareSerial"
			}
			var arg:Array = cmd.split(" -")
			var processArgs:Vector.<String> = new Vector.<String>(); 
			for(var i:int=0;i<arg.length;i++){
				if(arg[i].length>0)
					processArgs.push("-"+arg[i])
			}
			if(cpp.indexOf(".cpp")!=-1)
				processArgs.push(cpp)
			else
				processArgs.push(cpp+".cpp")
			processArgs.push("-o")	
			var tmp:Array = cpp.split("/")
			cpp = tmp[tmp.length-1]
			processArgs.push(cpp+".o")
			nativeProcessStartupInfo.arguments = processArgs;
			nativeWorkList.push(nativeProcessStartupInfo)
			//process.start(nativeProcessStartupInfo); 
		}
		
		private function compileC(cpp:String, dir:File):void{
			var nativeProcessStartupInfo:NativeProcessStartupInfo =new NativeProcessStartupInfo();
			var file:File = new File(arduinoInstallPath+"/hardware/tools/avr/bin/avr-gcc"+(ApplicationManager.sharedManager().system==ApplicationManager.WINDOWS?".exe":"")); 
			nativeProcessStartupInfo.executable = file
			nativeProcessStartupInfo.workingDirectory = dir;
			var path:String = arduinoInstallPath;
			path=path.split("file:///").join("");//.split("/").join("/");
			//			var cmd:String = " -c -g -Os -w -ffunction-sections -fdata-sections -MMD -mmcu=atmega32u4 -DF_CPU=16000000L -DARDUINO=156 -DARDUINO_AVR_LEONARDO -DARDUINO_ARCH_AVR -DUSB_VID=0x2341 -DUSB_PID=0x8036 -DUSB_MANUFACTURER= -DUSB_PRODUCT=\"Arduino Leonardo\" -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/leonardo -I"+path+arduinoLibPath+"/Wire -I"+path+"/libraries/Servo/src -I"+path+"/libraries/Servo -I"+path+arduinoLibPath+"/SoftwareSerial -I"+path+arduinoLibPath+"/Wire/utility"
			//			if(boardType!="leonardo")
			//				cmd = " -c -g -Os -w -ffunction-sections -fdata-sections -MMD -mmcu=atmega328p -DF_CPU=16000000L -DARDUINO=156 -DARDUINO_AVR_UNO -DARDUINO_ARCH_AVR -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/standard -I"+path+arduinoLibPath+"/Wire -I"+path+"/libraries/Servo/src -I"+path+"/libraries/Servo -I"+path+arduinoLibPath+"/SoftwareSerial -I"+path+arduinoLibPath+"/Wire/utility"
			var cmd:String = "";
			if(_currentDevice=="uno"){
				cmd = " -c -g -Os -w -ffunction-sections -fdata-sections -MMD -mmcu=atmega328p -DF_CPU=16000000L -DARDUINO=10605 -DARDUINO_AVR_UNO -DARDUINO_ARCH_AVR -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/standard -I"+path+"/libraries/Servo/src -I"+path+"/libraries/Servo -I"+path+arduinoLibPath+"/Wire -I"+path+arduinoLibPath+"/Wire/utility -I"+path+arduinoLibPath+"/SoftwareSerial -I" + path+"/libraries/makeblock/src"
			}else if(_currentDevice=="leonardo"){
				cmd = " -c -g -Os -w -ffunction-sections -fdata-sections -MMD -mmcu=atmega32u4 -DF_CPU=16000000L -DARDUINO=10605 -DARDUINO_AVR_LEONARDO -DARDUINO_ARCH_AVR -DUSB_VID=0x2a03 -DUSB_PID=0x8036 -DUSB_MANUFACTURER= -DUSB_PRODUCT=\"Arduino Leonardo\" -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/leonardo -I"+path+"/libraries/Servo/src -I"+path+"/libraries/Servo -I"+path+arduinoLibPath+"/Wire -I"+path+arduinoLibPath+"/Wire/utility -I"+path+arduinoLibPath+"/SoftwareSerial -I" + path+"/libraries/makeblock/src"
			}else if(_currentDevice=="mega1280"){
				cmd = " -c -g -Os -w -ffunction-sections -fdata-sections -MMD -mmcu=atmega1280 -DF_CPU=16000000L -DARDUINO=10605 -DARDUINO_AVR_MEGA -DARDUINO_ARCH_AVR -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/mega -I"+path+"/libraries/Servo/src -I"+path+"/libraries/Servo -I"+path+arduinoLibPath+"/Wire -I"+path+arduinoLibPath+"/Wire/utility -I"+path+arduinoLibPath+"/SoftwareSerial"
			}else if(_currentDevice=="mega2560"){
				cmd = " -c -g -Os -w -ffunction-sections -fdata-sections -MMD -mmcu=atmega2560 -DF_CPU=16000000L -DARDUINO=10605 -DARDUINO_AVR_MEGA2560 -DARDUINO_ARCH_AVR -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/mega -I"+path+"/libraries/Servo/src -I"+path+"/libraries/Servo -I"+path+arduinoLibPath+"/Wire -I"+path+arduinoLibPath+"/Wire/utility -I"+path+arduinoLibPath+"/SoftwareSerial"
			}else if(_currentDevice=="nano328"){
				cmd = " -c -g -Os -w -ffunction-sections -fdata-sections -MMD -mmcu=atmega328p -DF_CPU=16000000L -DARDUINO=10605 -DARDUINO_AVR_NANO -DARDUINO_ARCH_AVR -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/eightanaloginputs -I"+path+"/libraries/Servo/src -I"+path+"/libraries/Servo -I"+path+arduinoLibPath+"/Wire -I"+path+arduinoLibPath+"/Wire/utility -I"+path+arduinoLibPath+"/SoftwareSerial"
			}else if(_currentDevice=="nano168"){
				cmd = " -c -g -Os -w -ffunction-sections -fdata-sections -MMD -mmcu=atmega168 -DF_CPU=16000000L -DARDUINO=10605 -DARDUINO_AVR_NANO -DARDUINO_ARCH_AVR -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/eightanaloginputs -I"+path+"/libraries/Servo/src -I"+path+"/libraries/Servo -I"+path+arduinoLibPath+"/Wire -I"+path+arduinoLibPath+"/Wire/utility -I"+path+arduinoLibPath+"/SoftwareSerial"
			}
			
			var arg:Array = cmd.split(" -")
			var processArgs:Vector.<String> = new Vector.<String>(); 
			for(var i:int=0;i<arg.length;i++){
				if(arg[i].length>0)
					processArgs.push("-"+arg[i])
			}
			if(cpp.indexOf(".c")!=-1)
				processArgs.push(cpp)
			else
				processArgs.push(cpp+".c")
			processArgs.push("-o")	
			var tmp:Array = cpp.split("/")
			cpp = tmp[tmp.length-1]
			processArgs.push(cpp+".o")
			nativeProcessStartupInfo.arguments = processArgs;
			nativeWorkList.push(nativeProcessStartupInfo);
			
		}
		private function compileS(dir:File):void{
			var nativeProcessStartupInfo:NativeProcessStartupInfo =new NativeProcessStartupInfo();
			var file:File = new File(arduinoInstallPath+"/hardware/tools/avr/bin/avr-gcc"+(ApplicationManager.sharedManager().system==ApplicationManager.WINDOWS?".exe":"")); 
			nativeProcessStartupInfo.executable = file
			nativeProcessStartupInfo.workingDirectory = dir;
			var path:String = arduinoInstallPath;
			path=path.split("file:///").join("");//.split("/").join("/");
			//			var cmd:String = " -c -g -Os -w -ffunction-sections -fdata-sections -MMD -mmcu=atmega32u4 -DF_CPU=16000000L -DARDUINO=156 -DARDUINO_AVR_LEONARDO -DARDUINO_ARCH_AVR -DUSB_VID=0x2341 -DUSB_PID=0x8036 -DUSB_MANUFACTURER= -DUSB_PRODUCT=\"Arduino Leonardo\" -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/leonardo -I"+path+arduinoLibPath+"/Wire -I"+path+"/libraries/Servo/src -I"+path+"/libraries/Servo -I"+path+arduinoLibPath+"/SoftwareSerial -I"+path+arduinoLibPath+"/Wire/utility"
			//			if(boardType!="leonardo")
			//				cmd = " -c -g -Os -w -ffunction-sections -fdata-sections -MMD -mmcu=atmega328p -DF_CPU=16000000L -DARDUINO=156 -DARDUINO_AVR_UNO -DARDUINO_ARCH_AVR -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/standard -I"+path+arduinoLibPath+"/Wire -I"+path+"/libraries/Servo/src -I"+path+"/libraries/Servo -I"+path+arduinoLibPath+"/SoftwareSerial -I"+path+arduinoLibPath+"/Wire/utility"
			var cmd:String = "";
			if(_currentDevice=="uno"){
				cmd = " -c -g -x -assembler-with-cpp -mmcu=atmega328p -DF_CPU=16000000L -DARDUINO=10605 -DARDUINO_AVR_UNO -DARDUINO_ARCH_AVR -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/standard -"+path+avrPath+"/cores/arduino/wiring_pulse.S";
			}else if(_currentDevice=="leonardo"){
				cmd = " -c -g -x -assembler-with-cpp -mmcu=atmega32u4 -DF_CPU=16000000L -DARDUINO=10605 -DARDUINO_AVR_LEONARDO -DARDUINO_ARCH_AVR -DUSB_VID=0x2a03 -DUSB_PID=0x8036 -DUSB_MANUFACTURER= -DUSB_PRODUCT=\"Arduino Leonardo\" -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/leonardo -"+path+avrPath+"/cores/arduino/wiring_pulse.S"
			}else if(_currentDevice=="mega1280"){
				cmd = " -c -g -x -assembler-with-cpp -mmcu=atmega1280 -DF_CPU=16000000L -DARDUINO=10605 -MMD -DUSB_VID=null -DUSB_PID=null -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/standard -"+path+avrPath+"/cores/arduino/wiring_pulse.S";
			}else if(_currentDevice=="mega2560"){
				cmd = " -c -g -x -assembler-with-cpp -mmcu=atmega2560 -DF_CPU=16000000L -DARDUINO=10605 -DARDUINO_AVR_MEGA2560 -DARDUINO_ARCH_AVR -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/standard -"+path+avrPath+"/cores/arduino/wiring_pulse.S";
			}else if(_currentDevice=="nano328"){
				cmd = " -c -g -x -assembler-with-cpp -mmcu=atmega328p -DF_CPU=16000000L -DARDUINO=10605 -MMD -DUSB_VID=null -DUSB_PID=null -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/standard -"+path+avrPath+"/cores/arduino/wiring_pulse.S";
			}else if(_currentDevice=="nano168"){
				cmd = " -c -g -x -assembler-with-cpp -mmcu=atmega168 -DF_CPU=16000000L -DARDUINO=10605 -MMD -DUSB_VID=null -DUSB_PID=null -I"+path+avrPath+"/cores/arduino -I"+path+avrPath+"/variants/standard -"+path+avrPath+"/cores/arduino/wiring_pulse.S";
			}
			
			var arg:Array = cmd.split(" -")
			var processArgs:Vector.<String> = new Vector.<String>(); 
			for(var i:int=0;i<arg.length;i++){
				if(arg[i].length>0){
					if(arg[i].indexOf("assembler")>-1||arg[i].indexOf("arduino/wiring_pulse.S")>-1){
						processArgs.push(arg[i]);
					}else{
						processArgs.push("-"+arg[i]);
					}
				}
			}
			processArgs.push("-o");
			processArgs.push("./wiring_pulse.S.o");
			nativeProcessStartupInfo.arguments = processArgs;
			nativeWorkList.push(nativeProcessStartupInfo);
		}
		private function compileElf(token:String,dir:File,elf:Array):void
		{
			var cmd:String = ""
			if(_currentDevice=="uno"){
				cmd = " -Os -Wl,--gc-sections -mmcu=atmega328p -o token.elf elflist core.a -L./ -lm "
			}else if(_currentDevice=="leonardo"){
				cmd = " -Os -Wl,--gc-sections -mmcu=atmega32u4 -o token.elf elflist core.a -L./ -lm ";
			}else if(_currentDevice=="mega1280"){
				cmd = " -Os -Wl,--gc-sections,--relax -mmcu=atmega1280 -o token.elf elflist core.a -L./ -lm ";
			}else if(_currentDevice=="mega2560"){
				cmd = " -Os -Wl,--gc-sections,--relax -mmcu=atmega2560 -o token.elf elflist core.a -L./ -lm ";
			}else if(_currentDevice=="nano328"){
				cmd = " -Os -Wl,--gc-sections,--relax -mmcu=atmega328p -o token.elf elflist core.a -L./ -lm ";
			}else if(_currentDevice=="nano168"){
				cmd = " -Os -Wl,--gc-sections,--relax -mmcu=atmega168 -o token.elf elflist core.a -L./ -lm ";
			}
			if(elf.indexOf("MeServo.o")!=-1){
				elf.push("./Servo.cpp.o")
			}
			if(elf.indexOf("MeGyro.o")!=-1){
				elf.push("./Wire.cpp.o")
				elf.push("./twi.c.o")
			}
			if(elf.indexOf("MeInfraredReceiver.o")!=-1){
				elf.push("./SoftwareSerial.cpp.o")
			}
			var elflist:String = elf.join(" ")
			cmd = cmd.replace("token", token).replace("elflist", elflist)
			
			var nativeProcessStartupInfo:NativeProcessStartupInfo =new NativeProcessStartupInfo();
			var file:File = new File(arduinoInstallPath+"/hardware/tools/avr/bin/avr-gcc"+(ApplicationManager.sharedManager().system==ApplicationManager.WINDOWS?".exe":"")); 
			nativeProcessStartupInfo.executable = file
			nativeProcessStartupInfo.workingDirectory = dir
			var arg:Array = cmd.split(" ")
			var processArgs:Vector.<String> = new Vector.<String>(); 
			for(var i:int=0;i<arg.length;i++){
				if(arg[i].length>0)
					processArgs.push(arg[i])
			}
			nativeProcessStartupInfo.arguments = processArgs;
			nativeWorkList.push(nativeProcessStartupInfo);
			//process.start(nativeProcessStartupInfo); 
		}
		
		private function generateHex(cpp:String,dir:File):void{
			var nativeProcessStartupInfo:NativeProcessStartupInfo;
			var file:File = new File(arduinoInstallPath+"/hardware/tools/avr/bin/avr-objcopy"+(ApplicationManager.sharedManager().system==ApplicationManager.WINDOWS?".exe":"")); 
			
			// step 1
			var cmd:String = " -O ihex -j .eeprom --set-section-flags=.eeprom=alloc,load --no-change-warnings --change-section-lma .eeprom=0";
			var processArgs:Vector.<String> = new Vector.<String>(); 
			nativeProcessStartupInfo=new NativeProcessStartupInfo()
			nativeProcessStartupInfo.executable = file
			nativeProcessStartupInfo.workingDirectory = dir
			var arg:Array = cmd.split(" ")
			for(var i:int=0;i<arg.length;i++){
				if(arg[i].length>0)
					processArgs.push(arg[i])
			}
			processArgs.push(cpp+".elf")
			processArgs.push(cpp+".eep")
			nativeProcessStartupInfo.arguments = processArgs;
			nativeWorkList.push(nativeProcessStartupInfo)
			// step 2
			cmd = " -O ihex -R .eeprom"
			var processArgs2:Vector.<String> = new Vector.<String>(); 
			nativeProcessStartupInfo=new NativeProcessStartupInfo()
			nativeProcessStartupInfo.executable = file
			nativeProcessStartupInfo.workingDirectory = dir;
			var arg2:Array = cmd.split(" ")
			for(i=0;i<arg2.length;i++){
				if(arg2[i].length>0)
					processArgs2.push(arg2[i])
			}
			processArgs2.push(cpp+".elf")
			processArgs2.push(cpp+".hex")
			nativeProcessStartupInfo.arguments = processArgs2;
			nativeWorkList.push(nativeProcessStartupInfo)
			
		}
		
		
		private function gotoNextNativeCmd(event:Event):void{
			isUploading = true;
			process = new NativeProcess();
			process.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, onOutputData); 
			process.addEventListener(ProgressEvent.STANDARD_ERROR_DATA, onErrorData);
			process.addEventListener(NativeProcessExitEvent.EXIT, onExit);
			if(nativeWorkList.length>0 && compileErr==false){
				var nativeProcessStartupInfo:NativeProcessStartupInfo = nativeWorkList.shift()
				MBlock.app.scriptsPart.appendMessage(nativeProcessStartupInfo.executable.nativePath)
				MBlock.app.scriptsPart.appendMessage(nativeProcessStartupInfo.arguments.toString())
				process.start(nativeProcessStartupInfo); 
			}else if(nativeWorkList.length==0){
				// todo: is there a better way to check success of make??
				if(numOfSuccess==numOfProcess)
					dispatchEvent(new Event(nativeDoneEvent))
			}
		}
		
		
		private function onOutputData(event:ProgressEvent):void 
		{ 
			isUploading = true;
			/*
			var output:String = process.standardOutput.readUTFBytes(process.standardOutput.bytesAvailable)
			var date:Date = new Date;
			MBlock.app.scriptsPart.appendMessage(""+(date.month+1)+"-"+date.date+" "+date.hours+":"+date.minutes+": Got: "+output); 
			*/
		}
		
		private function onErrorData(event:ProgressEvent):void
		{
			isUploading = true;
			compileErr = true
			var errOut:String = process.standardError.readUTFBytes(process.standardError.bytesAvailable);
			if(null == errorText){
				errorText = errOut;
			}else{
				errorText += errOut;
			}
		}
		
		private function onExit(event:NativeProcessExitEvent):void
		{
			isUploading = false;
			var date:Date = new Date;
			
			MBlock.app.scriptsPart.appendMessage(""+(date.month+1)+"-"+date.date+" "+date.hours+":"+date.minutes+": Process exited with "+event.exitCode);
			numOfSuccess++;
			if(event.exitCode > 0){
				MBlock.app.scriptsPart.appendMsgWithTimestamp(errorText, true);
				errorText = null;
			}
			if(compileErr == false){
				dispatchEvent(new Event(EVENT_NATIVE_DONE));
			}
		}
		
		private var errorText:String;
	}
}