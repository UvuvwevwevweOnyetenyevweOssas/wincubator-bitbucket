// ActionScript file

//Motor Position Checker for GT WonderBoy
//This file checks for positions of User Defined Gt WonderBoy Motors for any conflict
package extensions
{
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.NativeProcessExitEvent;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	import flash.system.Capabilities;
	import flash.utils.ByteArray;
	import flash.utils.getQualifiedClassName;
	
	import mx.utils.ObjectUtil;
	
	import blocks.Block;
	import blocks.BlockIO;
	
	import cc.makeblock.mbot.util.StringUtil;
	
	import translation.Translator;
	
	import uiwidgets.DialogBox;
	
	import util.ApplicationManager;
	import util.JSON;
	import util.LogManager;
	import util.SharedObjectManager;
	
	public class PosChecker extends EventDispatcher {
		
		private static var _instance:PosChecker;
		
		public static var warningmsg:String = "";
		public static var complexWarning:String = "";
		public static var checker:Boolean = false;
		private var movementSetCache:Object = new Object;
//		private var motorRestrictionRange:Object = new Object;
		private var checkspeed:int;
		
		//22. Complex Movement Checker
	    private var movementWarning:String = "";
		
//		private function initRan():void{
//			var motorRestrictionRange:Object = new Object;
//			motorRestrictionRange[0] = [-98,98];//1
//			motorRestrictionRange[1] = [-8,33];//2
//			motorRestrictionRange[2] = [-500,500];//3
//			motorRestrictionRange[3] = [-500,500];//4
//			motorRestrictionRange[4] = [-200,86];//5
//			motorRestrictionRange[5] = [-86,200];//6
//			motorRestrictionRange[6] = [-500,500];//7
//			motorRestrictionRange[7] = [-500,500];//8
//			motorRestrictionRange[8] = [-400,400];//9
//			motorRestrictionRange[9] = [-400,400];//10
//			motorRestrictionRange[10] = [-200,200];//11
//			motorRestrictionRange[11] = [-200,200];//12
//			motorRestrictionRange[12] = [0,300];
//		}
		
		public static function sharedManager():PosChecker{
			if(_instance==null){
				_instance = new PosChecker;
			}
			return _instance;
		} 
		
		
		
		//Not complex
		public  static function checkPosSHELL(blk:Object):String{
			var motorRestrictionRange:Object = new Object;
			motorRestrictionRange[0] = [-150,150];//1 -25~25 degree -39~39
			motorRestrictionRange[1] = [-30,10];//2 -2~8 -7~2
			motorRestrictionRange[2] = [-420,400];//3 -131~131 -110~105
			motorRestrictionRange[3] = [-400,420];//4 -131~131 -105~110
			motorRestrictionRange[4] = [-300,100];//5 -52~22 -79~26
			motorRestrictionRange[5] = [-100,300];//6 -22~52 -26~79
			motorRestrictionRange[6] = [-400,470];//7 -131~131 -105~123
			motorRestrictionRange[7] = [-470,400];//8 -131~131 -123~105
			motorRestrictionRange[8] = [-330,350];//9 -105~105 -87~92
			motorRestrictionRange[9] = [-350,330];//10 -105~105 -92~87
			motorRestrictionRange[10] = [-40,120];//11 -52~52 -10~31
			motorRestrictionRange[11] = [-40,120];//12 -52~52 -10~31
			motorRestrictionRange[12] = [0,300];
			
			checker = false;
			//var returnMsg:String = "";
			warningmsg = "";
			var speedOfMovement:Number = blk[14];
//			initRan();
			
			//Remove Start & end Block
			var editblk:Object = new Object;
			editblk = clone(blk);
			editblk = filterObj(editblk);
			
			
									
			for (var i:int = 0; i < 12; i++){
				if ((editblk[i+1] *3.7926)< motorRestrictionRange[i][0] || (editblk[i+1]*3.7926) > motorRestrictionRange[i][1]){
					warningmsg += "Warning: Motor "+(i+1)+" is out of range \n";
					checker = true;
					break;
				}
			}
			
			
			return warningmsg;			
		}
		
		//17. Malcolm Complex 
		private function complexMovementChecker(blk:Object):Boolean{
			var motorRestrictionRange:Object = new Object;
			motorRestrictionRange[0] = [-98,98];//1
			motorRestrictionRange[1] = [-8,33];//2
			motorRestrictionRange[2] = [-500,500];//3
			motorRestrictionRange[3] = [-500,500];//4
			motorRestrictionRange[4] = [-200,86];//5
			motorRestrictionRange[5] = [-86,200];//6
			motorRestrictionRange[6] = [-500,500];//7
			motorRestrictionRange[7] = [-500,500];//8
			motorRestrictionRange[8] = [-400,400];//9
			motorRestrictionRange[9] = [-400,400];//10
			motorRestrictionRange[10] = [-200,200];//11
			motorRestrictionRange[11] = [-200,200];//12
			motorRestrictionRange[12] = [0,300];
			
			complexWarning = "";
			var status:Boolean = false;
			for (var i:int = 0; i < blk.length; i++){
				for (var c:int = 0; c < 12; c++){
					if (blk[i+1][c] < motorRestrictionRange[c][0] || blk[i+1][c] > motorRestrictionRange[c][1]){
						complexWarning += "Position "+(i+1)+": Motor "+(c+1)+"\n";					
						status = true;
					}
				}
				//Check speed timing Only
				if (blk[i+1][12] < motorRestrictionRange[12][0] || blk[i+1][12] > motorRestrictionRange[12][1]){
					complexWarning += "Position " + (i+1) + " timing is out of range!" + "\n";
					status = true;
				}
				
			}
			//speedchecker(checkspeed);
			return status;
		}
		
		//22. Motion Range Checker
		public function readMovlog(movlog:String):Array{
			//trim front #
			movlog = movlog.substr(1);
			movlog = movlog.slice(0,-1);
			movlog = movlog.replace("\n","");
			mx.utils.StringUtil.trim(movlog);
			var array:Array = new Array();
			trace (array = movlog.split("#"));
			trace (array);
			
			for (var i:int = 0; i < array.length; i++){
			    var totrim:String = array[i];
				/*if (array[i].length > 4){
					array[i] = totrim.substr(3).slice(0,-1);
				}
				else{
					array[i] = totrim.substr(2).slice(0,-1);
				}*/
				if (i > 8){
					array[i] = totrim.substr(3).slice(0,-1);
				}
				else{
					array[i] = totrim.substr(2).slice(0,-1);
				}
			}
			
			return array;
			
		}
		
		public function readcomplexMovLog(movlog:String):Boolean{
			movlog = movlog.substr(15);
			movlog = movlog.slice(0,-1);
			var array:Array = new Array();
			trace(array = movlog.split("\n"));
			trace(array);			
			
			var logCache:Object = new Object;
			
			for (var i:int = 0; i < array.length; i++){
				logCache[i] = readMovlog(array[i]);
				trace(logCache[i]);
				logCache.length = i+1;
			}
			                                                                                                          
			//trace(logCache);
			return checkLimbs(logCache);
			
		}
		
		private function checkLimbs(movset:Object):Boolean{
			var checker:Boolean = false;
			movementWarning = "";
			
			if (checkLeftArm(movset) == true){
				checker = true;
				//movementWarning += "There is a conflict in the Left arm in Position "+""+"\n";
			}
			if (checkRightArm(movset) == true){
				checker = true;
				//movementWarning += "There is a conflict in the Right arm in Position"+""+"\n";
			}
			
			return checker;
		}
		
		//22. Left arm checker
		private function checkLeftArm(movset:Object):Boolean{
			var checker:Boolean = false;
			
			
			//single row checker
			//var motor4:int = movset[0][3];
			//var motor6:int = movset[0][5];
			
			//if (motor4 != 0 || motor6 != 0 ){
				//if (motor6 > calculateMaxRangeLeftArm(motor4,motor6)){
					//checker=true;
				//}
			//}
			
			for (var i:int = 0; i < movset.length; i++){	
			 	if (movset[i][3] != 0 || movset[i][5]){
					if (movset[i][5] > calculateMaxRangeLeftArm(movset[i][3],movset[i][5])){
						checker = true;
						movementWarning += "There is a conflict in the Left arm(Motor 3 & 5) in Position "+ (i+1) +"\n";
					}
				}
				
			}
			
			
			
			return checker;
		}
		
		//22. Movement check Linear Reggresion/Fitting
		private function calculateMaxRangeLeftArm(motor4:int,motor6:int):int{
			var maxRange:int = 0;
			//3 Area of conflict. Bottom, middle, top
			
			
			if (motor4 > 360 ){
				//top y=724.30111260359-1.1309653546899x
				//calculate max motor6
				maxRange = 724.30111260359-(1.1309653546899*motor4);
				
			}
			else if (motor4 > 178){
				//middle
				maxRange = 200;
			}	
			else if (motor4 >= 0){
				//bottom y=55.737420157997+1.3786866935812x
				maxRange = 55.737420157997+(1.3786866935812*motor4);
			}
			else{
				maxRange = 45;
			}
			
			return maxRange;
		}
		//22. left arm checker end
		
		//22. Right Arm checker
		private function checkRightArm(movset:Object):Boolean{
			var checker:Boolean = false;
			
			//var motor3:int =  movset[0][2];
			//var motor5:int = movset[0][4];
			
			//if (motor3 != 0 || motor5 != 0 ){
				//if (motor5 > calculateMaxRangeRightArm(motor3,motor5)){
					//checker=true;
				//}
			//}
			
			for (var i:int = 0; i < movset.length; i++){	
				if (movset[i][2] != 0 || movset[i][4]){
					if (movset[i][4] > calculateMaxRangeLeftArm(movset[i][3],movset[i][5])){
						checker = true;
						movementWarning += "There is a conflict in the Right arm(Motor 4 & 6) in Position "+ (i+1) +"\n";
					}
				}
				
			}
			
			return checker;
		}
		
		private function speedchecker(speed:int):void{
			if (speed > 300){
				complexWarning += "Speed is out of range!"
			}
		}
		
		//22. Movement check Linear Reggresion/Fitting
		private function calculateMaxRangeRightArm(motor3:int,motor5:int):int{
			var maxRange:int = 0;
			//3 Area of conflict. Bottom, middle, top
			
			
			if (motor3 > 336 ){
				//top y=636.89139445682-0.93736080178174x
				//calculate max motor6
				maxRange = 636.89139445682-(0.93736080178174*motor3);
				
			}
			else if (motor3 > 216){
				//middle
				maxRange = 200;
			}	
			else if (motor3 >= 0){
				//bottom y=48.071132410748+1.1542009943819x
				maxRange = 48.071132410748+(1.1542009943819*motor3);
			}
			else{
				maxRange = 45;
			}
			
			return maxRange;
		}
		//22. Right arm checker end
		
		/*//22. Body Arm checker
		private function bodyArmConflictChecker(movset:Object):Boolean{
			var checker:Boolean = false;
			
			//Arm
			var motor3:int = movset[0][2];
			var motor4:int = movset[0][3];
			//Elbow
			var motor5:int = movset[0][4];
			var motor6:int = movset[0][6];
			
			if (motor3 != 0 || motor5 != 0 || motor4 != 0 || motor6 != 0){
				if (motor5 > bodyArmMaxRange(motor3,motor4,motor5,motor6)){
					checker=true;
				}
			}
			
			return checker;
		}
		
		private function bodyArmMaxRange(motor3,motor4,motor5,motor6:int):Boolean{
			var checker:Boolean = false;
			
			//Not needed yet
			
			return checker;  
		}
		//22. body arm checker end*/
		
		private static function filterObj(editblk:Object):Object{
			trace(delete editblk[0]);
			trace(delete editblk[13]);
//			editblk.length = 12;
			return editblk;
		}
		
		private static function clone( source:Object ):* { 
			var myBA:ByteArray = new ByteArray(); 
			myBA.writeObject( source ); 
			myBA.position = 0; 
			return( myBA.readObject() ); 
		}
		
//		public function checkPos():Boolean{
//			return this.checker;
//		}
//		
//		public function getErr():String{
//			return this.warningmsg;
//		}
//		
//		public function getComplexErr():String{
//			return this.complexWarning;
//		}
		
		public function addMovStep(MotorArray:Array,i:int):void{
			trace(movementSetCache[i] = MotorArray);
			movementSetCache.length = i;
 		}
		
		public function checkforLimitConflict():Boolean{
			return complexMovementChecker(movementSetCache);
		}
		
		public function clearmovementSet():void{
			movementSetCache = new Object;
		}
		
		public function getComplexMovErr():String{
			return this.movementWarning;
		}
		
		public function setspeed(speed:int):void{
			this.checkspeed = speed;
		}
		
	}
	
}