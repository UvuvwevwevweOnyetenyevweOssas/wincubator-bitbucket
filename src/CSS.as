/*
* Scratch Project Editor and Player
* Copyright (C) 2014 Massachusetts Institute of Technology
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

// CSS.as
// Paula Bonta, November 2011
//
// Styles for Scratch Editor based on the Upstatement design.

package {
	import flash.text.TextFormat;
	
	import assets.Resources;
	
	public class CSS {
		
		//	// Colors
		//	public static const white:int = 0xFFFFFF;
		//	public static const topBarColor:int = 0x9C9EA2;
		//	public static const tabColor:int = tabColorDark//0xE6E8E8; // default background
		//	public static const tabColorDark:int = 0x000000; // dark background
		//	public static const panelColor:int = tabColorDark;//0xF2F2F2; // default background sound section
		//	public static const panelColorDark:int = tabColorDark;
		//	public static const itemSelectedColor:int = white;//0xD0D0D0; // default selection music in sound section
		//	public static const itemSelectedColorDark:int = 0xFFF000;
		//	public static const borderColor:int = tabColorDark;//0xD0D1D2; // default border color
		//	public static const borderColorDark:int = white;
		//	public static const textColor:int = 0xFFF000;//0x5C5D5F; // 0x6C6D6F // text color
		//	public static const buttonLabelColor:int = textColor;
		//	public static const buttonLabelOverColor:int = 0xFFF000;//0xFBA939;
		//	public static const offColor:int = 0xFFF000;//0x8F9193; // 0x9FA1A3 // control, motions, wheels ...
		//	public static const onColor:int = 0xFFF000;//textColor; // 0x4C4D4F // script, sound writing when on
		//	public static const overColor:int = 0x179FD7; // over control, motions, wheels ...
		//	public static const arrowColor:int = 0xFF0000;//0xA6A8AC;
		//
		//	// Fonts
		//	public static const font:String = Resources.chooseFont(['微软雅黑','Arial', 'Verdana', 'DejaVu Sans','Microsoft Yahei']);
		//	public static const menuFontSize:int = 13;
		//	public static const normalTextFormat:TextFormat = new TextFormat(font, 13, textColor);
		//	public static const topBarButtonFormat:TextFormat = new TextFormat(font, 12, white, true);
		//	public static const titleFormat:TextFormat = new TextFormat(font, 13, textColor);
		//	public static const thumbnailFormat:TextFormat = new TextFormat(font, 12, textColor);
		//	public static const thumbnailExtraInfoFormat:TextFormat = new TextFormat(font, 12, textColor);
		//	public static const projectTitleFormat:TextFormat = new TextFormat(font, 13, textColor);
		//	public static const projectInfoFormat:TextFormat = new TextFormat(font, 12, textColor);
		
		// Colors
		public static var white:int = 0xFFFFFF;
		public static var golden:int = 0xD4AF37;
		public static var topBarColor:int = 0x9C9EA2; // no change
		public static var tabColor:int = 0xE6E8E8; // default background
		public static var tabColorDark:int = 0x000000; // dark background
		public static var panelColor:int = 0xF2F2F2; // default background sound section
		public static var itemSelectedColor:int = 0xD0D0D0; // default selection music in sound section
		public static var borderColor:int = 0xD0D1D2; // default border color
		public static var textColor:int = 0x5C5D5F; // 0x6C6D6F // text color
		public static var buttonLabelColor:int = textColor;
		public static var buttonLabelOverColor:int = 0xFBA939;
		public static var offColor:int = 0x8F9193; // 0x9FA1A3 // control, motions, wheels ...
		public static var onColor:int = textColor; // 0x4C4D4F // script, sound writing when on
		public static var overColor:int = 0x179FD7; // over control, motions, wheels ...
		public static var arrowColor:int = 0xFF0000;//0xA6A8AC;
		public static var lineSeparation:int = CSS.borderColor - 0x141414;
		public static var rectangleMiddle:int = lineSeparation;
		public static var textNoVCProject:int = 0x505050;
		public static var sendTextPaneColor:int = 0xEEAD0E;// 0xEEB422;//0x7F7F7F;//0xf8f8f8;
		public static var sendTextPaneMessage:int = 0x000000;//white;
		public static var listColor:int = 0xF1F1F1;
		public static var savedMovementColor:int = 0xEE7D16;//orange
		// Fonts
		public static const font:String = Resources.chooseFont(['微软雅黑','Arial', 'Verdana', 'DejaVu Sans','Microsoft Yahei']);
		public static const menuFontSize:int = 13;
		public static const normalTextFormat:TextFormat = new TextFormat(font, 13, textColor);
		public static const topBarButtonFormat:TextFormat = new TextFormat(font, 12, white, true);
		public static const titleFormat:TextFormat = new TextFormat(font, 13, textColor);
		public static const thumbnailFormat:TextFormat = new TextFormat(font, 12, textColor);
		public static const thumbnailExtraInfoFormat:TextFormat = new TextFormat(font, 12, textColor);
		public static const projectTitleFormat:TextFormat = new TextFormat(font, 13, textColor);
		public static const projectInfoFormat:TextFormat = new TextFormat(font, 12, textColor);
		
		// Section title bars
		public static var titleBarColors:Array = [white, tabColor];
		public static const titleBarH:int = 30;
		
	}}
